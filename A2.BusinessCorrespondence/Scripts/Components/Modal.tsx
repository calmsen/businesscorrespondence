﻿import * as React from "react";
import * as ReactDOM from "react-dom";

/**
 * Показывает указанный контент в рамках указанного DOM-элемента(или react элемента) как модальный PopUp-объект.
 *
 * @param content		Контент.
 * @param scope			DOM-элемент, контейнер для PopUp-объекта. Не указывать scope в качестве body, 
 *						это делается автоматически если scope не передан. Иначе могут возникнут ошибки.
 *						Если не указан, контейнерои считается вся страница.
 * @param isModal		Если модальное то не закрывается при нажатии в не области окна
 * @param zIndex		z-index style
 * @param overlayColor  Цвет заднего слоя
 */
export function popUp(content: HTMLElement, scope?: HTMLElement, isModal?: boolean, zIndex?: number, overlayColor?: string): PopUp;
export function popUp(content: React.ReactNode, scope?: HTMLElement, isModal?: boolean, zIndex?: number, overlayColor?: string): PopUp;
export function popUp(content: any, scope?: HTMLElement, isModal?: boolean, zIndex?: number, overlayColor?: string): PopUp {
	let manualZIndex = typeof zIndex !== "undefined";

	if (!manualZIndex) {
		zIndex = POP_UPS.length === 0
			? MIN_POPUP_Z_INDEX
			: POP_UPS[POP_UPS.length - 1].zIndex;
	}

	scope = scope || document.body;
	let htmlElement = document.documentElement || document.body.parentNode;
	
	let scopePosition: string | null;
	let scopeOverflow: string | null;

	if (scope !== document.body) {
		scopePosition = scope.style.position;
		scopeOverflow = scope.style.overflow;
		scope.style.position = "relative";
		scope.style.overflow = "hidden";
	}
	else {
		scopeOverflow = htmlElement.style.overflow;
		htmlElement.style.overflow = "hidden";
	}

	let container = document.createElement("div");

	container.style.backgroundColor = overlayColor || "rgba(0,0,0,0.5)";
	container.style.zIndex = `${zIndex} !important`;
	container.style.overflow = "hidden";
	container.style.position = "absolute";
	container.style.top = (scope !== document.body ? scope.scrollTop : (window.pageYOffset || htmlElement.scrollTop)) + "px";
	container.style.width = "100%";
	container.style.minHeight = "100%";

	let contentContainer = document.createElement("div");
	contentContainer.style.position = "absolute";
	contentContainer.style.top = "50%";
	contentContainer.style.left = "50%";
	contentContainer.style.backgroundColor = "white";

	const contentContainerOnClick = (e: MouseEvent) => {
		e.stopPropagation();
	};

	const htmlElementOnClick = (e: MouseEvent) => {
		popUp.close();
	};
	if (isModal === false) {
		contentContainer.addEventListener('click', contentContainerOnClick);
		htmlElement.addEventListener('click', htmlElementOnClick);
	}
	else {
	}

	if (!content.tagName && !content.nodeName)
		ReactDOM.render(content, contentContainer);
	else
		contentContainer.appendChild(content);

	container.appendChild(contentContainer);
	
	scope.appendChild(container);
	
	center(contentContainer);


	let popUp = {
		close: () => {
			if (!scope || !container.parentNode)
				return;

			contentContainer.removeEventListener('click', contentContainerOnClick)
			htmlElement.removeEventListener('click', htmlElementOnClick)
			scope.removeChild(container);

			if (!manualZIndex)
				POP_UPS.splice(POP_UPS.indexOf(info), 1);

			if (scope !== document.body) {
				scope.style.position = scopePosition;
				scope.style.overflow = scopeOverflow;
			}
			else {
				htmlElement.style.overflow = scopeOverflow;
			}

		},
		center: () => center(contentContainer)
	};
	
	let info = {
		zIndex: zIndex!,
		popUp: popUp
	};

	if (!manualZIndex)
		POP_UPS.push(info);

	return popUp;
}

/**
 * Возвращает значение true, если указанный объект PopUp находится на самом верхнем уровне.
 *
 * @param popUp Объект PopUp для проверки.
 */
export function isTopLevel(popUp: PopUp) {
	if (POP_UPS.length === 0)
		return false;

	return POP_UPS[POP_UPS.length - 1].popUp === popUp;
}

/**
* Объект управления показанным PopUp-объектом.
*/
export interface PopUp {
    /**
    * Закрывает PopUp-элемент.
    */
	close(): void;
    /**
    * Центрирует PopUp-элемент относительно контейнера.
    */
	center(): void;
}

/**
 * Минимальное значение z-index для элемента, показанного через функцию a2web/ui/Modal::popUp().
 */
export const MIN_POPUP_Z_INDEX = 10000;
/**
 * Максимальное значение z-index для элемента, показанного через функцию a2web/ui/Modal::popUp().
 */
export const MAX_POPUP_Z_INDEX = 2141000000;

/**
 * Массив всех показанных в настоящий момент PopUp-элементах.
 */
const POP_UPS: PopUpInfo[] = [];

/**
* Объект, представляющий информацию о показанных в настоящий момент модальных PopUp-объектах.
*/
interface PopUpInfo {
    /**
    * z-index PopUp-объекта.
    */
	zIndex: number;
    /**
    * Объект PopUp.
    */
	popUp: PopUp;
}

/**
 * Выравнивает элемент по центру. Следует вызывать при изменении содержимого окна.
 * @param element
 */
function center(element: HTMLElement) {
	element.style.marginTop = (-parseInt(window.getComputedStyle(element).height || "0") / 2).toString() + "px";
	element.style.marginLeft = (-parseInt(window.getComputedStyle(element).width || "0") / 2).toString() + "px";
}