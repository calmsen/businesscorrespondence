﻿
export namespace Utils {
	/**
	 * Генерирует guid
	 */
	export function guid() {
		function s4() {
			return Math.floor((1 + Math.random()) * 0x10000)
				.toString(16)
				.substring(1);
		}
		return s4() + s4() + '-' + s4() + '-' + s4() + '-' + s4() + '-' + s4() + s4() + s4();
	}

	export function formatDate(date: Date, format: format = "yyyy-MM-dd HH:mm:ss"): string {
		let formatedYear = date.getFullYear().toString();
		let formatedMonth = addLeadZero((date.getMonth() + 1).toString());
		let formatedDate = addLeadZero(date.getDate().toString());

		let formatedHours = addLeadZero(date.getHours().toString());
		let formatedMinutes = addLeadZero(date.getMinutes().toString());
		let formatedSeconds = addLeadZero(date.getSeconds().toString());

		switch (format) {
			case "yyyy-MM-dd HH:mm:ss":
				return `${formatedYear}-${formatedMonth}-${formatedDate} ${formatedHours}:${formatedMinutes}:${formatedSeconds}`;
			case "yyyy.MM.dd":
				return `${formatedYear}.${formatedMonth}.${formatedDate}`;
			case "dd.MM.yyyy":
				return `${formatedYear}.${formatedMonth}.${formatedDate}`;
		}
	}


	export function parseDate(date: string): Date {
		return new Date(Date.parse(date));
	}

	export function addLeadZero(data: string): string {
		return data.length == 2 ? data : "0" + data;
	}

	export type format = "yyyy-MM-dd HH:mm:ss" | "yyyy.MM.dd" | "dd.MM.yyyy";


	/**
	 * Возвращает расширение файла
	 * @param fileName
	 */
	export function getFileExtension(fileName: string): string {
		if (fileName.indexOf("\\") >= 0)
			fileName = fileName.substring(fileName.lastIndexOf("\\") + 1);

		if (fileName.indexOf(".") >= 0)
			return fileName.substring(fileName.lastIndexOf(".") + 1)
		else
			return "";
	}

	/**
	 * Возвращает имя файла
	 * @param filePath
	 */
	export function getFileName(filePath: string): string {
		if (filePath.indexOf("\\") >= 0)
			return filePath.substring(filePath.lastIndexOf("\\") + 1);
		return filePath;		
	}

	/**
	 * Нормализуем имя организации
	 * @param orgName
	 * @param userName
	 */
	export function normalizeOrgName(orgName: string, ...userNames: string[]) {
		if (orgName == undefined)
			return orgName;
		
		if (orgName.search(/индивидуальный предприниматель |ип /i) === 0) 
			return orgName.replace(/индивидуальный предприниматель |ип /i, "ИП ");

		let orgNameAsUserName = userNames.find(x => x === orgName);
		if (orgNameAsUserName != undefined)
			return "ИП " + orgName;

		if (/^\S+\s\S\.?\s?\S\.?$/.test(orgName)) {
			let orgNameWithoutDots = orgName.substring(0, orgName.indexOf(" ") + 1) + orgName.substring(orgName.indexOf(" ") + 1).replace(/\.?\s*/g, "");
			let orgNameAsShortUserName = userNames.find(x => getUserShortName(x).replace(".", "") === orgName.replace(/\.\s+/, ""));
			if (orgNameAsShortUserName != undefined)
				return "ИП " + orgName;
		}
		
		return orgName;
	}

	/**
	 * Получим короткое имя пользователя
	 * @param userName
	 */
	export function getUserShortName(userName: string) {
		let names = userName.split(" ");
		
		for (let i = 1; i < names.length; i++)
			names[i] = names[i].substring(0, 1) + ".";
		return names[0] + " " + (names.length >= 2 ? names[1] : "") + (names.length >= 3 ? names[2] : "");
	}
}