import * as React from "react"
import * as ReactDOM from "react-dom"
import * as classNames from "classnames";

/**
 * ��� � ��������. ��� �������� � ������ ���������� �����������. 
 * ��� ���������� ������� ������ ����������� ����� ����������� ���� ������
 */
export class Toolbar extends React.Component<ToolbarProps, ToolbarState> {
	constructor(props: ToolbarProps) {
		super(props);

		this.buttonOnClick = this.buttonOnClick.bind(this);
		
		this.state = this.props;
    }

	/**
	 * ���������� ������� �� ������ � ������ name. ���������� �������������� ������������ ������� buttonOnClick �� ������.
	 * ������������� ����� �������� ������ � �������� ������������ �������� ������� buttonOnClick.
	 * @param name
	 */
	private buttonOnClick(name: string): void {
		if (this.props.buttons.filter(x => x.name === name)[0].isDisabled)
			return;

		this.setState({ activeButtonName: name});

		this.props.buttonOnClick(name);		
	}

	/**
	 * ��������� ��������� � ����������� ����� ������� ����������
	 * @param nextProps
	 */
	public componentWillReceiveProps(nextProps: ToolbarProps) {
		const state = this.state;
		let newState = {} as ToolbarState;
		let isNeedUpdateState = false;

		
		if (isNeedUpdateState) {
			this.setState(newState);
		}
	}

	/**
	 * ��������� ����� �� ��������� ������. ���� ������ ������ ������ �� ��������� � �������� ������ �� ���������,
	 * �� ����������� true. ���� � �������� �������� �����-�� ������ 
	 * �� ����� �������������� ������ �� �������  �� ��������� (�������� ��������� � inputData), 
	 * �� ������������ true. ����� ������������ false.
	 * @param newButtons
	 */
	private isNeedUpdateButtons(newButtons: ToolbarButtonData[]): boolean {
		const buttons = this.props.buttons;
		if (newButtons.length !== buttons.length)
			return true;
		for (let i = 0; i < newButtons.length; i++) {
			if (newButtons[i] !== buttons[i]) {
				return true;
			}
		}
		return false;
	}

	/**
	 * ��������� �������� �� ������
	 * @param button
	 */
	private isButtonActive(button: ToolbarButtonData): boolean {
		return button.name === this.props.activeButtonName;
	}

	/**
	 * @internal
	 */
	public render() {
		const props = this.props;
		const buttons = this.props.buttons;
		return (
			<div className={ props.className + " a2-toolbar" }>
				<ul className="a2-toolbar-buttons">
					{buttons.map((b, i) => <ToolbarButton key={b.name}
						name={b.name}
						label={b.label}
						className={b.className}
						isDisabled={b.isDisabled}
						isActive={this.isButtonActive(b)}
						onClick={this.buttonOnClick}
						elementJsx={b.elementJsx}
					/>)}
				</ul>
            </div>
        );
    }
}

/**
 * ��������� ������
 * @param props
 */
const ToolbarButton = (props: ToolbarButtonProps)  => {	
    // ������� ��� ������
    let className: any[] = ["a2-toolbar-button-holder", props.className, props.name];
	if (props.isDisabled) 
		className.push("disabled");
	else 
		className.push({ "active": props.isActive});
	//
	return (
		<li className={classNames(className)}>
			{props.elementJsx ? props.elementJsx : 
				<a className="a2-toolbar-button" onClick={e => { e.preventDefault(); props.onClick(props.name); }} >
					{props.label}
				</a>}
			
		</li>
	);
	
}

/**
 * ������ ����������� �������� ��� ���������� Toolbar
 */
export interface ToolbarProps {
	/**
	 * �������������� ����� ��� ����������
	 */
	className: string,

	/**
	 * ��� �������� ������
	 */
	activeButtonName?: string,

	/**
	 * ���������� ������� �� ������ � ������ name
	 */
	buttonOnClick(name: string): void,

	/**
	 * ������ ������
	 */
	buttons: ToolbarButtonData[]
}

/**
 * ������ ����������� ��������� ��� ���������� Toolbar
 */
export interface ToolbarState { }

/**
 * ������ ����������� ������ ��� ���������� Toolbar (��������������� ��� ToolbarProps)
 */
export interface ToolbarButtonData {
	/**
	 * ���(���������) ������
	 */
	name: string,

	/**
	 * ������� ��� ������
	 */
	label: string,

	/**
	 * �������������� ����� ��� ������
	 */
	className: string,

	/**
	 * �������� �� ������
	 */
	isDisabled: boolean,

	/**
	 * ������� ������� ����� �������������� ������ ����������� ������. �-� <input type='file'/>
	 */
	elementJsx?: JSX.Element;
}

/**
 * ������ ����������� �������� ��� ���������� ToolbarButton
 */
interface ToolbarButtonProps extends ToolbarButtonData {
	/**
	 * ������� �� ������
	 */ 
	isActive: boolean,

	/**
	 * ���������� ������� �� ������ � ������ name
	 */
	onClick(name: string): void
}





