﻿import * as React from "react"
import * as ReactDOM from "react-dom"
import * as Console from "./Console"
import { Zoomer } from "./Zoomer";

/**
 * Просмоторщик документа (картинок). В данном компоненте padding и borderWidth нужно задавать через свойства, а не css стили,
 * так расчет высоты и скроллинга привязан к этим значениям.
 */
export class ImageViewer extends React.Component<ImageViewerProps, ImageViewerState> {

	constructor(props: ImageViewerProps) {
		super(props);

		this.scrollToPage = this.scrollToPage.bind(this);
		this.onScaleChanged = this.onScaleChanged.bind(this);

		
		this.state = {
			imageViewerOffsetTop: 0,
			imagesContainerOffsetLeft: 0,
			scale: 100
		};
	}

	/**
	 * Устанавливает свойства по умолчанию
	 */
	static defaultProps: ImageViewerProps = {
		images: [],
		imageViewerStyle: {
			padding: 0,
			borderWidth: 0
		},
		pagesContainerStyle: {
			padding: 10,
			borderWidth: 1
		},
		imagesContainerStyle: {
			padding: 0,
			borderWidth: 0
		},
		imagesListStyle: {
			padding: 0,
			borderWidth: 0
		},
		imageItemStyle: {
			padding: 10,
			borderWidth: 1
		}
	};

	/**
	 * Текущий размер картинок в правой колонке. Нужен для расчета значения скроллинга для заданной страницы.
	 */
	private _imageSizes: ImageSize[] = [];

	/**
	 * Основной элемент
	 */
	private _imageViewer: HTMLElement | null; 

	/**
	 * Элемент содержащий список страниц
	 */
	private _pagesContainer: HTMLElement | null;

	/**
	 * Элемент содержащий список картинок
	 */
	private _imagesContainer: HTMLElement | null;

	/**
	 * Скролит до указанной страницы
	 * @param page
	 */
	private scrollToPage(page: number) {
		let pageOffsetTop = getTopPadding(this.props.imagesContainerStyle);

		for (let i = 0; i < this._imageSizes.length; i++) {
			if (i < page - 1) {
				pageOffsetTop += this._imageSizes[i].height + getVertOffset(this.props.imageItemStyle);
			}
		}
		let imagesContainer = this._imagesContainer as HTMLElement;
		imagesContainer.scrollTop = pageOffsetTop;
	}

	/**
	 * Определяет верхнюю позицию элемента imageViewer
	 */
	private setImageViewerOffsetTop(): void {
		let imageViewer = this._imageViewer as HTMLElement;
		this.setState({
			imageViewerOffsetTop: this.topPosition(imageViewer)
		});
	}

	/**
	 * Определяет левую позицию элемента imagesContainer
	 */
	private setImagesContainerOffsetLeft(): void {
		let imagesContainer = this._imagesContainer as HTMLElement;
		this.setState({
			imagesContainerOffsetLeft: this.leftPosition(imagesContainer)
		});
	}
	
	/**
	 * Возвращает верхнюю позицию элемента
	 * @param domElt
	 */
	private topPosition(domElt: HTMLElement): number {
		if (!domElt) {
			return 0;
		}
		return domElt.offsetTop + this.topPosition(domElt.offsetParent as HTMLElement);
	}

	/**
	 * Возвращает левую позицию элемента
	 * @param domElt
	 */
	private leftPosition(domElt: HTMLElement): number {
		if (!domElt) {
			return 0;
		}
		return domElt.offsetLeft + this.leftPosition(domElt.offsetParent as HTMLElement);
	}

	/**
	 * Рисует канвас в левой колонке
	 * @param index
	 * @param image
	 * @param originalWidth ширина картинки
	 * @param originalHeight высота картинки
	 */
	private drawPageCanvas(index: number, image: HTMLImageElement, originalWidth: number, originalHeight: number): void {
		let pageElement = (this._pagesContainer as HTMLElement).children[index] as HTMLElement;
		for (let i = 0; i < pageElement.children.length; i++) {
			pageElement.removeChild(pageElement.children[i]);
		}
		let pageCanvas = document.createElement("canvas");
		let pageCtx = pageCanvas.getContext("2d");
		let width = pageElement ? parseInt(window.getComputedStyle(pageElement).width || "0") : 0;

		let ratio = originalWidth / width;
		let height = originalHeight / ratio;

		pageElement.style.position = "relative";

		pageCanvas.style.position = "absolute";
		pageCanvas.width = width;
		pageCanvas.height = height;
		pageCanvas.style.top = "50%";
		pageCanvas.style.left = "0";
		pageCanvas.style.marginTop = "-" + (height / 2) + "px";
		pageCtx && pageCtx.drawImage(image, 0, 0, width, height);

		pageElement.appendChild(pageCanvas);

		

		
	}

	/**
	 * Рисует канвас в правой колонке
	 * @param index
	 * @param image
	 * @param originalWidth ширина картинки
	 * @param originalHeight высота картинки
	 * @param scale
	 */
	private drawImageCanvas(index: number, image: HTMLImageElement, originalWidth: number, originalHeight: number, scale: number): number[] {
		let imageElement = (this._imagesContainer as HTMLElement).children[index] as HTMLElement;
		for (let i = 0; i < imageElement.children.length; i++) {
			imageElement.removeChild(imageElement.children[i]);
		}
		let imageCanvas = document.createElement("canvas");
		let imageCtx = imageCanvas.getContext("2d");
		
		let width = (image.width / 100) * scale;
		let height = (image.height / 100) * scale;

		imageElement.style.width = width + "px";
		imageElement.style.height = height + "px";

		imageCanvas.width = width;
		imageCanvas.height = height;
		imageCanvas.style.display = "block"; // по умолчанию inline => появляются отступы в 4px
		imageCtx && imageCtx.drawImage(image, 0, 0, width, height);
		imageElement.appendChild(imageCanvas);

		return [width, height];
	}

	/**
	 * Обработчик события срабатывает после загрузки изображения. Рисует канвасы в левой и правой колонках
	 * @param index
	 * @param image
	 * @param scale
	 * @param imageSizes
	 * @param isNeedDrawPages
	 */
	private imageOnLoad(index: number, image: HTMLImageElement, scale: number, imageSizes: ImageSize[], isNeedDrawPages: boolean) {
		let originalWidth = image.width;
		let originalHeight = image.height;
		if (isNeedDrawPages) {
			this.drawPageCanvas(index, image, originalWidth, originalHeight);
		}		
		let size = this.drawImageCanvas(index, image, originalWidth, originalHeight, scale);
		
		imageSizes[index] = {
			width: size[0],
			height: size[1]
		};
	}

	/**
	 * Рисует канвасы в левой и правой колонках
	 */
	private drawImages(scale: number, imageSizes: ImageSize[], isNeedDrawPages: boolean): void {
		this.props.images.forEach((x, i) => {
			let image = new Image();
			image.onload = () => { this.imageOnLoad(i, image, scale, imageSizes, isNeedDrawPages) };
			image.src = "data:image/jpeg;base64," + this.props.images[i].base64;
		}); 
	}
	

	/**
	 * Устанавливает новый масштаб (знаяение в процентах). 
	 * Все канвасы в правой колонке перерисовываются.
	 */
	private onScaleChanged(scale: number): void {
		if (scale === this.state.scale)
			return;

		this.setState({
			scale: scale
		});
		this.drawImages(scale, this._imageSizes, false);
	}
	
	/**
	 * @internal
	 */
	public componentWillReceiveProps(nextProps: ImageViewerProps) {
		Console.log("componentWillReceiveProps")
		const state = this.state;
		let newState = {} as ImageViewerState;
		let isNeedUpdateState = false;

		if (nextProps.images !== this.props.images) {
			Console.log(this.props.images);
			newState.scale = 100;
			this._imageSizes.length = 0;
			if (this._pagesContainer !== null)
				this._pagesContainer.scrollTop = 0;
			if (this._imagesContainer !== null)
				this._imagesContainer.scrollTop = 0;
			//
			isNeedUpdateState = true;
		}

		if (isNeedUpdateState) {
			this.setState(newState);
		}
	}

	/**
	 * @internal
	 */
	public componentDidMount() {
		// убираем у документа скролл. Иначе могут быть конфликты.
		document.documentElement.style.overflow = "hidden";

		this.setImageViewerOffsetTop();
		this.setImagesContainerOffsetLeft();
	}

	/**
	 * @internal
	 */
	public componentDidUpdate() {
		this.drawImages(this.state.scale, this._imageSizes, true);		
	}
	
	/**
	 * @internal
	 */
	public render() {
		let props = this.props;
		let state = this.state;
		
		let pagesContainerHeightOffset = state.imageViewerOffsetTop +
			getVertOffset(props.imageViewerStyle) +
			getVertOffset(props.pagesContainerStyle);
		let pagesContainerHeight = `calc(100vh - ${pagesContainerHeightOffset}px)`;
		
		let imagesContainerHeightOffset = state.imageViewerOffsetTop +
			getVertOffset(props.imageViewerStyle) +
			getVertOffset(props.imagesContainerStyle);
		let imagesContainerHeight = `calc(100vh - ${imagesContainerHeightOffset}px)`;
		
		let imagesContainerWidthOffset = state.imagesContainerOffsetLeft +
			getHorizOffset(props.imagesContainerStyle) +
			getRightPadding(props.imageViewerStyle) + getRightBorderWidth(props.imageViewerStyle);
		let imagesContainerWidth = `calc(100vw - ${imagesContainerWidthOffset}px)`;


		return (
			<div className="a2-image_viewer-wrapper">
				<div className="a2-image_viewer-scale">
					<div className="a2-image_viewer-scale-label">
						Масштаб 
						<span> {state.scale}%</span>
					</div>
					<div className="a2-image_viewer-scale-zoomer">
						<Zoomer onScaleChanged={this.onScaleChanged} doReset={state.scale === 100} />
					</div>
				</div>
				<table className="a2-image_viewer" ref={x => this._imageViewer = x}
					style={{ margin: "0px", borderWidth: getAllBorderWidth(props.imageViewerStyle), padding: getAllPadding(props.imageViewerStyle) }}
				><tbody><tr>
					<td style={{ margin: "0px", borderWidth: "0px", padding: "0px", verticalAlign: "top" }}>
							<div className="a2-image_viewer-pages" ref={x => this._pagesContainer = x}
								style={{ overflowY: "scroll", overflowX: "hidden", display: "inline-block", margin: "0px", height: pagesContainerHeight, borderWidth: getAllBorderWidth(props.pagesContainerStyle), padding: getAllPadding(props.pagesContainerStyle) }}
						>
							{props.images.map((x, i) => <div className="a2-image_viewer-page" key={i} onClick={e => this.scrollToPage(i + 1)}></div>)}
						</div>
					</td>
					<td style={{ margin: "0px", borderWidth: "0px", padding: "0px", verticalAlign: "top" }}>
							<div className="a2-image_viewer-images" ref={x => this._imagesContainer = x}
                                style={{ overflow: "scroll", display: "inline-block", margin: "5px 0px 0px 0px", height: imagesContainerHeight, width: imagesContainerWidth, borderWidth: getAllBorderWidth(props.imagesContainerStyle), padding: getAllPadding(props.imagesContainerStyle) }}>
							
							{props.images.map(
									(x, i) => <div className="a2-image_viewer-image" key={i}
										style={{ margin: "0px", borderWidth: getAllBorderWidth(props.imageItemStyle), padding: getAllPadding(props.imageItemStyle) }}
								></div>
							)}
						</div>
					</td>
				</tr></tbody></table>
			</div>
		);
	}
}


/************************************
 * Функции для работы со стилями
 ***********************************/
/**
 * Получить сумму всех вертикальных отступов и границ
 */
function getVertOffset(style: ElementStyle | undefined): number {
	return getTopPadding(style) + getBottomPadding(style) + getTopBorderWidth(style) + getBottomBorderWidth(style);
}

/**
 * Получить сумму всех горизонтальных отступов и границ
 */
function getHorizOffset(style: ElementStyle | undefined): number {
	return getRightPadding(style) + getLeftPadding(style) + getRightBorderWidth(style) + getLeftBorderWidth(style);
}

/**
 * Получить верхний отступ
 */
function getTopPadding(style: ElementStyle | undefined): number {
	return getPadding(style, BoundType.Top);
}

/**
 * Получить правый отступ
 */
function getRightPadding(style: ElementStyle | undefined): number {
	return getPadding(style, BoundType.Right);
}

/**
 * Получить нижний отступ
 */
function getBottomPadding(style: ElementStyle | undefined): number {
	return getPadding(style, BoundType.Bottom);
}

/**
 * Получить левый отступ
 */
function getLeftPadding(style: ElementStyle | undefined): number {
	return getPadding(style, BoundType.Left);
}

/**
 * Получить указанный отступ (по BoundType)
 */
function getPadding(style: ElementStyle | undefined, bound: BoundType): number {
	if (style === undefined || style.padding === undefined)
		return 0;

	if (typeof style.padding === "number")
		return style.padding;

	return getBound(style.padding, bound);
}

/**
 * Получить строку со всеми отступами (типа 10px 5px 10px 5px)
 */
function getAllPadding(style: ElementStyle | undefined): string {
	return `${getTopPadding(style)}px ${getRightPadding(style)}px ${getBottomPadding(style)}px ${getLeftPadding(style)}px`;
}

/**
 * Получить верхнюю границу
 */
function getTopBorderWidth(style: ElementStyle | undefined): number {
	return getBorderWidth(style, BoundType.Top);
}

/**
 * Получить правую границу
 */
function getRightBorderWidth(style: ElementStyle | undefined): number {
	return getBorderWidth(style, BoundType.Right);
}

/**
 * Получить нижнюю границу
 */
function getBottomBorderWidth(style: ElementStyle | undefined): number {
	return getBorderWidth(style, BoundType.Bottom);
}

/**
 * Получить левую границу
 */
function getLeftBorderWidth(style: ElementStyle | undefined): number {
	return getBorderWidth(style, BoundType.Left);
}

/**
 * Получить указанную границу (по BoundType)
 */
function getBorderWidth(style: ElementStyle | undefined, bound: BoundType): number {
	if (style === undefined || style.borderWidth === undefined)
		return 0;

	if (typeof style.borderWidth === "number")
		return style.borderWidth;

	return getBound(style.borderWidth, bound);
}

/**
 * Получить строку со всеми границами (типа 10px 5px 10px 5px)
 */
function getAllBorderWidth(style: ElementStyle | undefined): string {
	return `${getTopBorderWidth(style)}px ${getRightBorderWidth(style)}px ${getBottomBorderWidth(style)}px ${getLeftBorderWidth(style)}px`;
}

/**
 * Получить значение (по BoundType)
 */
function getBound(bounds: Bounds, type: BoundType): number {
	switch (type) {
		case BoundType.Top:
			return bounds.top || 0;
		case BoundType.Right:
			return bounds.right || 0;
		case BoundType.Bottom:
			return bounds.bottom || 0;
		case BoundType.Left:
			return bounds.right || 0;
	}
}


/**
 * Модель описывающая свойства компонента ImageViewer
 */
export interface ImageViewerProps {
	/**
	 * Список данных об изображениях 
	 */
	images: ImageData[],

	/**
	 * Стили для элемента imageViewer
	 */
	imageViewerStyle?: ElementStyle,

	/**
	 * Стили для элемента pagesContainer
	 */
	pagesContainerStyle?: ElementStyle,

	/**
	 * Стили для элемента imagesContainer
	 */
	imagesContainerStyle?: ElementStyle,

	/**
	 * Стили для элемента imagesList
	 */
	imagesListStyle?: ElementStyle,

	/**
	 * Стили для элемента imageItem
	 */
	imageItemStyle?: ElementStyle
}

/**
 * Описание возможно задаваемых стилей для элементов компонента
 */
interface ElementStyle {
	/**
	 * Величина отступов
	 */
	padding?: number | Bounds,

	/**
	 * Величина границ 
	 */
	borderWidth?: number | Bounds
}

/**
 * Значения для соответвующих направлений
 */
interface Bounds {
	top?: number,
	right?: number,
	bottom?: number,
	left?: number,
}

/**
 * Допустимые направления
 */
enum BoundType {
	Top, Right, Bottom, Left
}

/**
 * Модель описывающая состояние компонента ImageViewer
 */
interface ImageViewerState {
	/**
	 * Масштаб изображений
	 */
	scale: number,

	/**
	 * Верхнее смещение элемента imageViewer относительно document
	 */
	imageViewerOffsetTop: number,	

	/**
	 * Смещение элемента слева imagesContainer относительно document
	 */
	imagesContainerOffsetLeft: number,	
}



/**
 * Модель описывающая данные изображения поступающие на вход компонента
 */
export interface ImageData {
	/**
	 * Картинка в кодировке base64
	 */
	base64: string
}

/**
 * Модель описывающая текущие размеры изображения в правой колонке. Эти размеры необходимы при расчете скроллинга на указанную страницу
 */
interface ImageSize {
	/**
	 * Ширина картинки
	 */
	width: number,

	/**
	 * Высота картинки
	 */
	height: number
}

