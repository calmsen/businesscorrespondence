﻿import * as React from "react";


/**
 * Модель описывающая данные пользователя
 */
export interface User {
	/**
	 * Фамилия полное, имя и отчество заглавные буквы 
	 */
	shortName: string,

	/**
	 * Полное имя контрагента
	 */
	fullName: string,

	/**
	 * Электронный адрес контрагента
	 */
	email?: string,

	/**
	 * Номер телефона контрагента
	 */
	phone?: string,

	/**
	 * Удостоверен ли контрагент
	 */
	isConfirmed: boolean
}


/**
* Параметры верхней панели.
*/
export interface TopPanelProps {

    /**
    * Текущий пользователь.
    */
    user?: User;
    /**
     * Вызывается при клике по кнопке "Выйти из Деловой сети".
     */
    onLogOut(): void;
}

/**
* Состояние верхней панели.
*/
export interface TopPanelState {
    /**
    * Значение true, если панель находится в развернутом состоянии;
    * иначе - значение false.
    */
    opened: boolean;
}

/**
 * Верхняя панель.
 */
export class TopPanel extends React.Component<TopPanelProps, TopPanelState> {
    constructor(props: TopPanelProps) {
        super(props);

        this.state = {
			opened: false
        };

        this.onUserButtonClick = this.onUserButtonClick.bind(this);
        this.onChangePassword = this.onChangePassword.bind(this);
        this.onChangePhone = this.onChangePhone.bind(this);
        this.onServiceClick = this.onServiceClick.bind(this);
        this.onClosing = this.onClosing.bind(this);
        this.stopClickBubble = this.stopClickBubble.bind(this);
        this.onToolbarAction = this.onToolbarAction.bind(this);
    }

    public render() {
        const { user } = this.props;
        const { opened } = this.state;
        return (
            <div className={`a2-application-header ${opened ? "opened" : ""}`} onMouseDown={this.stopClickBubble}>
                <div className="HeaderPanel">
                    <div className="header_logo">
                        <img src="Content/Images/logo.png" />
                        <div>Деловая и Служебная Переписка</div>
					</div>
					{user && <Toolbar onAction={this.onToolbarAction} />}
					{user && <UserButton user={user} onClick={this.onUserButtonClick} />} 
                </div>
                {
					opened && user &&
                    <ContentPanel user={user}
                        onChangePassword={this.onChangePassword}
                        onChangePhone={this.onChangePhone}
                        onExit={this.props.onLogOut}
                        onServiceClick={this.onServiceClick}
                        onClosing={this.onClosing} />
                }
            </div>
        );
    }


    private onUserButtonClick() {
        this.setState({
            opened: !this.state.opened
        });
    }

    private onClosing() {
        this.setState({
            opened: false
        });
    }

    private onChangePhone(): void {

    }

    private onChangePassword(): void {

    }

    private onServiceClick(serviceID: string): void {

    }

	private onToolbarAction(actionID: "help" | "settings") {
        if (actionID === "settings")
            this.onUserButtonClick();
    }

    private stopClickBubble(event: React.MouseEvent<HTMLDivElement>) {
        event.stopPropagation();
        event.nativeEvent.stopImmediatePropagation();
    }
}

interface ToolbarProps {
    onAction(actionID: "help" | "settings"): void;
}

const Toolbar = (props: ToolbarProps) => {
    return (
        <div className="header_tools">
            <a href="#" onClick={e => props.onAction("help")}><img src="Content/Images/Icons/help.png" /></a>
            <a href="#" onClick={e => props.onAction("settings")}><img src="Content/Images/Icons/setting.png" /></a>
        </div>
    );
}

interface UserButtonProps {
    user: User;
    onClick(): void;
}

const UserButton = (props: UserButtonProps) => {
    const { user, onClick } = props;

    return (
        <div className="header_user" onClick={onClick}>
            {user.shortName}
        </div>
    );
}

interface ContentPanelProps {
    user: User;
    onChangePhone(): void;
    onChangePassword(): void;
    onExit(): void;
    onServiceClick(serviceID: string): void;
    onClosing(): void;
}

class ContentPanel extends React.Component<ContentPanelProps, {}> {
    constructor(props: ContentPanelProps) {
        super(props);

        this.handleMouseDown = this.handleMouseDown.bind(this);
    }

    public render() {
        const { user, onChangePhone, onChangePassword, onExit, onServiceClick } = this.props;

        return (
            <div className="ContentPanel" onMouseDown={e => this.stopClickBubble(e)} >
                <AccountPanel user={user} onChangePassword={onChangePassword} onChangePhone={onChangePhone} onExit={onExit} />
                <ServicesPanel onServiceClick={onServiceClick} />
            </div>
        )
    }

    public componentDidMount() {
        document.addEventListener("mousedown", this.handleMouseDown);
    }

    public componentWillUnmount() {
        document.removeEventListener("mousedown", this.handleMouseDown);
    }

    private handleMouseDown(event: MouseEvent) {
        this.props.onClosing();
    }

    private stopClickBubble(event: React.MouseEvent<HTMLDivElement>) {
        event.stopPropagation();
        event.nativeEvent.stopImmediatePropagation();
    }
}

interface AccountPanelProps {
    user: User;
    onChangePhone(): void;
    onChangePassword(): void;
    onExit(): void;
}

const AccountPanel = (props: AccountPanelProps) => {
    const { user, onChangePhone, onChangePassword, onExit } = props;

    return (
		<div className={`AccountPanel ${user.isConfirmed ? "confirmed" : ""}`}>
            <div className="icon"></div>
            <div className="title">
				<div className="label">{user.fullName}</div>
				<div className={`status ${(user.isConfirmed ? "confirmed" : "unconfirmed")}`}>
					{user.isConfirmed ? "Удостоверенный пользователь" : "Неудостоверенный пользователь"}
                </div>
            </div>
			<div className="contact email">{user.email}</div>
			<div className="contact phone">{user.phone}</div>
            <ul className="toolbar">
                <li className="change-phone" onClick={onChangePhone}>Сменить номер телефона</li>
                <li className="change-pwd" onClick={onChangePassword}>Сменить пароль</li>
                <li className="exit" onClick={onExit}>Выйти из Деловой сети</li>
            </ul>
            <div style={{ clear: "both" }}></div>
        </div>
    );
}

interface ServicesPanelProps {
    onServiceClick(serviceID: string): void;
}

const ServicesPanel = (props: ServicesPanelProps) => {
    return (
        <ul className="ServicesPanel">
            <ServiceCard id="pcab"
                title="Мой атлас"
                description="Личный кабинет Деловой сети"
                className="pcab"
                href="https://lk.atlas-2.ru" />
            <ServiceCard id="magazine"
                title="Журнал «Деловая сеть»"
                description="Электронное периодическое издание"
                className="magazine"
                href="http://деловаясеть.рф" />
        </ul>
    );
}

interface ServiceCardProps {
    id: string;
    title: string;
    description: string;
    className?: string;
    href?: string;
    onClick?(id: string): void;
}

const ServiceCard = (props: ServiceCardProps) => {
    const { id, title, description, className, href, onClick } = props;

    return (
        <li className={className}>
            <a href={href || "javascript: void 0"} onClick={() => onClick && onClick(props.id)}>
                <span className="title">{title}</span>
                <span className="description">{description}</span>
            </a>
        </li>
    );
}