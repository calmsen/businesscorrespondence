﻿import * as React from "react"
import * as ReactDOM from "react-dom"
import { CSSProperties } from "react";


/**
 * Представление для загрузчика по умолчанию
 */
let defaultLoader: JSX.Element = <div>Загрузка...</div>;

/**
 * Контейнер который позволяет подгрузить контент на событие скроллинга 
 */
export class InfiniteScroll extends React.Component<InfiniteScrollProps, InfiniteScrollState> {
	constructor(props: InfiniteScrollProps) {
		super(props);
		this.scrollListener = this.scrollListener.bind(this);
		this.state = {
			containerOffsetTop: 0,
		};
	}

	/**
	 * Установим свойства по умолчанию
	 */
	static defaultProps: InfiniteScrollProps = {
		pageLoaded: 0,
		hasMore: false,
		loadMore: async (page: number) => { },
		threshold: 150,
		loader: defaultLoader
	};

	/**
	 * Последняя загруженная страница
	 */
	private _pageLoaded: number; 

	/**
	 * Подгрузка страницы в прогрессе
	 */
	private _loadingInProgress = false;

	/**
	 * Блокировка подгрузки
	 */
	private _lockLoading = false;

	/**
	 * Элемент container
	 */
	private _containerElement: HTMLElement | null; 

	/**
	 * Элемент content
	 */
	private _contentElement: HTMLElement | null; 

	/**
	 * Возвращает верхнюю позицию элемента
	 * @param domElt
	 */
	private topPosition(domElt: HTMLElement): number {
		if (!domElt) {
			return 0;
		}
		return domElt.offsetTop + this.topPosition(domElt.offsetParent as HTMLElement);
	}

	private getContainerElement(): HTMLElement{
		return this.props.scopeType === ScopeType.Self
			? this._containerElement as HTMLElement
			: document.documentElement || document.body.parentNode;
	}

	/**
	 * Прослушивает движение сроллинга и когда значение скрола меньше threshold,
	 * подгружает новую страницу.
	 */
	private async scrollListener() {
		if (this._lockLoading) {
			let containerElement = this.getContainerElement();
			this._lockLoading = !(containerElement.scrollTop === 0);
			return;
		}

		if (this._loadingInProgress || this._pageLoaded === 0 || !this.props.hasMore) {
			return;
		}

		let containerElement = this.getContainerElement();

		let contentElement = this.props.scopeType === ScopeType.Self
			? this._contentElement as HTMLElement
			: document.body;

		if (containerElement === undefined || contentElement === undefined)
			return;
		
		let contentHeight = parseInt(window.getComputedStyle(contentElement).height || "0");

		if (contentHeight - containerElement.clientHeight - Number(this.props.threshold) < containerElement.scrollTop) {
			this._loadingInProgress = true;
			await this.props.loadMore(this._pageLoaded += 1);
			this._loadingInProgress = false;
		}
	}

	/**
	 * Устанавливает слушателя на события scroll и resize
	 */
	private attachScrollListener() {
		if (this.props.scopeType === ScopeType.Self) {
			let containerElement = this._containerElement as HTMLElement;
			containerElement.addEventListener('scroll', this.scrollListener);
			containerElement.addEventListener('resize', this.scrollListener);
		}
		else {
			window.addEventListener('scroll', this.scrollListener);
			window.addEventListener('resize', this.scrollListener);
		}
	}

	/**
	 * Удаляет слушателя из событий scroll и resize
	 */
	private detachScrollListener() {
		if (this.props.scopeType === ScopeType.Self) {
			let containerElement = this._containerElement as HTMLElement;
			containerElement.removeEventListener('scroll', this.scrollListener);
			containerElement.removeEventListener('resize', this.scrollListener);
		}
		else {
			window.removeEventListener('scroll', this.scrollListener);
			window.removeEventListener('resize', this.scrollListener);
		}
	}

	/**
	 * Определяет верхнюю позицию элемента container
	 */
	private setContainerOffsetTop(): void {
		let container = this._containerElement as HTMLElement;
		this.setState({
			containerOffsetTop: this.topPosition(container)
		});
	}

	/**
	 * @internal
	 */
	public componentDidMount() {
		this.setContainerOffsetTop();
		this._pageLoaded = this.props.pageLoaded || 0;
		this.attachScrollListener();
	}

	/**
	 * @internal
	 */
	public componentWillUnmount() {
		this.detachScrollListener();
	}
	
	/**
	 * @internal
	 */
	public componentWillReceiveProps(nextProps: InfiniteScrollProps) {
		const state = this.state;
		let newState = {} as InfiniteScrollState;
		let isNeedUpdateState = false;
		if (nextProps.pageLoaded !== this._pageLoaded) {
			let pageLoaded = nextProps.pageLoaded || 0;
			
			if (pageLoaded === 0) {
				this._lockLoading = true;

				let containerElement = this.getContainerElement();
				containerElement.scrollTop = 0;
			}			

			this._pageLoaded = pageLoaded;
		}

		if (isNeedUpdateState) {
			this.setState(newState);
		}
	}

	/**
	 * @internal
	 */
	public render () {
		let props = this.props;
		let state = this.state;

		let style: CSSProperties = {};
		if (props.scopeType === ScopeType.Self) {
			style.overflowY = "scroll";
			style.height = `calc(100vh - ${state.containerOffsetTop}px)`;
		}

		return (
			<div ref={x => this._containerElement = x} style={style}>
				<div ref={x => this._contentElement = x}>
				{props.children}
				{this.props.hasMore && props.loader}
				</div>
			</div>
		);
	}

}

/**
 * Устанавливает представление для loader'а по умолчанию
 * @param loader
 */
export function setDefaultLoader(loader: JSX.Element) {
	defaultLoader = loader;
};

/**
 * Модель описывающая свойства компонента InfiniteScroll
 */
export interface InfiniteScrollProps {
	/**
	 * Последняя загруженная страница(текущая страница)
	 */
	pageLoaded?: number,

	/**
	 * Нужно ли загружать следующие письма
	 */
	hasMore?: boolean,

	/**
	 * Загружает письма для указанной страницы 
	 * @param page страница которую нужно подгрузить
	 */
	loadMore(page: number): Promise<void>,

	/**
	 * Порог при котором срабатывает вызов loadMore
	 */
	threshold?: number,

	/**
	 * Представление для загрузчика
	 */
	loader?: JSX.Element,

	/**
	 * Тип элемента, внутри которого происходит скроллинг. 
	 */
	scopeType?: ScopeType
}

/**
 * Модель описывающая состояние компонента InfiniteScroll
 */
export interface InfiniteScrollState {
	/**
	 * Верхнее смещение элемента containerElement относительно document
	 */
	containerOffsetTop: number,
}

/**
 * Типы элементов, внутри которых происходит скроллинг. 
 */
export enum ScopeType {
	/**
	 * Cкроллинг устанавливается у самого компонента
	 */
	Self,

	/**
	 * используется скролл окна браузера
	 */
	Document
}


