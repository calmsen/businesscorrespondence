﻿import * as React from "react"
import * as ReactDOM from "react-dom"
import * as classNames from "classnames";

/**
 * Вертикальное меню. Все свойства в данном компоненте обновляемые. 
 * При обновлении свойств кнопок обязательно нужно пересоздать весь объект
 */
export class VerticalMenu extends React.Component<VerticalMenuProps, VerticalMenuState> {
	constructor(props: VerticalMenuProps) {
		super(props);

		this.buttonOnClick = this.buttonOnClick.bind(this);

		this.state = {
			activeButtonName: props.activeButtonName
		};
	}


	/**
	 * Обработчик события на кнопке с именем name. Обработчик переопределяет оригинальное функцию buttonOnClick на кнопке.
	 * Устанавливает новую активную кнопку и вызывает оригинальную обратную функцию buttonOnClick.
	 * @param name
	 */
	private buttonOnClick(name: string): void {
		if (this.props.buttons.filter(x => x.name === name)[0].isDisabled)
			return;

		this.setState({ activeButtonName: name });

		this.props.buttonOnClick(name);
	}

	/**
	 * Проверяет активна ли данная кнопка
	 * @param button
	 */
	private isButtonActive(button: VerticalMenuButtonData): boolean {
		return button.name === this.state.activeButtonName;
	}

	/**
	 * @internal
	 */
	public componentWillReceiveProps(nextProps: VerticalMenuProps) {
		const state = this.state;
		let newState = {} as VerticalMenuState;
		let isNeedUpdateState = false;

		if (nextProps.activeButtonName !== state.activeButtonName) {
			newState.activeButtonName = nextProps.activeButtonName;
			isNeedUpdateState = true;
		}

		if (isNeedUpdateState) {
			this.setState(newState);
		}
	}

	/**
	 * @internal
	 */
	public render() {
		const props = this.props;

		return (
			<div className={props.className + " a2-vertical_menu" } >
				<ul className="a2-vertical_menu-buttons" >
				{
						props.buttons.map((b, i) => <VerticalMenuButton key={ b.name }
						name = { b.name }
						label = { b.label }
						className = { b.className }
						isDisabled = { b.isDisabled }
						isActive = { this.isButtonActive(b) }
						onClick = { this.buttonOnClick }
						/>)
				}
				</ul>
			</div>
        );
	}
}

/**
 * Компонент кнопки
 * @param props
 */
const VerticalMenuButton = (props: VerticalMenuButtonProps) => {
	
    // получим имя класса
    let className: any[] = [props.className, "a2-vertical_menu-button-holder", props.name];
	if (props.isDisabled) 
		className.push("disabled");
	else 
		className.push({ "active": props.isActive});
	
	return (
        <li className={classNames(className)} onClick={e => { e.preventDefault(); props.onClick(props.name); }}>
			<a id={props.name} className="a2-vertical_menu-button">
			{ props.label }
			</a>
		</li>
	);
}

/**
 * Модель описывающая свойства для компонента VerticalMenu
 */
export interface VerticalMenuProps {
	/**
	 * Дополнительный класс для компонента
	 */
	className: string,

	/**
	 * Имя активной кнопки
	 */
	activeButtonName?: string,

	/**
	 * Обработчик события на кнопке с именем name
	 */
	buttonOnClick(name: string): void,

	/**
	 * Список кнопок
	 */
	buttons: VerticalMenuButtonData[]
}

/**
 * Модель описывающая состояние для компонента VerticalMenu
 */
export interface VerticalMenuState {
	activeButtonName?: string
}

/**
 * Модель описывающая кнопку для компонента VerticalMenuButton (непосредственно для VerticalMenuButtonProps)
 */
export interface VerticalMenuButtonData {
	/**
	 * Имя(системное) кнопки
	 */
	name: string,

	/**
	 * Надпись для кнопки
	 */
	label: string,

	/**
	 * Дополнительный класс для кнопки
	 */
	className: string,

	/**
	 * Доступна ли кнопка
	 */
	isDisabled: boolean,
}

/**
 * Модель описывающая свойства для компонента VerticalMenuButton
 */
interface VerticalMenuButtonProps extends VerticalMenuButtonData {
	/**
	 * Активна ли кнопка
	 */
	isActive: boolean,

	/**
	 * Обработчик события на кнопке с именем name
	 */
	onClick(name: string): void
}





