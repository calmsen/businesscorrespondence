﻿import * as React from "react"
import * as ReactDOM from "react-dom"
import { PopUp, popUp } from "./Modal";

/**
 * Окно с одной кнопкой "OK" - является заменой нативному javascript окну alert
 * @param message
 */
export function Alert(message: string): Promise<void> {
	return new Promise((resolve, reject) => {
		let alertPopUp: PopUp = popUp(
			<div className="a2-alert-popup">
				<div className="a2-alert-popup-header">
                    <span className="a2-alert-popup-close_btn" onClick={e => { resolve(); alertPopUp.close(); }}>&#10006;</span>
				</div>
				<div className="a2-alert-popup-body">
					{message}
				</div>
				<div className="a2-alert-popup-footer">
					<div className="a2-alert-popup-apply_btn" onClick={e => { resolve(); alertPopUp.close(); }}>OK</div>
				</div>
			</div>
		);
	});	
}


