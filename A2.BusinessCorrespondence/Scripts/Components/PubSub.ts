﻿/** 
 * Издатель / подписчик
 */
class PubSub {
	/**
	 * Обработчики событий
	 */
	private _events: { eventName: string, handler: (data: any) => void }[] = [];

	/**
	 * Добавляет событие
	 * @param eventName
	 * @param handler
	 */
	public subscribe(eventName: string, handler: (data: any) => void) {
		this._events.push({ eventName, handler });
	}

	/**
	 * Вызывает событие и удаляет событие после выполнения
	 * @param eventName
	 * @param data
	 * @param isNeedDeleteEvent
	 */
	public publish(eventName: string, data: any, isNeedDeleteEvent: boolean = true) {
		for (let i = 0; i < this._events.length; i++) {
			if (this._events[i].eventName === eventName) {
				this._events[i].handler(data);
				if (isNeedDeleteEvent)
					this._events.splice(i, 1);
				return;
			}
		}
	}
}

/**
 * Создадим экземпляр класса PubSub
 */
export const pubSub = new PubSub();
