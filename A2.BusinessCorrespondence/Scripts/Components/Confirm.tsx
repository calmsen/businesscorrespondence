﻿import * as React from "react"
import * as ReactDOM from "react-dom"
import { PopUp, popUp } from "./Modal";

/**
 * Окно с двумя кнопками "Да" и "Нет" - является заменой нативному javascript окну confirm
 * @param question
 * @param action
 */
export function Confirm(question: string): Promise<boolean> {
	return new Promise((resolve, reject) => {
		let confirmPopUp: PopUp = popUp(
			<div className="a2-confirm-popup">
				<div className="a2-confirm-popup-header">
                    <span className="a2-confirm-popup-close_btn" onClick={e => { resolve(true); confirmPopUp.close(); }}>&#10006;</span>
				</div>
				<div className="a2-confirm-popup-body">
					{question}
				</div>
				<div className="a2-confirm-popup-footer">
					<span className="a2-confirm-popup-apply_btn" onClick={e => { resolve(true); confirmPopUp.close(); }}>Да</span>
					<span className="a2-confirm-popup-cancel_btn" onClick={e => { resolve(false); confirmPopUp.close(); }}>Нет</span>
				</div>
			</div>
		);
	});	
}


