﻿/**
 * Выводит сообщение и стек вызова метода, если первый аргумент false.
 * Второй параметр допускает преобразование переданных параметров согласно правилам подстановки строк (см. https://developer.mozilla.org/ru/docs/Web/API/console#Using_string_substitutions).
 * 
 * Является безопасным для выполнения в Internet Explorer даже в том случае, когда консоль разработчика закрыта.
 */
export function assert(test?: boolean, message?: any, ...optionalParams: any[]): void;
export function assert() {
	executeConsole("assert", arguments);
}

/**
 * Выводит информационное сообщение.
 * Допускает преобразование переданных параметров согласно правилам подстановки строк (см. https://developer.mozilla.org/ru/docs/Web/API/console#Using_string_substitutions).
 * 
 * Является безопасным для выполнения в Internet Explorer даже в том случае, когда консоль разработчика закрыта.
 */
export function info(message?: any, ...optionalParams: any[]): void;
export function info() {
	executeConsole("info", arguments);
}

/**
 * Общий метод вывода информации.
 * Допускает преобразование переданных параметров согласно правилам подстановки строк (см. https://developer.mozilla.org/ru/docs/Web/API/console#Using_string_substitutions).
 * 
 * Является псевдонимом для log().
 * 
 * Является безопасным для выполнения в Internet Explorer даже в том случае, когда консоль разработчика закрыта. 
 */
export function debug(message?: string, ...optionalParams: any[]): void;
export function debug() {
	executeConsole("debug", arguments);
}

/**
 * Выводит сообщение об ошибке.
 * Допускает преобразование переданных параметров согласно правилам подстановки строк (см. https://developer.mozilla.org/ru/docs/Web/API/console#Using_string_substitutions).
 * 
 * Является безопасным для выполнения в Internet Explorer даже в том случае, когда консоль разработчика закрыта.
 */
export function error(message?: any, ...optionalParams: any[]): void;
export function error() {
	executeConsole("error", arguments);
}

/**
 * Общий метод вывода информации.
 * Допускает преобразование переданных параметров согласно правилам подстановки строк (см. https://developer.mozilla.org/ru/docs/Web/API/console#Using_string_substitutions).
 * 
 * Является безопасным для выполнения в Internet Explorer даже в том случае, когда консоль разработчика закрыта.
 */
export function log(message?: any, ...optionalParams: any[]): void;
export function log(): void {
	executeConsole("log", arguments);
}

/**
 * Выводит стек вызовов.
 * 
 * Является безопасным для выполнения в Internet Explorer даже в том случае, когда консоль разработчика закрыта.
 */
export function trace(): void {
	executeConsole("trace", arguments);
}

/**
 * Выводит предупреждающее сообщение. 
 * Допускает преобразование переданных параметров согласно правилам подстановки строк (см. https://developer.mozilla.org/ru/docs/Web/API/console#Using_string_substitutions).
 * 
 * Является безопасным для выполнения в Internet Explorer даже в том случае, когда консоль разработчика закрыта.
 */
export function warn(message?: any, ...optionalParams: any[]): void;
export function warn(): void {
	executeConsole("warn", arguments);
}

const NATIVE_CONSOLE = window.console as any;

function executeConsole(methodName: string, args: IArguments): void {
	let method: Function = NATIVE_CONSOLE ? NATIVE_CONSOLE[methodName] : null;
	if (method)
		method.apply(NATIVE_CONSOLE, args);
}