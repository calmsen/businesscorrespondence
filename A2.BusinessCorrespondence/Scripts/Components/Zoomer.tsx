﻿import * as React from "react"
import * as ReactDOM from "react-dom"


export class Zoomer extends React.Component<ZoomerProps, ZoomerState> {
	constructor(props: ZoomerProps) {
		super(props);

		this.sliderOnMouseDown = this.sliderOnMouseDown.bind(this);
		this.documentOnMouseUp = this.documentOnMouseUp.bind(this);
		this.documentOnMouseMove = this.documentOnMouseMove.bind(this);
		this.windowOnResize = this.windowOnResize.bind(this);
		this.containerOnClick = this.containerOnClick.bind(this);
		this.mapToScale = this.mapToScale.bind(this);

		this.state = {};
	}
	/**
	 * Определяет захвачен ли слайдер 
	 */
	private _sliderIsGrabbed = false;

	/**
	 * Позиция главного контейнера компонента относительно всего документа
	 */
	private _leftZeroPosition = 0;

	/**
	 * Минимальная позиция слайдера относительно всего документа
	 */
	private _sliderMinLeftPosition = 0;

	/**
	 * Максимальная позиция слайдера относительно всего документа
	 */
	private _sliderMaxLeftPosition = 0;

	/**
	 * Допустимые значения масщтаба 
	 */
	private _availableScales = [
		25, 33, 50, 67, 75, 80, 90, 100, 110, 125, 150, 175, 200, 250, 300, 400
	]

	/**
	 * Ползунок
	 */
	private _sliderElement: HTMLElement | null;

	/**
	 * Левый кругляшок, отвечающий за границу ползунка
	 */
	private _leftCircleElement: HTMLElement | null;

	/**
	 * Правый кругляшок, отвечающий за границу ползунка
	 */
	private _rightCircleElement: HTMLElement | null;

	/**
	 * Линия ко которой двигается ползунок
	 */
	private _lineElement: HTMLElement | null;

    /**
     * Устанавливает события для документа
     */
	private setDocumentEvents(): void {
		//
		document.documentElement.addEventListener("mouseup", this.documentOnMouseUp);
		document.documentElement.addEventListener("mousemove", this.documentOnMouseMove);
		window.addEventListener("resize", this.windowOnResize);

		let elements = [
			this._sliderElement as HTMLElement,
			this._leftCircleElement as HTMLElement,
			this._rightCircleElement as HTMLElement,
			this._lineElement as HTMLElement
		];

		for (let element of elements) {
			element.addEventListener("dragover", this.preventDefault);
			element.addEventListener("drop", this.preventDefault);
			element.addEventListener("mousedown", this.preventDefault);
		}
		
	}

	/**
	 * Предотвращает всплытие события и больше ничего не делает
	 * @param event
	 */
	private preventDefault(event: any) {
		event.preventDefault();
	}

	/**
     * Удаляет события для документа
     */
	private removeDocumentEvents(): void {
		document.documentElement.removeEventListener("mouseup", this.documentOnMouseUp);
		document.documentElement.removeEventListener("mousemove", this.documentOnMouseMove);
		window.removeEventListener("resize", this.windowOnResize);

		let elements = [
			this._sliderElement as HTMLElement,
			this._leftCircleElement as HTMLElement,
			this._rightCircleElement as HTMLElement,
			this._lineElement as HTMLElement
		];

		for (let element of elements) {
			element.removeEventListener("dragover", this.preventDefault);
			element.removeEventListener("drop", this.preventDefault);
			element.removeEventListener("mousedown", this.preventDefault);
		}
	}

	/**
	 * Обработчик события resize для окна window
	 * @param event
	 */
	private windowOnResize(event: any) {
		this.setBounds();
	}

    /**
     * Обработчик события mousedown на ползунке
     * @param event
     */
	private sliderOnMouseDown(event: React.MouseEvent<HTMLSpanElement>): void{
		this.grabSlider();
	}

	/**
     * Обработчик события mouseup на всем документе
     * @param event
     */
	private documentOnMouseUp(event: MouseEvent): void {
		this.releaseSlider();
	}

	/**
     * Обработчик события mousemove на документе
     * @param event
     */
	private documentOnMouseMove(event: MouseEvent): void {
		this.dragSlider(event.pageX);
	}

	/**
     * Обработчик события click на главном контейнере компонента
     * @param event
     */
	private containerOnClick(event: React.MouseEvent<HTMLSpanElement>): void {
		let slider = this._sliderElement as HTMLElement;
		if (event.target === slider)
			return;
		this.changeCurrentPosition(event.pageX);
	}

	/**
     * Захватывает ползунок
     */
    private grabSlider(): void {
		this._sliderIsGrabbed = true;
	}

    /**
     * Отпускает ползунок
     */
    private releaseSlider(): void {
		this._sliderIsGrabbed = false;
	}

    /**
     * Тащит ползунок
     * @param pageX координата мыши
     */
	private dragSlider(pageX: number): void {
		if(!this._sliderIsGrabbed)
			return;
		this.changeCurrentPosition(pageX - 21);
	}

    /**
     * Изменяет текущею позицию относительно координаты мыши
     * @param pageX координата мыши
     */
	private changeCurrentPosition(pageX: number): void {
		// узнаем позицию относительно всего документа
		let sliderCurrentPosition = pageX;

		if (sliderCurrentPosition < this._sliderMinLeftPosition)
			sliderCurrentPosition = this._sliderMinLeftPosition;
		if (sliderCurrentPosition > this._sliderMaxLeftPosition)
			sliderCurrentPosition = this._sliderMaxLeftPosition;

		// узнаем позицию относительно родителя ползунка
		sliderCurrentPosition -= this._leftZeroPosition;
		//
		this.setState({
			sliderCurrentPosition: sliderCurrentPosition
		});
		if (this.props.onScaleChanged !== undefined)
			this.props.onScaleChanged(this.mapToScale(
				sliderCurrentPosition,
				this._sliderMinLeftPosition - this._leftZeroPosition,
				this._sliderMaxLeftPosition - this._leftZeroPosition
			));
	}

	/**
	 * Преобразует текущею позицию в значение масштаба (в процентах)
	 * @param currentPosition
	 * @param minLeftPosition
	 * @param maxLeftPosition
	 */
	private mapToScale(currentPosition: number, minLeftPosition: number, maxLeftPosition: number): number {
		let scale = (currentPosition - minLeftPosition) / (maxLeftPosition - minLeftPosition);
		
		scale = Math.round(scale * (this._availableScales.length - 1));
		
		return this._availableScales[scale];
	}

    /**
     * Устанавливает граничные значения для ползунка
     */
	private setBounds(): void {
		let slider = this._sliderElement as HTMLElement;
		let left_circle = this._leftCircleElement as HTMLElement;
		let right_circle = this._rightCircleElement as HTMLElement;

		this._leftZeroPosition = this.leftPosition(left_circle);
		this._sliderMinLeftPosition = this.leftPosition(left_circle) + left_circle.offsetWidth / 2 - slider.offsetWidth / 2;
		this._sliderMaxLeftPosition = this.leftPosition(right_circle) + right_circle.offsetWidth / 2 - slider.offsetWidth / 2;
	}

	/**
	 * Возвращает левую позицию элемента
	 * @param domElt
	 */
	private leftPosition(domElt: HTMLElement): number {
		if (!domElt) {
			return 0;
		}
		return domElt.offsetLeft + this.leftPosition(domElt.offsetParent as HTMLElement);
	}

	/**
	 * @internal
	 */
	public componentWillReceiveProps(nextProps: ZoomerProps) {
		const state = this.state;
		let newState = {} as ZoomerState;
		let isNeedUpdateState = false;

		if (nextProps.doReset) {
			newState.sliderCurrentPosition = undefined;
			//
			isNeedUpdateState = true;
		}

		if (isNeedUpdateState) {
			this.setState(newState);
		}
	}

	/**
	 * @internal
	 */
	public componentDidMount() {
		this.setBounds();
		this.setDocumentEvents();
	}

	/**
	 * @internal
	 */
	public componentWillUnmount() {
		this.removeDocumentEvents();
	}

	/**
	 * @internal
	 */
	public render() {
		const props = this.props;
		const state = this.state;
		let style: React.CSSProperties | undefined;
		if (this.state.sliderCurrentPosition !== undefined) {
			style = {
				left: this.state.sliderCurrentPosition
			};
		}
		return (
			<span className="a2-zoomer" onClick={this.containerOnClick}>
				<span className="a2-zoomer-left_circle" ref={x => this._leftCircleElement = x}></span>
				<span className="a2-zoomer-right_circle" ref={x => this._rightCircleElement = x}></span>
				<span className="a2-zoomer-slider" ref={x => this._sliderElement = x} 
					onMouseDown={this.sliderOnMouseDown} style={style}
				></span>
				<span className="a2-zoomer-line" ref={x => this._lineElement = x}></span>
			</span>
        );
	}
}

/**
 * Модель описывающая свойства компонента Zoomer
 */
export interface ZoomerProps {
	/**
	 * Обратная функция для установки масштаба (знаяение в процентах)
	 */
	onScaleChanged?(num: number): void

	/**
	 * Сделать сброс компонента
	 */
	doReset: boolean
}

/**
 * Модель описывающая состояние компонента Zoomer
 */
export interface ZoomerState {
	/**
	 * Текущая позиция слайдера
	 */
	sliderCurrentPosition?: number	
}



