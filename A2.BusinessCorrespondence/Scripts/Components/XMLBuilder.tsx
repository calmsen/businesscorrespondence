﻿/**
 * Позволяет создавать xml документы. 
 * Собственная реализация отличная от нативной (DOMParser) тем, что позволяет создавать теги, названные на кирилице.
 */
export class XMLBuilder {
	constructor(nodeName: string, parent?: XMLBuilder) {
		this.nodeName = nodeName;
		this.parent = parent;
		this.elements = [];
		this.attributes = [];
	}

	/**
	 * Имя узла 
	 */
	public nodeName: string;

	/**
	 * Родительский узел
	 */
	public parent: XMLBuilder | undefined;

	/**
	 * Дочерние узлы
	 */
	public elements: XMLBuilder[];

	/**
	 * Атрибуты узла
	 */
	public attributes: XmlAttribute[];

	/**
	 * Создает новый элемент
	 */
	public createElement = (nodeName: string) => {
		return new XMLBuilder(nodeName, this);
	}

	/**
	 * Устанавливает атрибу
	 */
	public setAttribute = (name: string, value: string): void => {
		for (let attr of this.attributes) {
			if (attr.name === name) {
				attr.value = value;
				return;
			}
		}
		this.attributes.push({
			name: name,
			value: value
		})
		 
	}

	/**
	 * Добавляет дочерний узел
	 */
	public appendChild = (node: XMLBuilder) => {
		this.elements.push(node);
	}

	/**
	 * Представляет xml документа ввиде текста
	 */
	public toString = () => {
		let result = '';

		if (!this.parent) {
			result += '<?xml version="1.0" encoding="utf-8"?>';
		}

		result += '<' + this.nodeName;

		for (let attr of this.attributes) {
			result += ' ' + attr.name + '="' + attr.value + '"';
		}

		if (this.elements.length == 0) {
			result += ' />';
		} else {
			result += '>';

			for (let i = 0; i < this.elements.length; i++) {
				result += this.elements[i].toString();
			}

			result += '</' + this.nodeName + '>';
		}

		return result;
	}
};

/**
 * Описывает атрибут узла
 */
interface XmlAttribute {
	name: string,
	value: string
}