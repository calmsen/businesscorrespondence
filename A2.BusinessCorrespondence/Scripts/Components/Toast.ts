﻿import "jqueryToast";

/**
 * Jquery
 */
declare global {
	const $: any;
} 

/**
 * Обертка для работы с toast плагином
 */
export class Toast {
	/**
	 * Покажем сообщение
	 * @param title
	 * @param message
	 */
	public static showMessage(title: string, message: string): void {
		$.toast({
			heading: title,
			text: message,
			position: 'top-right',
			showHideTransition: 'slide',
			loader: false
		});
	}
}
