﻿import * as React from "react"
import * as ReactDOM from "react-dom"
import { InfiniteScroll, ScopeType } from "./Components/InfiniteScroll";
import { CertificateInfoPopUp } from "./CertificateInfo";
import { LetterData, LetterState, Direction, SubscriberType } from "./Models";
import { Utils } from "./Components/Utils";
import * as classNames from "classnames";

/**
 * Список писем
 */
export class LettersList extends React.Component<LettersListProps, LettersListState> {

	constructor(props: LettersListProps) {
		super(props);

		this.letterOnClick = this.letterOnClick.bind(this);
		this.letterOnMouseOver = this.letterOnMouseOver.bind(this);
		this.letterOnMouseLeave = this.letterOnMouseLeave.bind(this);
		this.openCertificateInfoPopup = this.openCertificateInfoPopup.bind(this);

		this.state = Object.assign({ hoveredColumn: LetterColumn.Other }, props);
	}
	
	/**
	 * Обработчик события на письме. Обработчик переопределяет оригинальное функцию onClick на письме.
	 * Устанавливает новое активное письмо и вызывает оригинальную обратную функцию onClick.
	 * @param guid 
	 */
	private letterOnClick(guid?: string): void {
		this.setState({ activeLetterGuid: guid });

		this.props.letterOnClick(guid);
	}

	/**
	 * Проверяет выбрано ли письмо
	 * @param letter
	 */
	private isLetterActive(letter: LetterData) {
		return letter.guid === this.state.activeLetterGuid;
	}

	/**
	 * Отлавливает переход на другую ячейку в таблице
	 * @param e
	 */
	private letterOnMouseOver(e: React.MouseEvent<HTMLTableElement>): void {
		// 
		let element = e.target as HTMLElement | null;
		while (element) {
			if (element.tagName.toLowerCase() == "td")
				break
			element = element.parentElement;
		}
		if (!element)
			return;

		let hoveredColumn = LetterColumn.Other;
		if (element.classList.contains(LetterColumn.Sender.toString())) {
			hoveredColumn = LetterColumn.Sender;
		}
		else if (element.classList.contains(LetterColumn.Receiver.toString())) {
			hoveredColumn = LetterColumn.Receiver;
		}

		if (hoveredColumn !== this.state.hoveredColumn) {
			this.setState({
				hoveredColumn: hoveredColumn
			});
		}
	}

	/**
	 * Отлавливает переход на другую таблицу
	 * @param e
	 */
	private letterOnMouseLeave(e: React.MouseEvent<HTMLTableElement>): void {
		this.setState({
			hoveredColumn: LetterColumn.Other
		});
	}

	/**
	 * Заглушка для обработчиков. Устанавливается для неактивных писем.
	 */
	private handlerNoop(e: React.MouseEvent<HTMLTableElement>): void { }

	/**
	 * Открывает окно с инфрмацией по сертификату
	 */
	private openCertificateInfoPopup(certSerial: string): void {
		CertificateInfoPopUp(certSerial);
	}

	/**
	 * Заглушка для обработчика openCertificateInfo. Устанавливается для неактивных писем.
	 */
	private openCertificateInfoNoop(certSerial: string): void { }

	/**
	 * Проверяет нужно ли обновлять письма. Если размер нового списка не совпадает с размером списка из состояния,
	 * то возвращется true. Если в процессе итерации какая-то письмо 
	 * не равно соответсвующему письму по индексу  из состояния (делается сравнение с inputData), 
	 * то возвращается true. Иначе возвращается false.
	 * @param newLetters
	 */
	private isNeedUpdateLetters(newLetters: LetterData[]): boolean {
		const letters = this.props.letters;
		if (newLetters.length !== letters.length)
			return true;
		for (let i = 0; i < newLetters.length; i++) {
			if (newLetters[i] !== letters[i]) {
				return true;
			}
		}
		return false;
	}

	/**
	 * @internal
	 */
	public componentDidMount() {
		// убираем у документа скролл. Иначе могут быть конфликты.
		document.documentElement.style.overflow = "hidden";
	}

	/**
	 * @internal
	 */
	public componentWillReceiveProps(nextProps: LettersListProps) {
		const state = this.state;
		let newState = {} as LettersListState;
		let isNeedUpdateState = false;

		if (nextProps.activeLetterGuid !== state.activeLetterGuid) {
			newState.activeLetterGuid = nextProps.activeLetterGuid;
			//
			isNeedUpdateState = true;
		}
		
		if (isNeedUpdateState) {
			this.setState(newState);
		}
	}

	/**
	 * @internal
	 */
	public render() {
		const letters = this.props.letters;
		return (
            <div className="a2-short-letters-list">
                <table cellPadding={0} cellSpacing={0} className="a2-short-letters-list-header-table">
					<tbody>
						<tr className="a2-short-letters-list-header">
							<th className="a2-list-header-subject">Тема</th>
							<th className="a2-list-header-date">Дата</th>
							<th className="a2-list-header-sender">Отправитель</th>
							<th className="a2-list-header-receiver">Получатель</th>
							<th className="a2-list-header-arrow"></th>
						</tr>
					</tbody>
				</table>
				<InfiniteScroll loadMore={this.props.loadMoreLetters} hasMore={this.props.hasMoreLetters} pageLoaded={this.props.pageLoaded} scopeType={ScopeType.Self}>
					{letters.map((l, i) => <Letter
						key={l.guid}
						guid={l.guid}
						date={l.date}
						state={l.state}
						subject={l.subject}
						receiver={l.receiver}
						sender={l.sender}
						isActive={this.isLetterActive(l)}
						hoveredColumn={this.isLetterActive(l) ? this.state.hoveredColumn : LetterColumn.Other}
						direction={l.direction}
						readNotification={l.readNotification}
						onClick={this.letterOnClick}
						onMouseOver={this.isLetterActive(l) ? this.letterOnMouseOver : this.handlerNoop}
						onMouseLeave={this.isLetterActive(l) ? this.letterOnMouseLeave : this.handlerNoop}
						openCertificateInfoPopup={this.isLetterActive(l) ? this.openCertificateInfoPopup : this.openCertificateInfoNoop}
					/>)}
				</InfiniteScroll>
			</div>
		);
	}
}


/**
 * Компонент письмо
 * @param props
 */
const Letter = (props: LetterProps) => {	
	// получим список состояний для каждого состояния письма. Это список необходим для развернутого письма.
	let activeStates: LetterProgressState[] = [
		{
			isActive: false,
			label: props.direction === Direction.Outcoming ? "Отправлено" : "Получено"
		},
		{
			isActive: false,
			label: props.direction === Direction.Outcoming ? "Доставлено" : "Отправлено извещение о получении"
		},
		{
			isActive: false,
			label: props.direction === Direction.Outcoming ? "Прочтено получателем" : "Отправлено извещение о прочтении"
		}
	];


	
	switch (props.state) {
		case LetterState.IzvPr:
			activeStates[2].isActive = true;
		case LetterState.IzvPol:
			activeStates[1].isActive = true;
		case LetterState.MainDoc:
			activeStates[0].isActive = true;
	}

	// удалим статус об извещении о прочтении если не требуется отправлять данное уведомление 
	if (!props.readNotification) {
		activeStates.splice(2, 1);
	}
	let progress = (100 / activeStates.length) * activeStates.filter(x => x.isActive).length;
	progress = Math.round(progress);

	// получим надпись по текущему состоянию письма. Это необходимо для свернутого письма.
	let stateLabel = "";
	switch (props.state) {
		case LetterState.Draft:
			stateLabel = "Черновик";
			break;
		case LetterState.Trash:
			stateLabel = "Удалено";
			break;
		case LetterState.Sending:
			stateLabel = "Отправляется";
			break;
		case LetterState.IzvPr:
			stateLabel = props.direction === Direction.Outcoming ? "Прочтено получателем" : "Отправлено извещение о прочтении";
			break;
		case LetterState.IzvPol:
			stateLabel = props.direction === Direction.Outcoming ? "Доставлено" : "Отправлено извещение о получении";
			break;
		case LetterState.MainDoc:
			stateLabel = props.direction === Direction.Outcoming ? "Отправлено" : "Получено";
			break;
	}

	/**
	 * Получает класс для наведенной колонки
	 * @param column
	 */
	const getHoveredClass = (column: LetterColumn): string => {
		return props.isActive && column === props.hoveredColumn ? " hovered" : "";
	}

	return (
        <table className={"a2-short-letters-list-item" + (props.isActive ? " un" : " ") + "collapsed"} cellPadding={0} cellSpacing={0}
			onClick={e => { !props.isActive && props.onClick(props.guid); }} onMouseOver={props.onMouseOver} onMouseLeave={props.onMouseLeave} >
			<tbody>
				<tr>
					<td className="a2-letter-cell a2-letter-subject">{props.subject}</td>
					<td className="a2-letter-cell a2-letter-date">{Utils.formatDate(props.date, "dd.MM.yyyy")}</td>
					<td className={"a2-letter-cell a2-letter-org_name " + LetterColumn.Sender.toString() + getHoveredClass(LetterColumn.Sender)} onClick={(e) => props.openCertificateInfoPopup(props.sender.certSerial)}>
                        {props.isActive ? <div className="a2-letter-org_name-label">Организация</div> : null}
						{props.sender.orgName}
					</td>
					<td className={"a2-letter-cell a2-letter-org_name " + LetterColumn.Receiver.toString() + getHoveredClass(LetterColumn.Receiver)} onClick={(e) => props.openCertificateInfoPopup(props.receiver.certSerial)}>
                        {props.isActive ? <div className="a2-letter-org_name-label">Организация</div> : null}
						{props.receiver.orgName}
					</td>
					<td rowSpan={4} className={"a2-letter-cell a2-letter" + (props.isActive ? "-un" : "-") + "collapsed_arrow"} onClick={e => { props.isActive && props.onClick(undefined); }}></td>
				</tr>
				<tr>
					{!props.isActive
						? <td className="a2-letter-cell a2-letter-state">
							<span>{stateLabel}</span>
						</td>
						: <td className="a2-letter-cell"></td>}
					<td></td>
					<td className={"a2-letter-cell a2-letter-names " + LetterColumn.Sender.toString() + getHoveredClass(LetterColumn.Sender)} onClick={(e) => props.openCertificateInfoPopup(props.sender.certSerial)}>
                        {props.isActive ? <div className="a2-letter-names-label">ФИО</div> : null}
						{props.isActive ? props.sender.fullName : props.sender.shortName}
					</td>
					<td className={"a2-letter-cell a2-letter-names " + LetterColumn.Receiver.toString() + getHoveredClass(LetterColumn.Receiver)} onClick={(e) => props.openCertificateInfoPopup(props.receiver.certSerial)}>
                        {props.isActive ? <div className="a2-letter-names-label">ФИО</div> : null}
						{props.isActive ? props.receiver.fullName : props.receiver.shortName}
					</td>
					<td className="a2-letter-cell"></td>
				</tr>
				<tr>
					<td className="a2-letter-cell"></td>
					<td className="a2-letter-cell"></td>
					<td className={"a2-letter-cell a2-letter-post " + LetterColumn.Sender.toString() + getHoveredClass(LetterColumn.Sender)} onClick={(e) => props.openCertificateInfoPopup(props.sender.certSerial)}>
                        {props.isActive ? <div className="a2-letter-post-label">Должность</div> : null}
						{props.sender.post}
					</td>
					<td className={"a2-letter-cell a2-letter-post " + LetterColumn.Receiver.toString() + getHoveredClass(LetterColumn.Receiver)} onClick={(e) => props.openCertificateInfoPopup(props.receiver.certSerial)}>
                        {props.isActive ? <div className="a2-letter-post-label">Должность</div> : null}
						{props.receiver.post}
					</td>
					<td className="a2-letter-cell"></td>
				</tr>
				{props.isActive && props.state !== LetterState.Draft && props.state !== LetterState.Trash &&
					<tr>
					<td className={classNames("a2-letter-cell", "a2-col-" + activeStates.length)} colSpan={5}>
						<div className="a2-letter-state-uncollapsed">
								<div className="a2-states-list">
								{
									activeStates.map((x, i) => <div key={i} className={classNames("a2-state", { active: x.isActive })}>
										<div className="a2-state-label">{x.label}</div>
										<div className="a2-state-circle" />
									</div>)
								}
							</div>
							<div className={classNames("a2-letter-state-progress", "a2-letter-state-progress-" + progress)}>
									<div className="a2-progress-gradient" />
								</div>
							</div>
						</td>
					</tr>}
			</tbody>
		</table>
	);

}

/**
 * Модель описывающая свойства компонента LettersList
 */
export interface LettersListProps {
	/**
	 * Обработчик события на письме
	 */
	letterOnClick(guid?: string): void,

	/**
	 * Загружает письма для указанной страницы 
	 * @param page страница которую нужно подгрузить
	 */
	loadMoreLetters(page: number): Promise<void>,

	/**
	 * Нужно ли загружать следующие письма
	 */
	hasMoreLetters: boolean,

	/**
	 * Последняя загруженная страница(текущая страница)
	 */
	pageLoaded: number,

	/**
	 * Guid активного письма
	 */
	activeLetterGuid?: string,

	/**
	 * Список писем
	 */
	letters: LetterData[]
}

/**
 * Модель описывающая состояние компонента LettersList
 */
interface LettersListState {
	/**
	 * Наведенная колонка
	 */
	hoveredColumn: LetterColumn,

	/**
	 * Guid активного письма
	 */
	activeLetterGuid?: string,
}


/**
 * Модель описывающая свойства компонента Letters
 */
interface LetterProps extends LetterData {
	/**
	 * Обработчик события click
	 */
	onClick(guid?: string): void,

	/**
	 * Обработчик события mouseOver
	 */
	onMouseOver(e: React.MouseEvent<HTMLTableElement>): void,

	/**
	 * Обработчик события mouseLeave
	 */ 
	onMouseLeave(e: React.MouseEvent<HTMLTableElement>): void,

	/**
	 * Показывает окно с информацией по сертификату для указаного абонента
	 */
	openCertificateInfoPopup(certSerial: string | undefined): void,

	/**
	 * Наведенная колонка
	 */
	hoveredColumn: LetterColumn,

	/**
	 * Активно ли письмо
	 */
	isActive: boolean
}

/**
 * Типы колонок(td) в письме
 */
enum LetterColumn {
	/**
	 * Колонка с информацией об отправителе 
	 */
	Sender,

	/**
	 * Колонка с информацией о получатале 
	 */
	Receiver,

	/**
	 * Другие колонки
	 */
	Other
}

/** 
 * Описывает статус в активном письме
 */
interface LetterProgressState {
	isActive: boolean,
	label: string
}
