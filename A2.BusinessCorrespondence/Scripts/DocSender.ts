﻿import * as Console from "./Components/Console"
import { BaseDocSenderData, DocSenderData, ResultException, ItemType } from "./DocSenderMappers";
import * as Cookies from 'js-cookie';
import { Utils } from "./Components/Utils";
import { pubSub } from "./Components/PubSub";

/**
 * Обертка над плагином 
 */
export class DocSender {
	constructor() {
		this._docSender = document.getElementById("docSender");
		try {
			this._docSender.SetScriptCallbackObject(this.docSenderOnAsyncResult);
			this._docSender.SetExchangeFormat(ExchangeFormats.Json);
			this.IsReady = true;
		}
		catch {
			Console.error("Плагин не поддерживается.");
		}
		
	}

	/**
	 * Экземпляр DocSender
	 */
	private static _instance: DocSender;

	/**
	 * Реализует синглтон для DocSender
	 */
	public static getInstance() {
		return this._instance || (this._instance = new DocSender());
	} 

	/**
	 * Непосредственно сам плагин
	 */
	private _docSender: any;

	/**
	 * Готов ли плагин к использованию
	 */
	public IsReady = false;

	/**
	 * Токен для доступа в плагин
	 */
	private _accessToken: string | undefined;

	/**
	 * Обработка асинхронного ответа
	 * @param args
	 */
	private docSenderOnAsyncResult(args: string) {
		var jsonDoc = JSON.parse(args);
		var responseId = jsonDoc.id;

		Console.log("Ответ: type -> ", jsonDoc.item && jsonDoc.item.$type, " json -> ", jsonDoc);

		let isResultMessagesHandler = jsonDoc.item && jsonDoc.item.$type === ItemType.ResultMessagesHandler;
		
		pubSub.publish(responseId, jsonDoc, !isResultMessagesHandler);
	}

	/**
	 * Получает токен для плагина
	 */
	public getAccessToken(): string | undefined {
		if (!this._accessToken) {
			let accessToken = Cookies.get('accessToken');
			if (accessToken)
				this._accessToken = accessToken;
		}
		return this._accessToken;
		
	}

	/**
	 * Устанавливает токен для плагина
	 * @param accessToken
	 */
	public setAccessToken(accessToken: string): void {
		this._accessToken = accessToken;
		Cookies.set('accessToken', accessToken);
	} 

	/**
	 * Проверяет есть ли обновления.
	 */
	public isCoreUpdateAvalable(): boolean {
		if (!this.IsReady)
			return false;
		return Number(this._docSender.IsCoreUpdateAvalable()) === -1;
	}

	/**
	 * Проверяет есть ли обновления.
	 */
	public async isCoreUpdateAvalableAsync(): Promise<boolean> {
		return new Promise<boolean>((resolve, reject) => {
			if (!this.IsReady) {
				resolve(false);
			}
			else {
				Console.log("Проверяем обновления плагина...");
				let guid = this._docSender.IsCoreUpdateAvalableAsync();
				Console.log("Проверяем обновления плагина  -> guid: ", guid);
				pubSub.subscribe(guid, (data: any) => {
					resolve(data.response);
				});
			}

		});
	}

	/**
	 * Обновляет плагин
	 */
	public updateCore(): void {
		if (!this.IsReady)
			return;
		this._docSender.UpdateCore();
	}

	/**
	 * Обновляет плагин
	 */
	public async updateCoreAsync(): Promise<boolean> {
		return new Promise<boolean>((resolve, reject) => {
			if (!this.IsReady) {
				resolve(false);
			}
			else {
				Console.log("Обновляем плагин...");
				let guid = this._docSender.UpdateCoreAsync();
				Console.log("Обновляем плагин  -> guid: ", guid);
				pubSub.subscribe(guid, (data: any) => {
					resolve(data.response);
				});
			}			
		});
		
	}

	/**
	 * Выполняет запрос
	 */
	public execute<TResponse extends BaseDocSenderData>(request: BaseDocSenderData): TResponse {
		if (!this.IsReady)
			return new DocSenderData(ResultException) as TResponse;
		
		request.accessToken = this.getAccessToken();

		let data = this._docSender.execute(JSON.stringify(request));
		return JSON.parse(data);
	}
	
	/**
	 * Выполняет запрос
	 */
	public async executeAsync<TResponse extends BaseDocSenderData>(request: BaseDocSenderData, handler?: (data: TResponse) => void): Promise<TResponse> {
		return new Promise<TResponse>((resolve, reject) => {
			if (!this.IsReady)
				return resolve(new DocSenderData(ResultException) as TResponse);

			let guid: string;
			if (!request.id) {
				guid = Utils.guid();
				request.id = guid;
			}
			else {
				guid = request.id;
			}

			if (handler === undefined)
				handler = (data: TResponse) => resolve(data);

			pubSub.subscribe(guid, handler);		
			request.accessToken = this.getAccessToken();
			Console.log("Запрос: ", request);
			this._docSender.executeAsync(JSON.stringify(request));
		});
	}

}

/**
 * Формат для запросов и ответов в плагине
 */
enum ExchangeFormats {
	Xml,
	Json
}