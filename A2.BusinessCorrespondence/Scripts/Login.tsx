﻿import * as React from "react"
import * as ReactDOM from "react-dom"
import { CertificateInfoData, SubscriberType, UserData, ModifiedLetter } from "./Models";
import { popUp } from "./Components/Modal";
import { userService } from "./Services/UserService";
import { Toast } from "./Components/Toast";
import { DocSender } from "./DocSender";
import { Utils } from "./Components/Utils";
import { lettersService } from "./Services/LetterService";
import { ResultMessagesHandler } from "./DocSenderMappers";

/**
 * Показывает окно для авторизации
 */
export async function LoginPopUp(): Promise<UserData> {
	return new Promise<UserData>((resolve, reject) => {
		let loginProps: LoginProps = {
			userReceiving: (user: UserData) => {
				loginPopup.close();
				resolve(user);
			},
			center: () => loginPopup.center()
		};
		// ---
		let loginPopup = popUp(<Login {...loginProps} />, undefined, true);
	});
	
}

/**
 * Окно для авторизации
 */
class Login extends React.Component<LoginProps, LoginState> {
	constructor(props: LoginProps) {
		super(props);

		this.certOnClick = this.certOnClick.bind(this);
		
		this.state = {
			displayState: DisplayState.Loading
		};
	}

	/**
	 * Получает текст для отображения состояния авторизации
	 */
	private getDisplayState(): string | undefined {
		switch (this.state.displayState) {
			case DisplayState.Loading:
				return "Загрузка...";
			case DisplayState.PluginIsNotSupported:
				return "Плагин не поддерживается";
			case DisplayState.UpdatingPlugin:
				return "Обновление плагина...";
			case DisplayState.ReceivingCertificates:
				return "Получение информации о сертификатах...";
			case DisplayState.ReceivingCertificatesFailed:
				return "Не удалось получить информацию о сертификатах.";
			case DisplayState.ChoosingCertificate:
				return "Выберите сертификат для авторизации:";
			case DisplayState.Authentication:
				return "Аутентификация в плагине...";
			case DisplayState.AuthenticationFailed:
				return "Не получилось авторизоваться.";
			case DisplayState.ReceivingUserData:
				return "Получение информации о пользователе...";
			case DisplayState.ReceivingUserDataFailed:
				return "Не удалось получить информацию о пользователе.";
			case DisplayState.CertificatesListIsEmpty:
				return "Нет сертификатов с активной услугой ДСП.";
			default:
				return undefined;
		}

	}

	/**
	 * Получает css класс для отображения состояния авторизации
	 */
	private getDiplayStateCssClass(): CssClass {
		if (this.state.displayState === DisplayState.PluginIsNotSupported
			|| this.state.displayState === DisplayState.AuthenticationFailed
			|| this.state.displayState === DisplayState.ReceivingUserDataFailed
			|| this.state.displayState === DisplayState.ReceivingCertificatesFailed)
			return CssClass.Error;
		return CssClass.Success;
	}

	/**
	 * Получаем информацию о пользователе
	 */
	private async getUserInfo(): Promise<void> {
		this.setState({
			displayState: DisplayState.ReceivingUserData
		});
		try {
			let user = await userService.getUserInfo();
			this.props.userReceiving(user);
		}
		catch {
			this.setState({
				displayState: DisplayState.ReceivingUserDataFailed
			});
		}
	}

	/**
	 * Войти в систему
	 * @param certSerial
	 */
	private async loginUser(certSerial: string): Promise<boolean> {
		this.setState({
			displayState: DisplayState.Authentication
		});

		try {
			let accessToken = await userService.loginUser(certSerial);
			DocSender.getInstance().setAccessToken(accessToken);
			return true;
		}
		catch {
			this.setState({
				displayState: DisplayState.AuthenticationFailed
			});
			return false;
		}
	}

	/**
	 * Делает запрос на авторизацию с указанным сертификатом
	 * @param certSerial
	 */
	private async certOnClick(certSerial: string): Promise<void> {
		if (await this.loginUser(certSerial)) {
			await this.getUserInfo();
		}
	}

	/**
	 * @internal
	 */
	public async componentDidMount(): Promise<void> {	
		this.props.center();
		// проверяем поддерживается ли плагин 
		if (!DocSender.getInstance().IsReady) {
			this.setState({
				displayState: DisplayState.PluginIsNotSupported
			});
			return;
		}
		// обновляем плагин, если доступно обновление
		if (await DocSender.getInstance().isCoreUpdateAvalableAsync()) {
			this.setState({
				displayState: DisplayState.UpdatingPlugin
			});
			let isUpdated = await DocSender.getInstance().updateCoreAsync();
			if (!isUpdated)
				Toast.showMessage("Ошибка", "Не удалось обновить плагин");
		}
		// получим информацию о пользователе если в куках есть токен
		if (DocSender.getInstance().getAccessToken()) {
			await this.getUserInfo();
			return;
		}
		// авторизуем пользователя
		this.setState({
			displayState: DisplayState.ReceivingCertificates
		});
		let certificates: CertificateInfoData[] = [];
		try {
			certificates = await userService.getCertificatesWithDspService();
		}
		catch {
			this.setState({
				displayState: DisplayState.ReceivingCertificatesFailed
			});
			return;
		}
		if (certificates.length == 0) {
			this.setState({
				displayState: DisplayState.CertificatesListIsEmpty
			});
			return;
		}

		if (certificates.length == 1) {
			if (await this.loginUser(certificates[0].serialNumber)) {
				await this.getUserInfo();
			}
			return;
		}

		this.setState({
			certificates: certificates,
			displayState: DisplayState.ChoosingCertificate
		});
	}

	/**
	 * @internal
	 */
	public componentDidUpdate() {
		this.props.center();
	}

	/**
	 * @internal
	 */
	public render() {
		const displayState = this.getDisplayState();

		return (
			<div className="a2-login-popup">
				<div className="a2-login-popup-header">
					Авторизация
				</div>
				<div className="a2-login-popup-body">
					{displayState && <div className={"display_state " + this.getDiplayStateCssClass()}>{displayState}</div>}
					{this.state.displayState == DisplayState.ChoosingCertificate && this.state.certificates && 
						<div className="a2-certificates">
						{this.state.certificates
							.map((x, i) => <div key={x.serialNumber} className="a2-certificate" onClick={e => this.certOnClick(x.serialNumber)}>
								<div className="a2-certificate-user_info"><span>ФИО:</span> {x.surname} {x.name} {x.patronymic}</div>
								<div className="a2-certificate-org_info"><span>Организация:</span> {Utils.normalizeOrgName(x.organization, `${x.surname} ${x.name} ${x.patronymic}`)}</div>
							</div>
							)}
						</div>
					}
				</div>
			</div>
        );
	}
}

/**
 * Модель описывающая свойства компонента Login
 */
interface LoginProps {

	/**
	 * Обработчик события перед отправкой письма
	 */
	userReceiving(user: UserData): void,
	
	/**
	 * Выравнивает окно по центру
	 */
	center(): void
}

/**
 * Модель описывающая состояние компонента Login
 */
interface LoginState {
	/**
	 * Информация о сертификатах
	 */
	certificates?: CertificateInfoData[],

	/**
	 * Текущее состояние приложения
	 */
	displayState: DisplayState,
}


/**
 * Состояние приложения от инициализации до готовности
 */
export enum DisplayState {

	/**
	 * Отображается loading
	 */
	Loading,

	/**
	 * Плагин не поддерживается
	 */
	PluginIsNotSupported,

	/**
	 * Обновляется плагин
	 */
	UpdatingPlugin,
	
	/**
	 * Получение информации о сертификатах
	 */
	ReceivingCertificates,

	/**
	 * Возникла ошибка при получении информации о сертификатах
	 */
	ReceivingCertificatesFailed,

	/**
	 * Нет сертификатов с активной услугой ДСП
	 */
	CertificatesListIsEmpty,

	/**
	 * Выбор сертификата для авторизации 
	 */
	ChoosingCertificate,

	/**
	 * Аутентификация в плагине
	 */
	Authentication,

	/**
	 * Не удалось авторизоваться
	 */
	AuthenticationFailed,

	/**
	 * Получение информации о пользователе
	 */
	ReceivingUserData,

	/**
	 * Возникла ошибка при получении информации о пользователе
	 */
	ReceivingUserDataFailed,

	/**
	 * Приложение готово к использованию
	 */
	Ready
}

enum CssClass {
	Error = "error",
	Success = "success"
}


