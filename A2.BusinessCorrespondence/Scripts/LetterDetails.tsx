﻿import * as React from "react"
import * as ReactDOM from "react-dom"
import * as Console from "./Components/Console"
import { popUp, PopUp } from "./Components/Modal";
import { LetterData, LetterState, LetterAttachmentData, Direction } from "./Models";
import { LetterAttachmentsProps, LetterAttachments } from "./LetterAttachments";
import { ImageViewer, ImageData } from "./Components/ImageViewer";
import { filesService } from "./Services/FilesService";
import { Utils } from "./Components/Utils";
import { Alert } from "./Components/Alert";
import { DownloadFileSucceedAlert } from "./DownloadFileSucceedAlert";
import { Toast } from "./Components/Toast";
import { lettersService } from "./Services/LetterService";

/**
 * Содержимое письма
 */
export class LetterDetails extends React.Component<LetterDetailsProps, LetterDetailsState> {

	constructor(props: LetterDetailsProps) {
		super(props);

		this.attachmentOnClick = this.attachmentOnClick.bind(this);
		this.downloadFile = this.downloadFile.bind(this);
		this.sendReadNotification = this.sendReadNotification.bind(this);

		this.state = {
			activeAttachmentGuid: undefined,
			letterDetailsOffsetLeft: 0,
			images: [],
			isSendReadNotificationInProgress: false
		};
	}	

	_letterDetails: HTMLElement | null;

	/**
	 * Устанавливает активный документ для просмотра 
	 * @param guid
	 */
	private async attachmentOnClick(guid: string): Promise<void> {
		if (this.state.activeAttachmentGuid === guid)
			return;
		

		this.setState({
			activeAttachmentGuid: guid
		});

		await this.loadImages(guid);
	}

	/**
	 * Скачивает файл
	 * @param attachmentGuid
	 */
	private async downloadFile(attachmentGuid: string): Promise<void> {
		if (this.props.attachments === undefined)
			return;

		let attachment = this.props.attachments.find(x => x.guid == attachmentGuid);
		if (attachment === undefined)
			return;

		try {
			let filePathInDownloadsDir = await filesService.downloadFile(this.props.guid, attachmentGuid, attachment.filePath, attachment.isLocal || false);
			await DownloadFileSucceedAlert(filePathInDownloadsDir);
		}
		catch {
			Toast.showMessage("Ошибка", "Не удалось скачать файл в папку Downloads");
		}
	}

	/**
	 * Загружает картинки для активного документа.
	 */
	private async loadImages(attachmentGuid: string): Promise<void> {
		let attachment = this.props.attachments && this.props.attachments.find(x => x.guid == attachmentGuid);
		if (attachment === undefined)
			return;
		let images = await filesService.getDocAsImages(this.props.guid, attachmentGuid, attachment.filePath, attachment.isLocal || false);
		this.setState({
			images: images
		});
	}

	/**
	 * Определяет левую позицию элемента letterDetails
	 */
	private setLetterDetailsOffsetLeft(): void {
		let letterDetails = this._letterDetails as HTMLElement;
		this.setState({
			letterDetailsOffsetLeft: this.leftPosition(letterDetails)
		});
	}

	/**
	 * Возвращает левую позицию элемента
	 * @param domElt
	 */
	private leftPosition(domElt: HTMLElement): number {
		if (!domElt) {
			return 0;
		}
		return domElt.offsetLeft + this.leftPosition(domElt.offsetParent as HTMLElement);
	}

	/**
	 * Отправляет уведомление о прочтении
	 */
	private async sendReadNotification(): Promise<void> {
		if (this.props.readNotification === undefined)
			return;

		if (this.props.chainId === undefined) 
			throw new Error("Не известен идентификатор цепочки документов.");

		if (this.props.state !== LetterState.IzvPol)
			return;

		this.setState({
			isSendReadNotificationInProgress: true
		});

		let letterGuid = this.props.guid;
		try {
			await lettersService.sendReadNotification(this.props.chainId);
			this.props.readNotificationSended(letterGuid);
		}
		catch {
			Toast.showMessage("Ошибка", "Не удалось отправить уведомление о прочтении.");

			if (letterGuid == this.props.guid)
				this.setState({
					isSendReadNotificationInProgress: false
				});
		}
	}

	/**
	 * @internal
	 */
	public componentWillReceiveProps(nextProps: LetterDetailsProps) {
		const state = this.state;
		let newState = {} as LetterDetailsState;
		let isNeedUpdateState = false;

		if (nextProps.attachments !== this.props.attachments) {
			newState.activeAttachmentGuid = undefined;
			isNeedUpdateState = true;
		}

		if (nextProps.guid !== this.props.guid) {
			newState.isSendReadNotificationInProgress = false;
			isNeedUpdateState = true;
		}

		if (isNeedUpdateState) {
			this.setState(newState);
		}
	}

	/**
	 * @internal
	 */
	public async componentDidMount(): Promise<void> {
		// убираем у документа скролл. Иначе могут быть конфликты.
		document.documentElement.style.overflow = "hidden";
		
		this.setLetterDetailsOffsetLeft();
	}

	/**
	 * @internal
	 */
	public render() {
		let props = this.props;
		let state = this.state;

		let letterAttachmentsProps: LetterAttachmentsProps = {
			activeAttachmentGuid: this.state.activeAttachmentGuid,
			attachmentOnClick: this.attachmentOnClick,
			attachments: props.attachments || [],
			downloadFile: this.downloadFile
		};

		let isSendReadNotificationBtnVisible = !!this.props.readNotification && this.props.direction === Direction.Incoming
			&& props.state === LetterState.IzvPol && !this.state.isSendReadNotificationInProgress;		

		Console.log("activeAttachmentName: ", state.activeAttachmentGuid)
		return (
			<div className="a2-letter-details" style={{ width: `calc(100vw - ${state.letterDetailsOffsetLeft}px)` }} ref={x => this._letterDetails = x}>
				<div className="a2-letter-header">
					<span className="a2-letter-subject">{props.subject}</span>&nbsp;
					<span className="a2-letter-date">от {Utils.formatDate(props.date, "dd.MM.yyyy")}</span>
					{isSendReadNotificationBtnVisible && <span onClick={e => this.sendReadNotification()} className="a2-letter-send_read_notification_btn" />}					
				</div>
				{props.attachments !== undefined && props.attachments.length > 0 &&
					<LetterAttachments {...letterAttachmentsProps} />}

				{props.body && <div className="a2-letter-body-box"><hr /><div className="a2-letter-body" dangerouslySetInnerHTML={{ __html: props.body }} ></div></div>}
				{state.activeAttachmentGuid && <ImageViewer images={state.images} />}
			</div>
		);
	}
}


/**
 * Модель описывающая свойства компонента LetterDetails
 */
export interface LetterDetailsProps extends LetterData {
	/**
	 * Обработчик события - отправлено уведомление о прочтении
	 */
	readNotificationSended(letterGuid: string): void,
}

/**
 * Модель описывающая состояние компонента LetterDetails
 */
interface LetterDetailsState {
	/**
	 * Guid активного документа
	 */
	activeAttachmentGuid?: string,

	/**
	 * Смещение элемента слева letterDetails относительно document
	 */
	letterDetailsOffsetLeft: number,

	/**
	 * Список данных об изображениях 
	 */
	images: ImageData[],

	/**
	 *  Отправка уведомления о прочтении в прогрессе
	 */
	isSendReadNotificationInProgress: boolean
}


