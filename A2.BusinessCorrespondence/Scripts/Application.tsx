﻿import * as React from "react"
import * as ReactDOM from "react-dom"
import * as Console from "./Components/Console"
import { TopPanel } from "./Components/TopPanel";
import { ToolbarProps, Toolbar } from "./Components/Toolbar";
import { VerticalMenuButtonData, VerticalMenu, VerticalMenuProps } from "./Components/VerticalMenu";
import { LetterData, LetterState, AttachmentType, Direction, FolderName, UserData, ModifiedLetter } from "./Models";
import { LetterDetailsProps, LetterDetails } from "./LetterDetails";
import { Confirm } from "./Components/Confirm";
import { AddressBookPopUp } from "./AddressBook";
import { lettersService } from "./Services/LetterService";
import { LetterFormPopUp } from "./LetterForm";
import { LettersList, LettersListProps } from "./LettersList";
import { popUp } from "./Components/Modal";
import { DocSender } from "./DocSender";
import { userService } from "./Services/UserService";
import { filesService } from "./Services/FilesService";
import { Toast } from "./Components/Toast";
import { DisplayState, LoginPopUp } from "./Login";
import * as Cookies from 'js-cookie';
import { ResultMessagesHandler } from "./DocSenderMappers";


/**
 * Компонент приложение
 */
class Application extends React.Component<ApplicationProps, ApplicationState> {
    constructor(props: {}) {
		super(props);

		this.state = {
			displayState: DisplayState.Loading,
			activeFolderName: undefined,
			folders: this.initFolders()
		};
		this.loadMoreLetters = this.loadMoreLetters.bind(this);
		this.letterOnClick = this.letterOnClick.bind(this);
		this.folderOnClick = this.folderOnClick.bind(this);
		this.toolbarOnClick = this.toolbarOnClick.bind(this);
		this.letterSended = this.letterSended.bind(this);
		this.letterDrafted = this.letterDrafted.bind(this);
		this.logoutUser = this.logoutUser.bind(this);
		this.readNotificationSended = this.readNotificationSended.bind(this);
		this.modifyLetters = this.modifyLetters.bind(this);
	}
	
	/**
	 * Количество писем на одной странице
	 */
	public LETTERS_LIST_PAGE_SIZE = 30;


	/**
	 * Инициализирует папки
	 */
	private initFolders(): FolderState[] {
		return [
			{
				folderName: "incoming",
				activeLetterGuid: undefined,
				letters: [],
				hasMoreLetters: true,
				pageLoaded: 0
			},
			{
				folderName: "outcoming",
				activeLetterGuid: undefined,
				letters: [],
				hasMoreLetters: true,
				pageLoaded: 0
			},
			{
				folderName: "sending",
				activeLetterGuid: undefined,
				letters: [],
				hasMoreLetters: true,
				pageLoaded: 0
			},
			{
				folderName: "draft",
				activeLetterGuid: undefined,
				letters: [],
				hasMoreLetters: true,
				pageLoaded: 0
			},
			{
				folderName: "trash",
				activeLetterGuid: undefined,
				letters: [],
				hasMoreLetters: true,
				pageLoaded: 0
			}
		];
	} 
	
	/**
	 * Загружает письма для указанной страницы 
	 * @param page страница которую нужно подгрузить
	 */
	private async loadMoreLetters(page: number): Promise<void> {
		let folderData = this.getActiveFolder();
		try {
			let letters = await this.getLetters(page);
			// исключим письма которые уже есть 
			for (let i = 0; i < letters.length; i++) {
				for (let j = 0; j < folderData.letters.length; j++) {
					if (letters[i].guid == folderData.letters[j].guid) {
						letters.splice(i, 1);
						i--;
						break;
					}
				}
			}
			folderData.letters = folderData.letters.concat(letters);
			Console.log(page, letters.length, this.LETTERS_LIST_PAGE_SIZE, letters.length % this.LETTERS_LIST_PAGE_SIZE)
			folderData.hasMoreLetters = letters.length > 0 && letters.length % this.LETTERS_LIST_PAGE_SIZE === 0
			folderData.pageLoaded = page;
		}
		catch {
			Toast.showMessage("Ошибка", "Произошла ошибка при получении списка писем.");
		}

		

		this.updateFolderState(folderData);
	}
	
	/**
	 * Получает письма для указанной страницы
	 * @param page
	 */
	private async getLetters(page: number): Promise<LetterData[]> {
		if (this.state.activeFolderName === undefined)
			return [];

		return await lettersService.getLetters(page, this.LETTERS_LIST_PAGE_SIZE, this.state.activeFolderName);
	}

	/**
	 * Обрабатывает событие click-а по письму в списке писем. Делает письмо активным.
	 * @param guid
	 */
	private async letterOnClick(guid: string): Promise<void> {
		Console.log("Выбранно письмо " + guid);
		let folderData = this.getActiveFolder();
		folderData.activeLetterGuid = guid;
		this.updateFolderState(folderData);

		
		let activeLetter = folderData.letters.find(x => x.guid === guid);
		// получим тело документа
		if (activeLetter && activeLetter.body === undefined && activeLetter.mainDocId !== undefined) {
			let cacheItem = filesService.docAsTextCache.find(x => x.docId == guid)
			let letterBodyContent = cacheItem && cacheItem.content;
			if (!letterBodyContent) {
				try {
					letterBodyContent = await filesService.getDocAsText(guid, activeLetter.mainDocId);
				}
				catch {
					Toast.showMessage("Ошибка", "Не удалось получить описание документа.");
				}
			}
			if (letterBodyContent !== undefined) {
				activeLetter.body = letterBodyContent;
				for (let i = 0; i < folderData.letters.length; i++) {
					if (folderData.letters[i].guid == guid) {
						folderData.letters[i] = Object.assign({}, activeLetter);
						break;
					}
				}
				this.updateFolderState(folderData);
			}
		}

		// получим прикрепленные файлы
		if (activeLetter && activeLetter.attachments === undefined) {
			let cacheItem = filesService.attachmentsCache.find(x => x.docId == guid)
			let letterAttachments = cacheItem && cacheItem.attachments;
			if (!letterAttachments) {
				try {
					letterAttachments = await filesService.getAttachments(guid);
				}
				catch {
					Toast.showMessage("Ошибка", "Не удалось получить прикрепленные документы.");
				}
			}
			if (letterAttachments !== undefined) {
				activeLetter.attachments = letterAttachments
				for (let i = 0; i < folderData.letters.length; i++) {
					if (folderData.letters[i].guid == guid) {
						folderData.letters[i] = Object.assign({}, activeLetter);
						break;
					}
				}
				this.updateFolderState(folderData);
			}
		}
	}

	/**
	 * Обрабатывает событие click-а по папке. Делает папку активной.
	 * @param name
	 */
	private folderOnClick(name: FolderName) {
		Console.log(`Нажата кнопка ${name}`)
		let oldFolderData = this.getActiveFolder();
		oldFolderData.activeLetterGuid = undefined;
		this.updateFolderState(oldFolderData);

		this.setState({
			activeFolderName: name
		}, () => {
			let folderData = this.getActiveFolder();
			if (folderData.pageLoaded === 0) {
				this.loadMoreLetters(1);
			}
		});
		
	}

	/**
	 * Обрабатывает событие click-а по кнопке в тулбаре. 
	 * @param name
	 */
	private async toolbarOnClick(name: ToolbarButtonName): Promise<void> {
		try {
			let folder: FolderState;
			let editingLetter: LetterData | undefined;
			switch (name) {
				case ToolbarButtonName.CreateLetter:
					await this.openLetterForm();
					return;
				case ToolbarButtonName.EditLetter:
					folder = this.getActiveFolder();
					if (folder.folderName !== "draft")
						return;
					editingLetter = folder.letters.find(x => x.guid === folder.activeLetterGuid);
					await this.openLetterForm(editingLetter);
					return;
				case ToolbarButtonName.DeleteLetter:
					await this.deleteActiveLetter();
					return;
				case ToolbarButtonName.ForwardLetter:
					folder = this.getActiveFolder();
					editingLetter = folder.letters.find(x => x.guid === folder.activeLetterGuid);
					if (editingLetter == undefined)
						return;

					editingLetter = Object.assign({
						subject: 'FW: ' + editingLetter.subject.replace('FW: ', '').replace('RE: ', ''),
						body: this.getReplyHeader(editingLetter) + editingLetter.body,
						attachments: editingLetter.attachments
					});

					await this.openLetterForm(editingLetter);
					return;
				case ToolbarButtonName.AnswerLetter:
					folder = this.getActiveFolder();
					editingLetter = folder.letters.find(x => x.guid === folder.activeLetterGuid);
					if (editingLetter == undefined)
						return;
					
					editingLetter = Object.assign({
						subject: 'RE: ' + editingLetter.subject.replace('RE: ', '').replace('RE: ', ''),
						body: this.getReplyHeader(editingLetter) + editingLetter.body,
						receiver: editingLetter.sender,
						attachments: editingLetter.attachments
					});
					
					await this.openLetterForm(editingLetter);
					return;
				case ToolbarButtonName.OpenAddressBook:
					await this.openAddressBook();
					return;
			}
		}
		catch {
		}

		this.setState({
			activeToolbarButtonName: undefined
		});
	}

	/**
	 * Возвращает заголовочный текст к телу документа
	 * @param letter
	 */
	private getReplyHeader(letter: LetterData): string {
		return `_______________________________________________<br/>` + 
			`От: ${letter.sender.fullName}<br/>` +
			`Дата: ${letter.date.toString()}<br/>` +
			`Кому: ${letter.receiver.fullName}<br/>` +
			`Тема: ${letter.subject}<br/><br/>`;
	}

	/**
	 * Открывает адресную книгу
	 */
	private async openAddressBook(): Promise<void> {
		if (this.state.user === undefined)
			return;
		
		await AddressBookPopUp(this.state.user.userId);
	}

	/**
	 * Открывает форму для создания письма
	 */
	private async openLetterForm(editingLetter: LetterData | undefined = undefined): Promise<void> {
		if (this.state.user === undefined)
			return;
		

		let letter = await LetterFormPopUp(this.state.user, this.letterSended, this.letterDrafted, editingLetter);
		// удалим письмо из папки "Черновики" если есть
		let draftFolder = this.state.folders.filter(x => x.folderName === "draft")[0];
		for (let i = 0; i < draftFolder.letters.length; i++) {
			if (draftFolder.letters[i].guid == letter.guid) {
				draftFolder.letters.splice(i, 1);
				break;
			}
		}

		if (this.state.activeFolderName === "draft") {
			this.updateFolderState(draftFolder);
		}
		// добавим письмо в папку "Исходящие"
		let sendingFolder = this.state.folders.filter(x => x.folderName === "sending")[0];
		sendingFolder.letters.unshift(letter);

		if (this.state.activeFolderName === "sending") {
			this.updateFolderState(sendingFolder);
		}
	}
	
	/**
	 * Изменяет письма после обработка входящих сообщений с плагина
	 * @param letters
	 */
	private async modifyLetters(letters: ModifiedLetter[]): Promise<void> {		
		for (let l of letters) {
			let letter: LetterData | undefined;
			Console.log("letter:", l);
			if (l.direction === Direction.Outcoming && l.state === LetterState.MainDoc) {
				// удалим из папки "Исходящие"
				let sendingFolder = this.state.folders.filter(x => x.folderName === "sending")[0];
				for (let i = 0; i < sendingFolder.letters.length; i++) {
					if (sendingFolder.letters[i].guid == l.serverGuid) {
						letter = sendingFolder.letters[i];
						sendingFolder.letters.splice(i, 1);
						break;
					}
				}
				if (letter === undefined)
					continue;

				if (this.state.activeFolderName === "sending") {
					this.updateFolderState(sendingFolder);
				}
				// добавим в папку "Отправленные"
				let outcomingFolder = this.state.folders.filter(x => x.folderName === "outcoming")[0];
				letter.state = LetterState.MainDoc;
				outcomingFolder.letters.unshift(letter);
				if (this.state.activeFolderName === "outcoming") {
					this.updateFolderState(outcomingFolder);
				}
			}
			if (l.direction === Direction.Outcoming && (l.state === LetterState.IzvPol || l.state === LetterState.IzvPr)) {
				let outcomingFolder = this.state.folders.filter(x => x.folderName === "outcoming")[0];
				for (let i = 0; i < outcomingFolder.letters.length; i++) {
					if (outcomingFolder.letters[i].guid == l.serverGuid) {
						outcomingFolder.letters[i] = Object.assign({}, outcomingFolder.letters[i], { state: l.state });
						break;
					}
				}

				if (this.state.activeFolderName === "outcoming") {
					this.updateFolderState(outcomingFolder);
				}
			}
			if (l.direction === Direction.Incoming && l.state === LetterState.MainDoc) {
				// добавим в папку "Входящие"
				let incomingFolder = this.state.folders.filter(x => x.folderName === "incoming")[0];
				let letter = await lettersService.getLetter(l.serverGuid);
				incomingFolder.letters.unshift(letter);

				if (this.state.activeFolderName === "incoming") {
					this.updateFolderState(incomingFolder);
				}
			}			
		}
		
	}

	/**
	 * Обработчик события после отправки письма
	 * @param letterGuid
	 * @param serverLetterGuid
	 */
	private letterSended(letterGuid: string, serverLetterGuid: string): void {
		// найдем письмо в папке "Исходящие" и установим serverGuid. В другой папке письмо не должно находится.
		let sendingFolder = this.state.folders.filter(x => x.folderName === "sending")[0];
		for (let i = 0; i < sendingFolder.letters.length; i++) {
			if (sendingFolder.letters[i].guid == letterGuid) {
				sendingFolder.letters[i] = Object.assign({}, sendingFolder.letters[i], { guid: serverLetterGuid })
				break;
			}
		}

		if (this.state.activeFolderName === "sending") {
			this.updateFolderState(sendingFolder);
		}
	}

	/**
	 * Обработчик события - сохранено в черновик
	 * @param letter
	 */
	private letterDrafted(letter: LetterData): void {
		let draftFolder = this.state.folders.filter(x => x.folderName === "draft")[0];
		// удалим старый черновик
		for (let i = 0; i < draftFolder.letters.length; i++) {
			if (draftFolder.letters[i].guid == letter.guid) {
				draftFolder.letters.splice(i, 1);
				break;
			}
		}
		// добавим новый черновик
		draftFolder.letters.unshift(letter);
		
		if (this.state.activeFolderName === "draft") {
			this.updateFolderState(draftFolder);
		}
	}

	/**
	 * Переносит письмо в корзину(в папку удаленные). В случае если удаление происходит из корзины, 
	 * то происходит безвозвратное удаление письма. 
	 */
	private async deleteActiveLetter(): Promise<void> {
		let folderData = this.getActiveFolder();
		if (folderData.activeLetterGuid === undefined)
			return;

		if (folderData.folderName !== "trash") {			
			await this.trashActiveLetter();
			return;
		}
		let doDelete = await Confirm("Удалить письмо без возможности последующего восстановления?");
		if (!doDelete)
			return;

		let letterIndex = folderData.letters.findIndex(x => x.guid === folderData.activeLetterGuid);
		if (letterIndex === -1)
			return;

		let letterToDelete = folderData.letters.splice(letterIndex, 1)[0];
		folderData.letters.slice();

		this.updateFolderState(folderData);
		
		await lettersService.deleteLetter(letterToDelete.guid);		
	}

	/**
	 * Переносит письмо в корзину(в папку удаленные)
	 */
	private async trashActiveLetter(): Promise<void>{
		let folderData = this.getActiveFolder();
		if (folderData.activeLetterGuid === undefined)
			return;

		let letterIndex = folderData.letters.findIndex(x => x.guid === folderData.activeLetterGuid);
		if (letterIndex === -1)
			return;

		let letterToTrash = folderData.letters.splice(letterIndex, 1)[0];
		folderData.letters.slice();

		this.updateFolderState(folderData);

		let trashFolder = this.state.folders.filter(x => x.folderName === "trash")[0];
		trashFolder.letters.unshift(letterToTrash)

		letterToTrash.state = LetterState.Trash;
		await lettersService.saveLetter(letterToTrash);
	}

	/**
	 * Обработчик события - отправлено уведомление о прочтении.
	 * Меняет статус письма на LetterState.IzvPr
	 */
	private readNotificationSended(letterGuid: string): void {
		// найдем письмо в папке "Входящие" и обновим статус. В другой папке письмо не должно находится.
		let incomingFolder = this.state.folders.filter(x => x.folderName === "incoming")[0];
		for (let i = 0; i < incomingFolder.letters.length; i++) {
			if (incomingFolder.letters[i].guid == letterGuid) {
				incomingFolder.letters[i] = Object.assign({}, incomingFolder.letters[i], { state: LetterState.IzvPr })
				break;
			}
		}

		if (this.state.activeFolderName === "incoming") {
			this.updateFolderState(incomingFolder);
		}
	}

	/**
	 * Получает активную папку
	 */
	private getActiveFolder(): FolderState {
		return this.state.folders.filter(x => x.folderName === this.state.activeFolderName)[0];
	}

	/**
	 * Делает копию папки и заменяет ее в списке папок в состоянии компонента. Сам список тоже копируется в новый. 
	 * Затем вызывается метод setState у компонента.
	 * @param folderData
	 */
	private updateFolderState(folderData: FolderState): void {
		let newFoldersList = this.state.folders.slice();
		for (let i = 0; i < newFoldersList.length; i++) {
			if (folderData === newFoldersList[i]) {
				newFoldersList[i] = Object.assign({}, folderData);
			}
		}
		this.setState({
			folders: newFoldersList
		});
	}
	/**
	 * Получает данные для инициализации вержнего тулбара
	 * @param folderData
	 */
	private getToolbarProps(folderData: FolderState | undefined): ToolbarProps {
		return {
			className: "a2-top-toolbar",
			buttonOnClick: this.toolbarOnClick,
			activeButtonName: this.state.activeToolbarButtonName,
			buttons: [
				{
					name: ToolbarButtonName.CreateLetter,
					label: "Создать",
					className: "a2-top-toolbar-button",
					isDisabled: !folderData
				},
				{
					name: ToolbarButtonName.EditLetter,
					label: "Редактировать",
					className: "a2-top-toolbar-button",
					isDisabled: !folderData || folderData.activeLetterGuid === undefined || folderData.folderName !== "draft"
				},
				{
					name: ToolbarButtonName.DeleteLetter,
					label: "Удалить",
					className: "a2-top-toolbar-button",
					isDisabled: !folderData || folderData.activeLetterGuid === undefined || (folderData.folderName !== "draft" && folderData.folderName !== "trash")
				},
				{
					name: ToolbarButtonName.ForwardLetter,
					label: "Переслать",
					className: "a2-top-toolbar-button",
					isDisabled: !folderData || folderData.activeLetterGuid === undefined
				},
				{
					name: ToolbarButtonName.AnswerLetter,
					label: "Ответить",
					className: "a2-top-toolbar-button",
					isDisabled: !folderData || folderData.activeLetterGuid === undefined
				},
				{
					name: ToolbarButtonName.OpenAddressBook,
					label: "Address book",
					className: "a2-top-toolbar-button",
					isDisabled: !folderData
				}
			]
		};
	}

	/**
	 * Получает данные для инициализации списка папок
	 * @param folderData
	 */
	private getVerticalMenuProps(folderData: FolderState | undefined): VerticalMenuProps {
		let buttonsIsDisabled = !folderData;
		return {
			className: "a2-folders-menu",
			activeButtonName: folderData && folderData.folderName,
			buttonOnClick: this.folderOnClick,
			buttons: [
				{
					className: "a2-folders-menu-button",
					isDisabled: buttonsIsDisabled,
					label: "Входящие",
					name: "incoming"
				},
				{
					className: "a2-folders-menu-button",
					isDisabled: buttonsIsDisabled,
					label: "Отправленные",
					name: "outcoming"
				},
				{
					className: "a2-folders-menu-button",
					isDisabled: buttonsIsDisabled,
					label: "Исходящие",
					name: "sending"
				},
				{
					className: "a2-folders-menu-button",
					isDisabled: buttonsIsDisabled,
					label: "Черновики",
					name: "draft"
				},
				{
					className: "a2-folders-menu-button",
					isDisabled: buttonsIsDisabled,
					label: "Удаленные",
					name: "trash"
				}
			]
		};
	}

	/**
	 * Получает данные для инициализации списка писем
	 * @param folderData
	 */
	private getLettersListProps(folderData: FolderState | undefined): LettersListProps {
		if (!folderData)
			return {
				letterOnClick: this.letterOnClick,
				loadMoreLetters: this.loadMoreLetters,
				letters: [],
				hasMoreLetters: false,
				pageLoaded: 0
			};

		return {
			activeLetterGuid: folderData.activeLetterGuid,
			letterOnClick: this.letterOnClick,
			loadMoreLetters: this.loadMoreLetters,
			letters: folderData.letters,
			hasMoreLetters: folderData.hasMoreLetters,
			pageLoaded: folderData.pageLoaded
		};
	}

	/**
	 * Получает данные для инициализации компонента - Информация о письме
	 * @param folderData
	 */
	private getLetterDetailsProps(folderData: FolderState | undefined): LetterDetailsProps | undefined {
		let activeLetter = folderData && folderData.letters.find(x => x.guid === folderData.activeLetterGuid);
		if (activeLetter === undefined)
			return undefined;

		return Object.assign({}, activeLetter, { readNotificationSended: this.readNotificationSended });
	}

	/**
	 * Выйти из системы
	 */
	private async logoutUser(): Promise<void> {
		await userService.logoutUser();
		Cookies.remove('accessToken');
		location.reload();
	}

	/**
	 * @internal
	 */
	public async componentDidMount(): Promise<void> {
		// покажем окно для авторизации
		let user = await LoginPopUp();

		this.setState({
			user: user
		});
		// загружаем письма
		this.setState({
			displayState: DisplayState.Ready,
			activeFolderName: "outcoming"
		});

		await this.loadMoreLetters(1);

		lettersService.queryMessagesHandler(this.modifyLetters);
	}
	
	/**
	 * @internal
	 */
	public render() {
		let folderData = this.getActiveFolder();
		let lettersListData = this.getLettersListProps(folderData);
        let toolbarData = this.getToolbarProps(folderData);
		let verticalMenuProps = this.getVerticalMenuProps(folderData);
		let letterDetailsProps = this.getLetterDetailsProps(folderData);

        return (
			<div>
				<TopPanel user={this.state.user} onLogOut={this.logoutUser} />
                <Toolbar {...toolbarData} />
                <table className="a2-application" cellPadding={0} cellSpacing={0}>
				    <tbody>
					    <tr>
						    <td className="a2-application-vertical_menu">
							    <VerticalMenu  {...verticalMenuProps} />
						    </td>
						    <td className="a2-application-letters_list">
							    <LettersList {...lettersListData} />
						    </td>
						    <td>
							    {letterDetailsProps && <LetterDetails {...letterDetailsProps} />}							
						    </td>
					    </tr>
				    </tbody>
			    </table>
            </div>
		);
    }
}

/**
 * Модель описывающая свойства компонента Application
 */
interface ApplicationProps { }

/**
 * Модель описывающая состояние компонента Application
 */
interface ApplicationState {

	/**
	 * Текущее состояние приложения
	 */
	displayState: DisplayState,

	/**
	 * Текущий пользователь ДСП
	 */
	user?: UserData,

	/**
	 * Текущая активная кнопка в тулбаре
	 */
	activeToolbarButtonName?: ToolbarButtonName,
	
	/**
	 * Текущая активная папка 
	 */
	activeFolderName?: FolderName,

	/**
	 * Список папок - входящие, отправленные, удаленные и тп
	 */
	folders: FolderState[]
}


/**
 * Модель описывающая состояние папки в компоненте Application
 */
interface FolderState {
	/**
	 * Название папки
	 */
	folderName: FolderName,

	/**
	 * Список писем
	 */
	letters: LetterData[],

	/**
	 * Нужно ли загружать следующие письма
	 */
	hasMoreLetters: boolean,

	/**
	 * Последняя загруженная страница(текущая страница)
	 */
	pageLoaded: number,

	/**
	 * Guid активного письма
	 */
	activeLetterGuid?: string
}

/**
 * Типы кнопок в тулбаре
 */
enum ToolbarButtonName {
	CreateLetter = "createLetter",
	EditLetter = "editLetter",
	DeleteLetter = "deleteLetter",
	ForwardLetter = "forwardLetter",
	AnswerLetter = "answerLetter",
	OpenAddressBook = "openAddressBook"
}

/**
 * Рендерим приложение
 */
ReactDOM.render(<Application  />, document.getElementById("content"));
