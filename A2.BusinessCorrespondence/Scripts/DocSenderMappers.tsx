﻿import { LetterData, LetterState, LetterAttachmentData, CertificateInfoData, ModifiedLetter } from "./Models";


/**
 * Модель описывающая базовые свойства I/O плагина
 */
export abstract class BaseDocSenderData {
	public readonly $type = "A2.DocSender.Client.InteractionObjects.AsyncObject, A2.DocSender.Client";
	public readonly xmlns = new Xmlns();
	public id?: string;
	public accessToken?: string;
	public item: any;
}

/**
 * Модель описывающая свойства I/O плагина
 */
export class DocSenderData<Item extends IBaseItem> extends BaseDocSenderData  {
	constructor(ctor?: NoParamConstructor<Item>) {
		super();
		if (ctor)
			this.item = new ctor();
	}
	
	public item: Item;
}

/**
 * Модель описывающая тип xml документа для сериализации
 */
export class Xmlns {
	public readonly $type = "System.Xml.Serialization.XmlSerializerNamespaces, System.Xml";
}

/**
 * Описывает базовый тип I/O плагина
 */
export interface IBaseItem {
	$type: ItemType;
}

/**
 * Описывает овтет в случае непредвиденной ошибки
 */
export class ResultException {
	constructor() {
		this.description = "При выполнии запроса в плагине произошла ошибка.";
	}
	public readonly $type = ItemType.ResultException;
	public description: string;
}

/**
 * Описывает данные для запроса - Аутентификация
 */
export class Authentication implements IBaseItem {
	public readonly $type = ItemType.Authentication;
	public readonly isOrgLikeUser = true;
	public certificateSerialNumber: string;
}

/**
 * Описывает ответ в  случае успеха аутентификации
 */
export class AuthenticationSucceed implements IBaseItem {
	public readonly $type = ItemType.AuthenticationSucceed;
	public accessToken: string;
}

/**
 * Описывает данные для запроса - Logout
 */
export class Logout implements IBaseItem {
	public readonly $type = ItemType.Logout;
}

/**
 * Описывает ответ в  случае успеха выхода из системы
 */
export class NotAuthorized implements IBaseItem {
	public readonly $type = ItemType.NotAuthorized;
}

/**
 * Описывает данные для запросов - Получение информации о пользователе, Получение документов, Получение информации о сертификатах пользователя
 */
export class Query implements IBaseItem {
	public readonly $type = ItemType.Query;
	public from: QueryType;
	public certSerial?: string;
}

/**
 * Описывает данные ответа на запрос Query
 */
export class QueryResult implements IBaseItem {
	public readonly $type = ItemType.QueryResult;
	public userInfo: {
		id: string,
		name: string,
		phone: string,
		orgInfos: {
			$values: { name: string, subjectOrganizationPosition: string }[]
		}
	};
	public certs: CertificateInfoData[];
	public certificateInfo: CertificateInfoData;
}

/**
 * Типы запросов для модели Query
 */
type QueryType = "UserInfo" | "Docs" | "PersonalCertificates" | "CertificatesWithDspService" | "CertificateInfo";

/**
 * Описывает данные для запроса - SaveDocToLocalDisk
 */
export class QuerySaveDocToLocalDisk implements IBaseItem {
	public readonly $type = ItemType.QuerySaveDocToLocalDisk;
	public fileName: string;
	public base64: string;
	public content: string;
}

/**
 * Описывает ответ на запрос SaveDocToLocalDisk
 */
export class ResultSaveDocToLocalDisk implements IBaseItem {
	public readonly $type = ItemType.ResultSaveDocToLocalDisk;
	public filePath: string;
}

/**
 * Описывает данные для запроса - Отправка письма
 */
export class SendingDocInfo implements IBaseItem {
	public readonly $type = ItemType.SendingDocInfo;
	public files: {
		path: string,
		data: string,
		isPlainText: boolean
	}[];
}

/**
 * Описывает ответ на запрос - Отправка письма
 */
export class SendingDocSucceed implements IBaseItem {
	public readonly $type = ItemType.SendingDocSucceed;
	public results: {
		result: SendingResults,
		fileName: string,
		chainId: string,
		docId: string
	}[]
}

/**
 * Описывает данные для запроса - Сохранение письма
 */
export class QuerySaveLetter implements IBaseItem {
	public readonly $type = ItemType.QuerySaveLetter;
	public letter: LetterData;
}

/**
 * Описывает ответ на запрос - Сохранение письма
 */
export class ResultSaveLetter implements IBaseItem {
	public readonly $type = ItemType.ResultSaveLetter;
}
/**
 * Описывает данные для запроса - Удаление письма
 */
export class QueryDeleteLetter implements IBaseItem {
	public readonly $type = ItemType.QueryDeleteLetter;
	public letterGuid: string;
}

/**
 * Описывает ответ на запрос - Удаление письма
 */
export class ResultDeleteLetter implements IBaseItem {
	public readonly $type = ItemType.ResultDeleteLetter;
}

/**
 * Описывает данные для запроса - Сохранение письма
 */
export class QueryGetLetters implements IBaseItem {
	public readonly $type = ItemType.QueryGetLetters;
	public page: number;
	public size: number;
	public letterState: LetterState;
}

/**
 * Описывает ответ на запрос - Сохранение письма
 */
export class ResultGetLetters implements IBaseItem {
	public readonly $type = ItemType.ResultGetLetters;
	public letters: {
		$values: LetterData[]
	}
}

/**
 * Описывает данные для запроса - Получение документа ввиде массива картинок
 */
export class QueryGetDocAsImages implements IBaseItem {
	public readonly $type = ItemType.QueryGetDocAsImages;

	/**
	 * Идентификатор документа (всего пакета zip)
	 */
	public docId: string;

	/**
	 * Идентификатор одного документа (в пакете zip содержатся множество файлов)
	 */
	public fileId: string;

	/**
	 * Полный путь к файлу (если не пустой то docId и fileId игнорируются)
	 */
	public filePath: string;
}

/**
 * Описывает ответ на запрос - Получение документа ввиде массива картинок
 */
export class ResultGetDocAsImages implements IBaseItem {
	public readonly $type = ItemType.ResultGetDocAsImages;
	public images: ImageData[];
}

/**
 * Описывает данные для запроса - Получение документа ввиде текста
 */
export class QueryGetDocAsText implements IBaseItem {
	public readonly $type = ItemType.QueryGetDocAsText;

	/**
	 * Идентификатор документа (всего пакета zip)
	 */
	public docId: string;

	/**
	 * Идентификатор одного документа (в пакете zip содержатся множество файлов)
	 */
	public fileId: string;
}

/**
 * Описывает ответ на запрос - Получение документа ввиде текста
 */
export class ResultGetDocAsText implements IBaseItem {
	public readonly $type = ItemType.ResultGetDocAsText;
	public content: string;
}

/**
 * Описывает данные для запроса - Получение прикрепленных файлов
 */
export class QueryGetAttachments implements IBaseItem {
	public readonly $type = ItemType.QueryGetAttachments;

	/**
	 * Идентификатор документа (всего пакета zip)
	 */
	public docId: string;
}

/**
 * Описывает ответ на запрос - Получение прикрепленных файлов
 */
export class ResultGetAttachments implements IBaseItem {
	public readonly $type = ItemType.ResultGetAttachments;
	public attachments: LetterAttachmentData[];
}

/**
 * Описывает данные для запроса - Скачать файл в папку Downloads
 */
export class GetFile implements IBaseItem {
	public readonly $type = ItemType.GetFile;

	/**
	 * Идентификатор документа (всего пакета zip)
	 */
	public docId: string;

	/**
	 * Идентификатор одного документа (в пакете zip содержатся множество файлов)
	 */
	public fileId: string;

	/**
	 * Полный путь к файлу (если не пустой то docId и fileId игнорируются)
	 */
	public filePath: string;

	/**
	 * Перенести в директорию Downloads
	 */
	public toDownloadsDir: boolean;
}

/**
 * Описывает ответ на запрос - Скачать файл в папку Downloads
 */
export class ExchangeDoc implements IBaseItem {
	public readonly $type = ItemType.ExchangeDoc;

	/**
	 * Путь к файлу
	 */
	public originalFilePath: string;
}


/**
 * Описывает данные для запроса - Открыть папку Downloads
 */
export class OpenDownloadsDir implements IBaseItem {
	public readonly $type = ItemType.OpenDownloadsDir;
}

/**
 * Описывает данные для запроса - Обработка входящих сообщений
 */
export class QueryMessagesHandler implements IBaseItem {
	public readonly $type = ItemType.QueryMessagesHandler;
}

/**
 * Описывает данные ответ на запрос - Обработка входящих сообщений
 */
export class ResultMessagesHandler implements IBaseItem {
	public readonly $type = ItemType.ResultMessagesHandler;
	public letters: ModifiedLetter[]
}

/**
 * Описывает ответ в случае успешного выполнения
 */
export class Succeed implements IBaseItem {
	public readonly $type = ItemType.Succeed;
}

/**
 * Описывает пустой конструктор. Необходим для создания генерик экземпляра item в классе DocSenderData. 
 * В typescript нельзя создать экземпляр генерика посредством простого вызова конструктора у генерика, например new TItem().
 * Поэтому конструктор для генерика следует описывать в отдельном интерфейсе.
 */
interface NoParamConstructor<T> {
	new(): T;
}

export enum SendingResults {
	Succeed,
	Failed
}


/**
 * Перечесление доступных запрос/ответов
 */
export enum ItemType {
	Succeed = "A2.DocSender.Client.InteractionObjects.Succeed, A2.DocSender.Client",
	ResultException = "A2.DocSender.Client.InteractionObjects.ResultObjects.ResultException, A2.DocSender.Client",
	Authentication = "A2.DocSender.Client.InteractionObjects.AuthenticationObjects.Authentication, A2.DocSender.Client",
	AuthenticationSucceed = "A2.DocSender.Client.InteractionObjects.AuthenticationObjects.AuthenticationSucceed, A2.DocSender.Client",
	Logout = "A2.DocSender.Client.InteractionObjects.AuthenticationObjects.Logout, A2.DocSender.Client",
	NotAuthorized = "A2.DocSender.Client.InteractionObjects.AuthenticationObjects.NotAuthorized, A2.DocSender.Client",
	Query = "A2.DocSender.Client.InteractionObjects.QueryObjects.Query, A2.DocSender.Client",
	QueryResult = "A2.DocSender.Client.InteractionObjects.QueryObjects.QueryResult, A2.DocSender.Client",
	QuerySaveDocToLocalDisk = "A2.DocSender.Client.InteractionObjects.FileObjects.QuerySaveDocToLocalDisk, A2.DocSender.Client",
	ResultSaveDocToLocalDisk = "A2.DocSender.Client.InteractionObjects.FileObjects.ResultSaveDocToLocalDisk, A2.DocSender.Client",
	SendingDocInfo = "A2.DocSender.Client.InteractionObjects.SendDocObjects.SendingDocInfo, A2.DocSender.Client",
	SendingDocSucceed = "A2.DocSender.Client.InteractionObjects.SendDocObjects.SendingDocSucceed, A2.DocSender.Client",
	QuerySaveLetter = "A2.DocSender.Client.InteractionObjects.QuerySaveLetter, A2.DocSender.Client",
	ResultSaveLetter = "A2.DocSender.Client.InteractionObjects.ResultSaveLetter, A2.DocSender.Client",
	QueryDeleteLetter = "A2.DocSender.Client.InteractionObjects.QueryDeleteLetter, A2.DocSender.Client",
	ResultDeleteLetter = "A2.DocSender.Client.InteractionObjects.ResultDeleteLetter, A2.DocSender.Client",
	QueryGetLetters = "A2.DocSender.Client.InteractionObjects.QueryGetLetters, A2.DocSender.Client",
	ResultGetLetters = "A2.DocSender.Client.InteractionObjects.ResultGetLetters, A2.DocSender.Client",
	QueryGetDocAsImages = "A2.DocSender.Client.InteractionObjects.FileO	bjects.QueryGetDocAsImages, A2.DocSender.Client",
	ResultGetDocAsImages = "A2.DocSender.Client.InteractionObjects.FileObjects.ResultGetDocAsImages, A2.DocSender.Client",
	QueryGetDocAsText = "A2.DocSender.Client.InteractionObjects.FileObjects.QueryGetDocAsText, A2.DocSender.Client",
	ResultGetDocAsText = "A2.DocSender.Client.InteractionObjects.FileObjects.ResultGetDocAsText, A2.DocSender.Client",
	QueryGetAttachments = "A2.DocSender.Client.InteractionObjects.FileObjects.QueryGetAttachments, A2.DocSender.Client",
	ResultGetAttachments = "A2.DocSender.Client.InteractionObjects.FileObjects.ResultGetAttachments, A2.DocSender.Client",
	GetFile = "A2.DocSender.Client.InteractionObjects.FileObjects.GetFile, A2.DocSender.Client",
	ExchangeDoc = "A2.DocSender.Client.InteractionObjects.FileObjects.ExchangeDoc, A2.DocSender.Client",
	OpenDownloadsDir = "A2.DocSender.Client.InteractionObjects.FileObjects.OpenDownloadsDir, A2.DocSender.Client",
	QueryMessagesHandler = "A2.DocSender.Client.InteractionObjects.MessagesHandlerObjects.QueryMessagesHandler, A2.DocSender.Client",
	ResultMessagesHandler = "A2.DocSender.Client.InteractionObjects.MessagesHandlerObjects.ResultMessagesHandler, A2.DocSender.Client"
}

/**
 * Словарь классов
 */
export interface ItemClassMap {
	[ItemType.Succeed]: Succeed,
	[ItemType.ResultException]: ResultException,
	[ItemType.Authentication]: Authentication,
	[ItemType.AuthenticationSucceed]: AuthenticationSucceed,
	[ItemType.Logout]: Logout,
	[ItemType.NotAuthorized]: NotAuthorized,
	[ItemType.Query]: Query,
	[ItemType.QueryResult]: QueryResult,
	[ItemType.QuerySaveDocToLocalDisk]: QuerySaveDocToLocalDisk,
	[ItemType.ResultSaveDocToLocalDisk]: ResultSaveDocToLocalDisk,
	[ItemType.SendingDocInfo]: SendingDocInfo,
	[ItemType.SendingDocSucceed]: SendingDocSucceed,
	[ItemType.QuerySaveLetter]: QuerySaveLetter,
	[ItemType.ResultSaveLetter]: ResultSaveLetter,
	[ItemType.QueryDeleteLetter]: QueryDeleteLetter,
	[ItemType.ResultDeleteLetter]: ResultDeleteLetter,
	[ItemType.QueryGetLetters]: QueryGetLetters,
	[ItemType.ResultGetLetters]: ResultGetLetters,
	[ItemType.QueryGetDocAsImages]: QueryGetDocAsImages,
	[ItemType.ResultGetDocAsImages]: ResultGetDocAsImages,
	[ItemType.QueryGetDocAsText]: QueryGetDocAsText,
	[ItemType.ResultGetDocAsText]: ResultGetDocAsText,
	[ItemType.QueryGetAttachments]: QueryGetAttachments,
	[ItemType.ResultGetAttachments]: ResultGetAttachments,
	[ItemType.GetFile]: GetFile,
	[ItemType.ExchangeDoc]: ExchangeDoc,
	[ItemType.OpenDownloadsDir]: OpenDownloadsDir,
	[ItemType.QueryMessagesHandler]: QueryMessagesHandler,
	[ItemType.ResultMessagesHandler]: ResultMessagesHandler
}

export namespace DocSenderMappers {

	/**
	 * Проверяет является ли item экземпляром класса с именем itemType
	 * @param item
	 * @param itemType
	 */
	export function IsItem(item: IBaseItem, itemType: string): boolean {
		return item && item.$type && item.$type == itemType;
	}
}