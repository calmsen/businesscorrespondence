﻿import * as React from "react"
import * as ReactDOM from "react-dom"
import { CertificateInfoData, SubscriberType } from "./Models";
import { popUp } from "./Components/Modal";
import { userService } from "./Services/UserService";
import { Toast } from "./Components/Toast";

/**
 * Показывает окно с информацией по сертификату
 */
export const CertificateInfoPopUp = async (certSerial: string): Promise<void> => {
	let certificateInfoProps: CertificateInfoProps = {
		certSerial: certSerial,
		close: () => cerificateInfoPopup.close(),
		center: () => cerificateInfoPopup.center()
	};
	// ---
	let cerificateInfoPopup = popUp(<CertificateInfo {...certificateInfoProps} />);
}

/**
 * Окно с информацией о сертификате
 */
class CertificateInfo extends React.Component<CertificateInfoProps, CertificateInfoState> {
	constructor(props: CertificateInfoProps) {
		super(props);

		this.tabOnClick = this.tabOnClick.bind(this);

		let certSerial = props.certSerial.replace(" ", "").toUpperCase();

		this.state = {
			activeTabName: "common",
			certificateInfo: CertificateInfo._cache.find(x => x.serialNumber == certSerial)
		};
	}

	/**
	 * Кэш данных по сертификатам
	 */
	private static _cache: CertificateInfoData[] = [];

	/**
	 * Устанавливает новый таб по имени таба
	 * @param name
	 */
	private tabOnClick(name: TabType): void {
		this.setState({ activeTabName: name });
	}


	/**
	 * @internal
	 */
	public async componentDidMount(): Promise<void> {
		if (!this.state.certificateInfo) {
			try {
				let certificateInfo = await userService.getCertificateInfo(this.props.certSerial);
				CertificateInfo._cache.push(certificateInfo);
				this.setState({
					certificateInfo: certificateInfo
				});
			}
			catch {
				Toast.showMessage("Ошибка", "Не удалось получить информацию о сертификате.");
			}
		}
	}

	/**
	 * @internal
	 */
	public componentDidUpdate() {
		this.props.center();
	}

	/**
	 * @internal
	 */
	public render() {
		const props = this.props;
		const state = this.state;

		return (
			<div className="a2-certificate_info">
				<div className="a2-certificate_info-header">
					Сведения о сертификате
					<span onClick={e => props.close()}>&#10006;</span>
				</div>
				{state.certificateInfo === undefined ?
					<div>Загрузка...</div> : 
					<div>
						<table className="a2-certificate_info-issue_info" cellPadding={0} cellSpacing={0}>
							<tbody>
								<tr>
									<td className="a2-certificate_info-key">Выдан кому: </td>
									<td className="a2-certificate_info-value">{state.certificateInfo.surname} {state.certificateInfo.name} {state.certificateInfo.patronymic}</td>
								</tr>
								<tr>
									<td className="a2-certificate_info-key">Выдан кем: </td>
									<td className="a2-certificate_info-value">{state.certificateInfo.issuerCommonName}</td>
								</tr>
								<tr>
									<td className="a2-certificate_info-key">Действителен: </td>
									<td className="a2-certificate_info-value">с {state.certificateInfo.issueDate} по {state.certificateInfo.endDate}</td>
								</tr>
							</tbody>
						</table>

						<table className="a2-certificate_info-tabs" cellPadding="0" cellSpacing="0">
							<tbody>
								<tr>
									<td className={"a2-certificate_info-tab" + (state.activeTabName === "common" ? " active" : "")} onClick={e => this.tabOnClick("common")}>Общие</td>
									<td className={"a2-certificate_info-tab" + (state.activeTabName === "content" ? " active" : "")} onClick={e => this.tabOnClick("content")}>Состав</td>
								</tr>
							</tbody>
						</table>

						{state.activeTabName === "common" ?
							<div className="a2-certificate_info-common_info">
								<div className="a2-common_info-header">Этот сертификат предназначен для:</div>
								<ul className="a2-common_info-list">
									{state.certificateInfo.assignments.map((a, i) => <li key={i}>{a}</li>)}
								</ul>
							</div>
							:
							<table className="a2-certificate_info-content_info" cellPadding={0} cellSpacing={0}>
								<tbody>
									<tr>
										<td className="a2-certificate_info-key">Версия: </td>
										<td className="a2-certificate_info-value">{state.certificateInfo.version}</td>
									</tr>
									<tr>
										<td className="a2-certificate_info-key">Серийный номер: </td>
										<td className="a2-certificate_info-value">{state.certificateInfo.serialNumber}</td>
									</tr>
									<tr>
										<td className="a2-certificate_info-key">Алгоритм подписи: </td>
										<td className="a2-certificate_info-value">{state.certificateInfo.signatureAlgorithm}</td>
									</tr>
									<tr>
										<td className="a2-certificate_info-key">Алгоритм хэширования подписи: </td>
										<td className="a2-certificate_info-value">{state.certificateInfo.hashAlgoritmSign}</td>
									</tr>
									<tr>
										<td className="a2-certificate_info-key">Публичный ключ: </td>
										<td className="a2-certificate_info-value">{state.certificateInfo.publicKey}</td>
									</tr>
									<tr>
										<td className="a2-certificate_info-key">Организация: </td>
										<td className="a2-certificate_info-value">{state.certificateInfo.organization}</td>
									</tr>
									<tr>
										<td className="a2-certificate_info-key">ИНН: </td>
										<td className="a2-certificate_info-value">{state.certificateInfo.inn}</td>
									</tr>
									<tr>
										<td className="a2-certificate_info-key">КПП: </td>
										<td className="a2-certificate_info-value">{state.certificateInfo.kpp}</td>
									</tr>
									<tr>
										<td className="a2-certificate_info-key">Занимаемая должность: </td>
										<td className="a2-certificate_info-value">{state.certificateInfo.post}</td>
									</tr>
								</tbody>
							</table>
						}
					</div>
					
				}
                <div className="a2-certificate_info-buttons_box">
                    <div className="a2-certificate_info-button_ok" onClick={e => props.close()}>ОК</div>
                </div>
			</div>
        );
	}
}

/**
 * Модель описывающая свойства компонента CertificateInfo
 */
interface CertificateInfoProps {

	/**
	 * Серийный номер сертификата
	 */
	certSerial: string,

	/**
	 * Закрывает окно
	 */
	close(): void,

	/**
	 * Выравнивает окно по центру
	 */
	center(): void
}

/**
 * Модель описывающая состояние компонента CertificateInfo
 */
interface CertificateInfoState {
	/**
	 * Информация о сертификате
	 */
	certificateInfo?: CertificateInfoData,

	/**
	 * Имя активного таба
	 */
	activeTabName: TabType
}

/**
 * Типы табов 
 */
type TabType = "common" | "content";


