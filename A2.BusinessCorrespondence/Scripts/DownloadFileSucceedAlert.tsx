﻿import * as React from "react";
import * as ReactDOM from "react-dom";
import { popUp, PopUp } from "./Components/Modal";
import { Utils } from "./Components/Utils";
import { filesService } from "./Services/FilesService";
import { Toast } from "./Components/Toast";

/**
 * Окно об успешном скачивании файла в папку Downloads
 * @param message
 */
export function DownloadFileSucceedAlert(filePathInDownloadsDir: string): Promise<void> {
	return new Promise((resolve, reject) => {
		let alertPopUp: PopUp = popUp(
			<div className="a2-alert-popup">
				<div className="a2-alert-popup-header">
					<span className="a2-alert-popup-close_btn" onClick={e => { resolve(); alertPopUp.close(); }}>&#10006;</span>
				</div>
				<div className="a2-alert-popup-body">
					Файл {Utils.getFileName(filePathInDownloadsDir)} сохранен в папку <a title="Открыть папку Downloads" onClick={e => { e.preventDefault(); openDownloadsDir(); }}>Downloads.</a>
				</div>
				<div className="a2-alert-popup-footer">
					<div className="a2-alert-popup-apply_btn" onClick={e => { resolve(); alertPopUp.close(); }}>OK</div>
				</div>
			</div>
		);
	});
}

/**
 * Открывает папку Downloads
 */
async function openDownloadsDir(): Promise<void> {
	try {
		await filesService.openDownloadsDir();
	}
	catch {
		Toast.showMessage("Ошибка", "Не получилось открыть папку Downloads");
	}
}
