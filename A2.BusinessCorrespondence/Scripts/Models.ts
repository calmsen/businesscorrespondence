﻿
/**
 * Типы папок
 */
export type FolderName = "incoming" | "outcoming" | "sending" | "draft" | "trash";

/**
 * Модель описывающая данные для письма
 */
export interface LetterData {
	/**
	 * GUID письма в нашей системе
	 */
	guid: string,

	/**
	 * Заголовок письма
	 */
	subject: string,

	/**
	 * Дата создания письма
	 */
	date: Date,

	/**
	 * Отправитель
	 */
	sender: UserData,

	/**
	 * Получатель
	 */
	receiver: UserData,

	/**
	 * Состояние письма
	 */
	state: LetterState,

	/**
	 * Направление письма
	 */
	direction: Direction,

	/**
	 * Тело письма (обычный текст)
	 */
	body?: string, 

	/**
	 * Прикрепленные документы
	 */
	attachments?: LetterAttachmentData[],

	/**
	 * Идентификатор главного документа (по своей сути это идентификатор главного файла в пакете документов )
	 */
	mainDocId?: string,

	/**
	 * Требовать извещение о прочтении
	 */
	readNotification?: boolean,

	/**
	 * Идентификатор цепочки документов
	 */
	chainId?: string,
}

/**
 * Измененное письмо
 */
export interface ModifiedLetter {

	/**
	 * Guid письма на сервере. Перед тем как сравнивать с LetterData, нужно быть уверенным, 
	 * что у локально созданного письма guid это именно серверный
	 */
	serverGuid: string,

	/**
	 * Состояние письма
	 */
	state: LetterState,

	/**
	 * Направление письма
	 */
	direction: Direction,
}

/**
 * Модель описывающая прикрепленный файл к письму
 */
export interface LetterAttachmentData {
	/**
	 * Guid файла
	 */
	guid: string,

	/**
	 * Полный путь к файлу
	 */
	filePath: string,

	/**
	 * Название документа
	 */
	label: string,

	/**
	 * Тип документа
	 */
	type: AttachmentType,

	/**
	 * Файл хранится только на локальной компьютере пользователя
	 */
	isLocal?: boolean
}

/**
 * Типы файлов
 */
export enum AttachmentType {
	/**
	 * Картинка
	 */
	Image,

	/**
	 * PDF
	 */
	Pdf,

	/**
	 * Microsoft office doсument
	 */
	MsDoc,

	/**
	 * Обычный текст
	 */
	Plain,

	/**
	 * Другой тип
	 */
	Other
}

/**
 * Состояние письма.
 */
export enum LetterState {
	/**
	 * Черновик
	 */
	Draft,

	/**
	 * Удаленные
	 */
	Trash,

	/**
	 * Исходящее
	 */
	Sending,

	/**
	 * Отправлен основной документ
	 */
	MainDoc,

	/**
	 * Отправлен документ извещение о получении
	 */
	IzvPol,

	/**
	 * Отправлен документ извещение о прочтении
	 */
	IzvPr,

	/** 
	 * Ошибка
	 */
	Error
}

/**
 * Направление письма
 */
export enum Direction {
	/**
	 * Входящее
	 */
	Incoming,

	/**
	 * Исходящее
	 */
	Outcoming
}

/**
 * Модель описывающая данные контрагента полученного при поиске
 */
export interface UserData {
	/**
	 * Идентификатор контрагента
	 */
	userId: number,

	/**
	 * Фамилия полное, имя и отчество заглавные буквы 
	 */
	shortName: string,

	/**
	 * Полное имя контрагента
	 */
	fullName: string,
	
	/**
	 * Электронный адрес контрагента
	 */
	email?: string,

	/**
	 * Номер телефона контрагента
	 */
	phone?: string,

	/**
	 * Организация контрагента
	 */
	orgName: string,

	/**
	 * Должность контрагента
	 */
	post: string,

	/**
	 * Удостоверен ли контрагент
	 */
	isConfirmed: boolean,

	/**
	* Серийный номер сертификата, на котором активирована услуга
	*/
	certSerial?: string,


	/**
	* Идентификатор организации
	*/
	orgId?: number
}

/**
 * Модель описывающая данные сертификата
 */
export interface CertificateInfoData {
	/**
	 * Серийный номер сертификата
	 */
	serialNumber: string,

	/**
	 * Версия сертификата
	 */
	version: string,

	/**
	 * Алгоритм подписи
	 */
	signatureAlgorithm: string,

	/**
	 * Алгоритм хэширования подписи
	 */
	hashAlgoritmSign: string,

	/**
	 * Название организации выпустившей сертификат
	 */
	issuerCommonName: string,

	/**
	 * Электронный адрес издателя
	 */
	issuerEmail: string,

	/**
	 * Дата выпуска сертификата 
	 */
	issueDate: string,

	/**
	 * Дата окончания действия сертификата
	 */
	endDate: string,

	/**
	 * Публичный ключ сертификата
	 */
	publicKey: string,

	/**
	 * Фамилия абонента
	 */
	surname: string,

	/**
	 * Имя абонента
	 */
	name: string,

	/**
	 * Отчество абонента
	 */
	patronymic: string,

	/**
	 * Электронный адрес абонента
	 */
	email: string,

	/**
	 * Название организации
	 */
	organization: string,

	/**
	 * Должность
	 */
	post: string,

	/**
	 * ИНН
	 */
	inn: string,

	/**
	 * КПП
	 */
	kpp: string,

	/**
	 * Предназначения
	 */
	assignments: string[]
}

/**
 * Типы абонентов
 */
export enum SubscriberType {
	/**
	 * Отправитель
	 */
	Sender,

	/**
	 * Получталь
	 */
	Receiver
}

/**
 * Модель описывающая данные организации
 */
export interface OrganisationData {
	/**
	 * Идентификатор организации
	 */
	id: number,

	/**
	 * Название организации
	 */
	name: string,

	/**
	 * Сотрудники организации
	 */
	members: UserData[]
}