﻿import * as Console from "../Components/Console"
import * as React from "react";
import * as ReactDOM from "react-dom";
import { UserData, OrganisationData, LetterData } from "../Models";
import { BaseService } from "./BaseService";

/**
 * Класс для работы с адресной книгой
 */
class AddressBookService extends BaseService {
	constructor() {
		super("http://localhost:31169/address-book/subscribers/");
	}

	/**
	 * Находит пользователей по строке запроса
	 * @param query
	 */
	public async findUsers(query: string): Promise<UserData[]> {
		return await this.sendGet(`search/${query}`);
	}

	/**
	 * Добавляет пользователя в адресную книгу
	 */
	public async addSubscriber(user: UserData): Promise<void> {
		if (Number(user.userId) > 0) 
			await this.sendPost("", user);
	}


	/**
	 * Cписок организаций с последними использованными контрагентами для указанного пользователя
	 * @param userId
	 */
	public async getOrganisations(userId: number): Promise<OrganisationData[]> {
		let users = await this.sendGet("") as UserData[];
		Console.log(users)
		let organisations = [] as OrganisationData[];
		for (let i = 0; i < users.length; i++) {
			let org = organisations.find(x => x.id == users[i].orgId);
			if (!org) {
				let orgId = users[i].orgId;
				if (orgId !== undefined) {
					org = {
						id: orgId,
						name: users[i].orgName,
						members: []
					};

					organisations.push(org);
				}
			}
			if (org !== undefined)
				org.members.push(users[i]);

		}
		Console.log(organisations)
		return organisations;

	}
}

/**
 * Экземпляр класса AddressBookService
 */
export const addressBookService = new AddressBookService();