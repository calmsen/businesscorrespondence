﻿import {  ImageData } from "../Components/ImageViewer";
import { LetterAttachmentData, AttachmentType } from "../Models";
import { QuerySaveDocToLocalDisk, ResultSaveDocToLocalDisk, QueryGetDocAsImages, ResultGetDocAsImages, QueryGetDocAsText, ResultGetDocAsText, QueryGetAttachments, ResultGetAttachments, GetFile, ExchangeDoc, OpenDownloadsDir, Succeed, ItemType } from "../DocSenderMappers";
import { Utils } from "../Components/Utils";
import { BaseService } from "./BaseService";

/**
 * Класс для работы с файлами
 */
class FilesService extends BaseService {

	/**
	 * Сохраняет файл в папку пользователя
	 * @param fileName
	 * @param base64
	 */
	public async saveDocToLocalDisk(fileName: string, base64: string): Promise<LetterAttachmentData> {
		let request = new QuerySaveDocToLocalDisk();
		request.fileName = fileName;
		request.base64 = base64.indexOf(",") >= 0
			? base64.split(",")[1]
			: base64;

		let response = await this.executeAsync(request, ItemType.ResultSaveDocToLocalDisk);
		
		return {
			guid: Utils.guid(),
			label: fileName,
			filePath: response.filePath,
			type: AttachmentType.Other,
			isLocal: true
		};
	}

	/**
	 * Получает документ ввиде списка картинок - каждая картинка одна страница
	 */
	public async getDocAsImages(docId: string, fileId: string, filePath: string, isLocal: boolean): Promise<ImageData[]> {
		let request = new QueryGetDocAsImages();
		if (isLocal) {
			request.filePath = filePath;
		}
		else {
			request.docId = docId;
			request.fileId = fileId;
		}
		
		let response = await this.executeAsync(request, ItemType.ResultGetDocAsImages);
		
		return (response.images as any).$values;
	}

	/**
	 * Кэш для результатов функции getDocAsText
	 */
	public readonly docAsTextCache: { docId: string, content: string }[] = [];

	/**
	 * Получает документ ввиде текста
	 */
	public async getDocAsText (docId: string, fileId: string): Promise<string> {
		let request = new QueryGetDocAsText();
		request.docId = docId;
		request.fileId = fileId;

		let response = await this.executeAsync(request, ItemType.ResultGetDocAsText);
				
		return response.content;
	}

	/**
	 * Кэш для результатов функции getAttachments
	 */
	public readonly attachmentsCache: { docId: string, attachments: LetterAttachmentData[] }[] = [];

	/**
	 * Получает прикрепленные документы
	 */
	public async getAttachments(docId: string): Promise<LetterAttachmentData[]> {
		let request = new QueryGetAttachments();
		request.docId = docId;

		let response = await this.executeAsync(request, ItemType.ResultGetAttachments);
		
		let attachments = (response.attachments as any).$values as LetterAttachmentData[];
		attachments.forEach(x => x.isLocal = false);
		return attachments;
	}

	/**
	 * Скачать файл в папку Downloads
	 */
	public async downloadFile(docId: string, fileId: string, filePath: string, isLocal: boolean): Promise<string> {
		let request = new GetFile();
		if (isLocal) {
			request.filePath = filePath;
		}
		else {
			request.docId = docId;
			request.fileId = fileId;
		}
		request.toDownloadsDir = true;

		let response = await this.executeAsync(request, ItemType.ExchangeDoc);
		
		return response.originalFilePath;
	}

	/**
	 * Открывает папку Downloads
	 */
	public async openDownloadsDir(): Promise<void> {
		let request = new OpenDownloadsDir();

		let response = await this.executeAsync(request, ItemType.Succeed);
	}	

}

/**
 * Экземпляр класса FilesService
 */
export const filesService = new FilesService();