﻿import * as React from "react";
import * as ReactDOM from "react-dom";
import { LetterData, LetterState, Direction, AttachmentType, FolderName, LetterAttachmentData, UserData, ModifiedLetter } from "../Models";
import { QuerySaveDocToLocalDisk, ResultSaveDocToLocalDisk, SendingDocInfo, SendingDocSucceed, QuerySaveLetter, ResultSaveLetter, QueryDeleteLetter, ResultDeleteLetter, QueryGetLetters, ResultGetLetters, ItemType, QueryMessagesHandler, ResultMessagesHandler, DocSenderData } from "../DocSenderMappers";
import { XMLBuilder } from "../Components/XMLBuilder";
import { Utils } from "../Components/Utils";
import { userService } from "./UserService";
import { BaseService } from "./BaseService";
import * as Console from "../Components/Console"

/**
 * Класс для работы с письмами
 */
class LettersService extends BaseService {
	constructor() {
		super("http://localhost:31169/letters/");
	}

	/**
	 * Имя главного документа, представляющего описание документа
	 */
	private readonly MAIN_DOC_NAME = "ТелоПисьма.txt";
	
	/**
	 * Получает письма для указанной страницы
	 * @param page номер страницы которую нужно получить
	 * @param size количество писем на странице
	 * @param folderName папка для которой загружаются письма
	 */
	public async getLetters(page: number, size: number, folderName: FolderName): Promise<LetterData[]> {
		if (folderName === "draft" || folderName === "trash" || folderName === "sending") {
			let request = new QueryGetLetters();
			request.page = page;
			request.size = size;
			request.letterState = folderName === "draft"
				? LetterState.Draft
				: (folderName === "trash" ? LetterState.Trash : LetterState.Sending);

			let response = await this.executeAsync(request, ItemType.ResultGetLetters);
			
			let letters = response.letters.$values;
			letters.forEach(x => {
				x.date = Utils.parseDate(x.date.toString());
				x.sender = userService.currentUser;
				x.attachments = (x.attachments as any).$values as LetterAttachmentData[];
				x.attachments.forEach(x => x.isLocal = true);
			});
			return letters;
		}

		let letters = await this.sendGet(`?page=${page}&size=${size}&direction=${folderName === "incoming" ? Direction.Incoming : Direction.Outcoming}`) as LetterData[];
		letters.forEach(x => {
			x.date = Utils.parseDate(x.date.toString());
		});
		Console.log(letters);
		return letters;
	}

	/**
	 * Получение письма
	 * @param letter
	 */
	public async getLetter(letterGuid: string): Promise<LetterData> {		
		let letter = await this.sendGet(letterGuid) as LetterData;
		letter.date = Utils.parseDate(letter.date.toString());
		return letter;
	}

	/**
	 * Сохраняет письмо
	 * @param letter
	 */
	public async saveLetter(letter: LetterData): Promise<void> {
		let request = new QuerySaveLetter();
		request.letter = Object.assign({}, letter, { date: Utils.formatDate(letter.date, "yyyy-MM-dd HH:mm:ss") })
		let response = await this.executeAsync(request, ItemType.ResultSaveLetter);
	}

	/**
	 * Удаляет письмо
	 * @param letterGuid
	 */
	public async deleteLetter(letterGuid: string): Promise<void> {
		let request = new QueryDeleteLetter();
		request.letterGuid = letterGuid;
		let response = await this.executeAsync(request, ItemType.ResultDeleteLetter);
	}

	/**
	 * Отправляет письмо
	 * @param letter
	 */
	public async sendLetter(letter: LetterData): Promise<string> {
		let mainDocFile: string | undefined;
		if (letter.body)
			mainDocFile = await this.saveLetterBodyToLocalDisk(letter.body);;

		let packageFile = this.createMainPackageDescriptionXml(letter, mainDocFile);
		
		let request = new SendingDocInfo();
		request.files = [{
			path: "packageDescription_Dsp.xml",
			data: packageFile,
			isPlainText: true
		}];
		let response = await this.executeAsync(request, ItemType.SendingDocSucceed, r => r.id = letter.guid);
		response.results = (response.results as any).$values;

		if (response.results.length === 0 && !response.results[0].docId)
			throw new Error("Произошли ошибки при отправке письма.");

		return response.results[0].docId;
	}

	/**
	 * Сохраняет тело документа в текстовый файл в папку пользователя
	 * @param content
	 */
	public async saveLetterBodyToLocalDisk(content: string): Promise<string | undefined> {
		let request = new QuerySaveDocToLocalDisk();
		request.fileName = this.MAIN_DOC_NAME; 
		request.content = content;

		let response = await this.executeAsync(request, ItemType.ResultSaveDocToLocalDisk);
				
		return response.filePath;
	}


	/**
	 * Отправляет уведомление о прочтении
	 */
	public async sendReadNotification(chainId: string): Promise<void> {
		let packageFile = this.createReadNotificationPackageDescriptionXml(chainId);

		let request = new SendingDocInfo();
		request.files = [{
			path: "packageDescription_Dsp_Izvpr.xml",
			data: packageFile,
			isPlainText: true
		}];
		await this.executeAsync(request, ItemType.SendingDocSucceed);
	}

	private readonly _interval = 15000; 
	private _lastInvokedDate: Date; 
	/**
	 * Обработка входящих сообщений
	 */
	public async queryMessagesHandler(modifyLetters: (letters: ModifiedLetter[]) => void): Promise<void> {
		this._lastInvokedDate = new Date(); 
		let onResponseCallback = (response: DocSenderData<ResultMessagesHandler>) => {
			let letters = (response.item.letters as any).$values;
			modifyLetters(letters);

			let timer = this._interval - (new Date().getTime() - this._lastInvokedDate.getTime());
			timer = timer < 0 ? 0 : timer;
			
			setTimeout(() => this.queryMessagesHandler(modifyLetters), timer)
		};

		let request = new QueryMessagesHandler();
		await this.executeAsync(request, ItemType.ResultMessagesHandler, undefined, onResponseCallback);
	}

	private queryMessagesHandlerByTimer() {
	}

	/**
	 * Создает xml, описывающий основное письмо
	 */
	private createMainPackageDescriptionXml(letter: LetterData, mainDocFile: string | undefined): string {
		if (letter.receiver.certSerial === undefined) {
			throw new Error("Не известен серийный номер получателя.");
		}

		let xmlDoc = new XMLBuilder("ТрансИнф");
		xmlDoc.setAttribute("типДокументооборота", "SC_DSP");
		xmlDoc.setAttribute("типТранзакции", "Документ_SC_DSP");

		let additionalInfo = xmlDoc.createElement("дополнительнаяИнформация");
		additionalInfo.setAttribute("имя", "темаСообщения");
		additionalInfo.setAttribute("значение", letter.subject);
		xmlDoc.appendChild(additionalInfo);

		additionalInfo = xmlDoc.createElement("дополнительнаяИнформация");
		additionalInfo.setAttribute("имя", "извещениеОПрочтении");
		additionalInfo.setAttribute("значение", letter.readNotification === true ? "Y" : "N");
		xmlDoc.appendChild(additionalInfo);

		let receiver = xmlDoc.createElement("получатель");
		receiver.setAttribute("сертификатСубъекта", letter.receiver.certSerial);
		xmlDoc.appendChild(receiver);

		if (mainDocFile) {
			let mainDoc = xmlDoc.createElement("документ");
			mainDoc.setAttribute("типДокумента", "ТелоПисьма");
			mainDoc.setAttribute("исходноеИмяФайла", mainDocFile);
			mainDoc.setAttribute("типСодержимого", "txt");
			xmlDoc.appendChild(mainDoc);
		}

		if (letter.attachments && letter.attachments.length > 0) {
			for (let attachment of letter.attachments) {
				let doc = xmlDoc.createElement("документ");
				doc.setAttribute("типДокумента", "Документ");
				doc.setAttribute("исходноеИмяФайла", attachment.filePath);
				doc.setAttribute("типСодержимого", Utils.getFileExtension(attachment.filePath));
				xmlDoc.appendChild(doc);
			}
		}
		return xmlDoc.toString();
	}

	/**
	 * Создает xml, описывающий уведомление о прочтении
	 */
	private createReadNotificationPackageDescriptionXml(chainId: string): string {
		let xmlDoc = new XMLBuilder("ТрансИнф");
		xmlDoc.setAttribute("типДокументооборота", "SC_DSP");
		xmlDoc.setAttribute("типТранзакции", "ИзвещениеОПрочтении_SC_DSP");
		xmlDoc.setAttribute("идентификаторДокументооборота", chainId);
		return xmlDoc.toString();
	}

}

/**
 * Экземпляр класса LettersService
 */
export const lettersService = new LettersService();