﻿import { DocSenderData, IBaseItem, BaseDocSenderData, DocSenderMappers, ItemClassMap, ItemType } from "../DocSenderMappers";
import { DocSender } from "../DocSender";
import "fetch";

/**
 * Базовый класс для работы с плагином
 */
export abstract class BaseService {
	constructor(baseUrl?: string) {
		if (baseUrl)
			this.baseUrl = baseUrl;
	}

	/**
	 * Базовый URL для работы с API
	 */
	private baseUrl: string;

	/**
	 * Выполняет запрос в плагин
	 * @param requestItem
	 * @param responseItemType
	 * @param beforeSend
	 */
	protected async executeAsync<TResponseItemType extends keyof ItemClassMap, TResponseItem extends ItemClassMap[TResponseItemType]>(requestItem: IBaseItem, responseItemType: TResponseItemType,
		beforeSend?: (request: BaseDocSenderData) => void, complete?: (response: BaseDocSenderData) => void): Promise<TResponseItem> {

		let request = new DocSenderData();
		request.item = requestItem;

		beforeSend && beforeSend(request);

		let response = await DocSender.getInstance()
			.executeAsync<DocSenderData<TResponseItem>>(request, complete);

		if (DocSenderMappers.IsItem(response.item, responseItemType)) {
			return response.item;
		}
		else {
			throw new Error("Произошла ошибка при выполнения запроса.");
		}
	}

	/**
	 * Выполняет GET запрос в API
	 * @param uri
	 */
	protected async sendGet(uri: string): Promise<any> {
		let options = {
			method: 'get',
			headers: this.getDefaultHeaders()
		};

		let response = await fetch(`${this.baseUrl}${uri.replace(/^\//, "")}`, options);
		if (response.ok) {
			return await response.json();
		}
		else {
			throw new Error("Произошла ошибка при выполнения запроса на удаленный сервер.");
		}
	}

	/**
	 * Выполняет POST запрос в API
	 * @param uri
	 * @param data
	 */
	protected async sendPost(uri: string, data: any): Promise<any> {
		let options = {
			method: 'post',
			headers: this.getDefaultHeaders(),
			body: JSON.stringify(data)
		};

		let response = await fetch(`${this.baseUrl}${uri.replace(/^\//, "")}`, options);
		if (response.ok) {
			return await response.json();
		}
		else {
			throw new Error("Произошла ошибка при выполнения запроса на удаленный сервер.");
		}
	}

	/**
	 * Получает заголовки по умолчанию
	 */
	private getDefaultHeaders(): any {
		return {
			"Content-type": "application/json; charset=UTF-8",
			"Authorization": "Bearer " + DocSender.getInstance().getAccessToken()
		};
	}
}


