﻿import * as React from "react";
import * as ReactDOM from "react-dom";
import { UserData, CertificateInfoData } from "../Models";
import { Authentication, AuthenticationSucceed, Query, QueryResult, Logout, ItemType } from "../DocSenderMappers";
import { BaseService } from "./BaseService";
import { Utils } from "../Components/Utils";
import * as Console from "../Components/Console"

/**
 * Класс для работы с данными о пользователе
 */
class UserService  extends BaseService{

	/**
	 * Текущий пользователь ДСП
	 */
	public currentUser: UserData;

	/**
	 * Получает информацию о текущем пользователе
	 */
	public async getUserInfo(): Promise<UserData> {
		let request = new Query();
		request.from = "UserInfo";

		let response = await this.executeAsync(request, ItemType.QueryResult);
		
		let userInfo = response.userInfo;
		let orgInfo = userInfo.orgInfos.$values[0];
		let user: UserData = {
			userId: Number(userInfo.id),
			shortName: Utils.getUserShortName(userInfo.name),
			fullName: userInfo.name,
			orgName: orgInfo.name,
			post: orgInfo.subjectOrganizationPosition,
			isConfirmed: true
		};
		this.currentUser = user;
		return user;
	}

	/**
	 * Войти в систему 
	 */
	public async loginUser(certSerial: string): Promise<string> {
		let request = new Authentication();
		request.certificateSerialNumber = certSerial;

		let response = await this.executeAsync(request, ItemType.AuthenticationSucceed);
		return response.accessToken;
			
	}

	/**
	 * Выйти из системы
	 */
	public async logoutUser(): Promise<void> {
		let request = new Logout();
		let response = await this.executeAsync(request, ItemType.NotAuthorized);
	}


	/**
	 * Получает информацию о сертификате
	 */
	public async  getCertificateInfo(certSerial: string): Promise<CertificateInfoData> {
		let request = new Query();
		request.from = "CertificateInfo";
		request.certSerial = certSerial;

		let response = await this.executeAsync(request, ItemType.QueryResult);
		
		let certificateInfo = response.certificateInfo;
		certificateInfo.assignments = (certificateInfo.assignments as any).$values;
		return certificateInfo;
	}

	/**
	 * Получает информацию о сертификатах
	 */
	public async  getCertificatesWithDspService(): Promise<CertificateInfoData[]> {
		let request = new Query();
		request.from = "CertificatesWithDspService";

		let response = await this.executeAsync(request, ItemType.QueryResult);		
		let certificates = (response.certs as any).$values as CertificateInfoData[];
		return certificates;
	}
}

/**
 * Экземпляр класса UserService
 */
export const userService = new UserService();