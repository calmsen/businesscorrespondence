﻿import * as React from "react"
import * as ReactDOM from "react-dom"
import * as Console from "./Components/Console"
import { UserData, LetterData, Direction, LetterState, LetterAttachmentData } from "./Models";
import { AddressBookPopUp } from "./AddressBook";
import { popUp } from "./Components/Modal";
import { ToolbarProps, Toolbar } from "./Components/Toolbar";
import { lettersService } from "./Services/LetterService";
import { Alert } from "./Components/Alert";
import { filesService } from "./Services/FilesService";
import { LetterAttachments, LetterAttachmentsProps, LetterAttachmentProps, LetterAttachmentState } from "./LetterAttachments";
import { Utils } from "./Components/Utils";
import { Confirm } from "./Components/Confirm";
import { addressBookService } from "./Services/AddressBookService";
import { Toast } from "./Components/Toast";
import "tinymce";
import * as classNames from "classnames";

/**
 * Редактор html
 */
declare global {
	const tinymce: any;
} 

/**
 * Показывает окно с формой для создания письма. 
 */
export const LetterFormPopUp = async (sender: UserData, letterSended: (letterGuid: string, serverLetterGuid: string) => void,
	letterDrafted: (letter: LetterData) => void, editingLetter: LetterData | undefined = undefined): Promise<LetterData> => {

	return new Promise<LetterData>((resolve, reject) => {
		let letterFormProps: LetterFormProps = {
			sender: sender,
			close: () => {
				letterFormPopUp.close();
				reject();
			},
			center: () => letterFormPopUp.center(),
			letterSending: (letter: LetterData) => {
				resolve(letter);
			},
			letterSended: letterSended,
			letterDrafted: letterDrafted,
			editingLetter: editingLetter
		};
		let letterFormPopUp = popUp(<LetterForm {...letterFormProps} />);

	});
}

/**
 * Форма создания письма
 */
export class LetterForm extends React.Component<LetterFormProps, LetterFormState> {

	constructor(props: LetterFormProps) {
		super(props);		

		this.subjectFieldOnInput = this.subjectFieldOnInput.bind(this);
		this.toolbarOnClick = this.toolbarOnClick.bind(this);
		this.addAttachments = this.addAttachments.bind(this);
		this.replaceAttachment = this.replaceAttachment.bind(this);
		this.removeAttachment = this.removeAttachment.bind(this);
		this.toggleReadNotification = this.toggleReadNotification.bind(this);

		this.state = {
			subject: props.editingLetter && props.editingLetter.subject,
			body: props.editingLetter && props.editingLetter.body,
			receiver: props.editingLetter && props.editingLetter.receiver,
			attachments: props.editingLetter && props.editingLetter.attachments || [],
			attachmentsIsReady: true
		};
	}

	/**
	 * Выбирает получателя
	 */
	private async selectReceiver(): Promise<void> {
		let receiver = await AddressBookPopUp(this.props.sender.userId);
		this.setState({
			receiver: receiver
		});
		
		await addressBookService.addSubscriber(receiver);
	}

	/**
	 * Проверяет форму
	 */
	private async validateForm(): Promise<boolean> {
		let isValidate = this.state.subject && this.state.body && this.state.receiver;
		
		if (!isValidate) {
			await Alert("Не все поля заполнены.");
			return false;
		}
		return true;
	}

	/**
	 * Отправляет письмо
	 */
	private async sendLetter(): Promise<void> {
		let isValidate = await this.validateForm();
		if (!isValidate) 
			return;

		if (!(this.state.subject && this.state.body && this.state.receiver))
			return;

		if (!this.state.attachmentsIsReady) {
			await Alert("Не все файлы подготовлены для отправки.");
			return;
		}

		if (this.state.attachments.filter(x => x.state === LetterAttachmentState.Failed).length > 0) {
			await Alert("Некоторые файлы не удалось загрузить.");
			return;
		}

		let letterGuid = (this.props.editingLetter && this.props.editingLetter.guid) || Utils.guid();

		let letter: LetterData = {
			guid: letterGuid,
			subject: this.state.subject,
			body: this.state.body,
			sender: this.props.sender,
			receiver: this.state.receiver,
			date: new Date(),
			direction: Direction.Outcoming,
			state: LetterState.Sending,
			attachments: this.state.attachments,
			readNotification: this.state.readNotification
		};


		this.props.letterSending(letter);
		this.props.close();
		
		try {
			await lettersService.saveLetter(letter);
		}
		catch {
			Toast.showMessage("Ошибка", "Не удалось отправить письмо.");
			return;
		}

		try {
			let serverLetterGuid = await lettersService.sendLetter(letter);
			this.props.letterSended(letter.guid, serverLetterGuid);
		}
		catch {
			Toast.showMessage("Ошибка", "Не удалось отправить письмо.");
			return;
		}
		
	}

	/**
	 * Добавить письмо в черновик
	 */
	private async draftLetter(): Promise<void> {
		if (!(this.state.subject || this.state.body || this.state.receiver || this.state.attachments.length)) {
			this.props.close();
			return;
		}

		if (this.props.editingLetter) {
			let oldLetter = this.props.editingLetter;
			let isNeedToDraft = false;
			if ((oldLetter.subject || "").trim() !== (this.state.subject || "").trim()) {
				isNeedToDraft = true;
			}
			if ((oldLetter.body || "").trim() !== (this.state.body || "").trim()) {
				isNeedToDraft = true;
			}
			if ((oldLetter.receiver && oldLetter.receiver.certSerial || "").replace('-', '').toUpperCase()
				!== (this.state.receiver && this.state.receiver.certSerial || "").replace('-', '').toUpperCase()) {
				isNeedToDraft = true;
			}
			if ((oldLetter.attachments || []).length !== this.state.attachments.length) {
				isNeedToDraft = true;
			}
			if ((oldLetter.readNotification || "N") !== (this.state.readNotification || "N")) {
				isNeedToDraft = true;
			}
			if (!isNeedToDraft) {
				this.props.close();
				return;
			}
		}

		let doDraft = await Confirm("Сохранить сделанные изменения?");
		this.props.close();

		if (!doDraft) 
			return;
		
		let letterGuid = (this.props.editingLetter && this.props.editingLetter.guid) || Utils.guid();

		let letter: LetterData = {
			guid: letterGuid,
			subject: this.state.subject || "-",
			body: this.state.body || "-",
			sender: this.props.sender,
			receiver: this.state.receiver || this.props.sender,
			date: new Date(),
			direction: Direction.Outcoming,
			state: LetterState.Draft,
			attachments: this.state.attachments,
			readNotification: this.state.readNotification
		};
		try {
			await lettersService.saveLetter(letter);
		}
		catch {
			Toast.showMessage("Ошибка", "Не удалось сохранить письмо в черновики.");
			return;
		}
		
		this.props.letterDrafted(letter);
	}

	/**
	 * Добавляет документы в список прикрепленных файлов.
	 * @param attachments
	 */
	private addAttachments(attachments: LetterAttachmentData[]): void {
		this.setState({
			attachments: this.state.attachments.concat(attachments)
		});
	}

	/**
	 * Заменяет документ в списке прикрепленных файлов. 
	 * @param source
	 * @param target
	 */
	private replaceAttachment(source: LetterAttachmentData, target: LetterAttachmentData): void {
		let attachments = this.state.attachments.slice();
		for (let i = 0; i < attachments.length; i++) {
			if (attachments[i] === target) {
				attachments[i] = source;
				this.setState({
					attachments: attachments
				});
				return;
			}
		}
	}

	/**
	 * Удалеяет прикрепленный документ
	 * @param guid
	 */
	private removeAttachment(guid: string): void {
		let attachments = this.state.attachments;
		for (let i = 0; i < attachments.length; i++) {
			if (attachments[i].guid === guid) {
				attachments.splice(i, 1)
				this.setState({
					attachments: attachments.slice()
				});
				return;
			}
		}
	}

	/**
	 * Устанавлиет введеное значение в поле Тема письма
	 * @param subject
	 */
	private subjectFieldOnInput(subject: string) {
		this.setState({
			subject: subject
		});
	}

	/**
	 * Обрабатывает событие click-а по кнопке в тулбаре. 
	 * @param name
	 */
	private async toolbarOnClick(name: ToolbarButtonName): Promise<void> {
		
		switch (name) {
			case "sendLetter":
				await this.sendLetter();
				return;
			case "attachFiles":
				// используется обработчик по умолчанию
				return; 
		}
		this.setState({
			activeToolbarButtonName: undefined
		});
	}

	/**
	 * Переключает флаг readNotification
	 */
	private toggleReadNotification(): void {
		this.setState({
			readNotification: !this.state.readNotification
		})
	}
	/**
	 * Получает данные для инициализации тулбара
	 */
	private getToolbarProps(): ToolbarProps {
		return {
			className: "a2-letter_form-toolbar",
			buttonOnClick: this.toolbarOnClick,
			activeButtonName: this.state.activeToolbarButtonName,
			buttons: [
				{
					name: "sendLetter",
					label: "Отправить",
					className: "a2-letter_form-toolbar-button",
					isDisabled: false
				},
				{
					name: "attachFile",
					label: "Вложить файл",
					className: "a2-letter_form-toolbar-button",
					isDisabled: false,
					elementJsx: <FileField
						onAdded={this.addAttachments}
						onReplaced={this.replaceAttachment}
						onExecuting={() => this.setState({ attachmentsIsReady: false})}
						onExecuted={() => this.setState({ attachmentsIsReady: true })}/>
				}
			]
		};
	}

	/**
	 * Инициализирует редактор tinymce
	 */
	private initTinymce() {
		tinymce.init({
			setup: (ed: any) => {
				ed.on('blur', (e: any) => {
					Console.log(ed.getContent())
					this.setState({
						body: ed.getContent()
					});
				});
			},
			selector: 'textarea',
			plugins: [
				"textcolor nonbreaking lists"
			],

			toolbar: "undo redo bold italic underline strikethrough alignleft aligncenter alignright alignjustify bullist numlist outdent indent forecolor backcolor nonbreaking",

			menubar: false,
			toolbar_items_size: 'small'
		});
	}
	
	/**
	 * @internal
	 */
	public componentDidMount() {
		setTimeout(() => this.initTinymce(), 0);
	}

	/**
	 * @internal
	 */
	public render() {
		const props = this.props;
		const state = this.state;
		const toolbarProps = this.getToolbarProps();
		
		return (
            <div className="a2-letter_form">
				<div className="a2-letter_form-header">
					--------------
					<span onClick={e => this.draftLetter()}>&#10006;</span>
                </div>
                <div className="a2-letter_form-toolbar">
					<Toolbar {...toolbarProps}/>
                </div>
                <table className="a2-letter_form-props_box" cellPadding={0} cellSpacing={0}><tbody>
				    <tr className="a2-letter_form-sender">
                        <td className="a2-letter_form-props-key">Отправитель:</td>
                        <td className="a2-letter_form-props-value">{props.sender.shortName} - {props.sender.orgName}</td>
				    </tr>
				    <tr className="a2-letter_form-receiver">
                        <td className="a2-letter_form-props-key">Получатель:</td>
                        <td className="a2-letter_form-props-value" onClick={e => this.selectReceiver()}>
						    {
							    state.receiver
									? `${state.receiver.shortName} - ${state.receiver.orgName}`
								    : <div className="a2-letter_form-receiver_blank">...</div>
						    }
					    </td>
				    </tr>
				    <tr className="a2-letter_form-subject">
                        <td className="a2-letter_form-props-key">Тема:</td>
                        <td className="a2-letter_form-props-value">
						    <input value={state.subject} onInput={e => this.subjectFieldOnInput(e.currentTarget.value)} />
					    </td>
				    </tr>
                    <tr>
                        <td colSpan={2}>
							{state.attachments.length > 0 &&
								<LetterAttachments attachments={state.attachments} removeAttachment={this.removeAttachment} isEditable={true} />}
                        </td>
                    </tr>
                </tbody></table>
				<div className="a2-letter_form-body">
					<div onClick={e => this.toggleReadNotification()} className={classNames("a2-letter_form-with_notice", { active: state.readNotification === true })} />
					<textarea value={state.body}></textarea>
                </div>
            </div>
		);
	}
}

/**
 * Компонент отвечающий за прикрепление документов. 
 * Сохраняет документы на локульный диск.
 */
class FileField extends React.Component<FileFieldProps> {
	constructor(props: FileFieldProps) {
		super(props);
		this.fileFieldOnChange = this.fileFieldOnChange.bind(this);
	}

	/**
	 * Счетчик уникальных идентификаторов
	 */
	private static _uniqId = 0;

	/**
	 * Поле с типом file
	 */
	private _fileField: HTMLInputElement | null;

	/**
	 * Идентификаторы задач, которые выполняются 
	 */
	private _promisesInProgress:number[] = [];

	/**
	 * Сохраняет документы на локальный диск
	 */
	private async fileFieldOnChange(): Promise<void> {
		if (this._fileField === null)
			return;

		let files = this._fileField.files;
		if (!files || files.length === 0)
			return;

		// подготовим загружаемые файлы
		let loadingFiles = [] as LetterAttachmentProps[];		
		for (let i = 0; i < files.length; i++) {
			let file = files[i];

			loadingFiles.push({
				guid: Utils.guid(),
				filePath: `__is_loading_file_${++FileField._uniqId}__`,
				label: file.name,
				type: LetterAttachments.Utils.getAttachmentType(file.name),
				state: LetterAttachmentState.Loading,
				isLocal: true
			});
		}
		this.props.onAdded(loadingFiles);

		// сохраним файлы в папку пользователя
		this.props.onExecuting();
		let promisesInProgress = ++FileField._uniqId;
		this._promisesInProgress.push(promisesInProgress);

		let onReplacedCallback = this.props.onReplaced;
		let promises: Promise<LetterAttachmentData>[] = [];
		for (let i = 0; i < files.length; i++) {
			let file = files[i];

			promises.push(new Promise<LetterAttachmentData>((resolve, reject) => {
				let reader = new FileReader;
				reader.onload = async (e) => {
					try {
						let newFile = await filesService.saveDocToLocalDisk(file.name, (e.target as any).result);
						onReplacedCallback(newFile, loadingFiles[i]);
					}
					catch {
						let newFile = Object.assign({}, loadingFiles[i], { state: LetterAttachmentState.Failed })
						onReplacedCallback(newFile, loadingFiles[i]);
					}
					resolve(loadingFiles[i]);
					
				};
				reader.readAsDataURL(file);
			}));
		}
		await Promise.all(promises);
		this._promisesInProgress = this._promisesInProgress.filter(x => x !== promisesInProgress);
		if (this._promisesInProgress.length === 0)
			this.props.onExecuted();
	}

	public render() {
		return (
            <div className="a2-letter_form-file_field">
                <span className="a2-letter_form-file_button">Выбрать файлы</span>
                <input ref={x => this._fileField = x} className="a2-letter_form-file_input" onChange={this.fileFieldOnChange} type="file" multiple={true} />
			</div>
		);
	}	
}

/**
 * Модель описывающая свойства компонента LetterForm
 */
export interface LetterFormProps {
	/**
	 * Отправитель
	 */
	sender: UserData,

	/**
	 * Закрывает окно
	 */
	close(): void,

	/**
	 * Выравнивает окно по центру
	 */
	center(): void,

	/**
	 * Обработчик события перед отправкой письма
	 */
	letterSending(letter: LetterData): void,

	/**
	 * Обработчик события после отправки письма
	 */
	letterSended(letterGuid: string, serverLetterGuid: string): void,

	/**
	 * Обработчик события - сохранено в черновик
	 */
	letterDrafted(letter: LetterData): void,

	/**
	 * Редактируемое письмо
	 */
	editingLetter?: LetterData
}

/**
 * Модель описывающая состояние компонента LetterForm
 */
interface LetterFormState {
	/**
	 * Название активной кнопки в тулбаре
	 */
	activeToolbarButtonName?: ToolbarButtonName,

	/**
	 * Заголовок письма
	 */
	subject?: string,

	/**
	 * Дата создания письма
	 */
	date?: string,

	/**
	 * Получатель
	 */
	receiver?: UserData,

	/**
	 * Тело письма (обычный текст)
	 */
	body?: string, 

	/**
	 * Прикрепленные файлы
	 */
	attachments: LetterAttachmentProps[],

	/**
	 * Готовы ли прикрепленные документы к отправке письма
	 */
	attachmentsIsReady: boolean,

	/**
	 * Требовать извещение о прочтении
	 */
	readNotification?: boolean,
}

/**
 * Типы кнопок в тулбаре
 */
type ToolbarButtonName = "sendLetter" | "attachFiles";

/**
 * Модель описывающая состояние компонента FileField
 */
interface FileFieldProps {
	onAdded(filePaths: LetterAttachmentData[]): void
	onReplaced(source: LetterAttachmentData, target: LetterAttachmentData): void
	onExecuting(): void
	onExecuted(): void
}
