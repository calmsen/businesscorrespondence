﻿import * as React from "react"
import * as ReactDOM from "react-dom"
import * as classNames from "classnames"
import { LetterAttachmentData, AttachmentType } from "./Models";

/**
 * Прикрепленные документы к письму. Все свойства в данном компоненте обновляемые. 
 */
export class LetterAttachments extends React.Component<LetterAttachmentsProps, LetterAttachmentsState> {
	constructor(props: LetterAttachmentsProps) {
		super(props);

		this.attachmentOnClick = this.attachmentOnClick.bind(this);

		this.state = {
			activeAttachmentGuid: props.activeAttachmentGuid
		};
	}


	/**
	 * Обработчик события на документе. Обработчик переопределяет оригинальное функцию attachmentOnClick на документе.
	 * Устанавливает новый активный документ и вызывает оригинальную обратную функцию attachmentOnClick.
	 * @param guid
	 */
	private attachmentOnClick(guid: string): void {
		if (this.props.attachments.filter(x => x.guid === guid)[0].state !== undefined 
			&& this.props.attachments.filter(x => x.guid === guid)[0].state !== LetterAttachmentState.Normal)
			return;

		this.setState({ activeAttachmentGuid: guid });

		this.props.attachmentOnClick && this.props.attachmentOnClick(guid);
	}

	/**
	 * Устанавливает статус активный, если документ таковыс является
	 * @param attachment
	 */
	private mapAttachmentState(attachment: LetterAttachmentProps): LetterAttachmentState {
		return this.props.activeAttachmentGuid === attachment.filePath 
			? LetterAttachmentState.Active
			: (attachment.state || LetterAttachmentState.Normal);
	}

	/**
	 * @internal
	 */
	public componentWillReceiveProps(nextProps: LetterAttachmentsProps) {
		const state = this.state;
		let newState = {} as LetterAttachmentsState;
		let isNeedUpdateState = false;

		if (isNeedUpdateState) {
			this.setState(newState);
		}
	}

	/**
	 * @internal
	 */
	public render() {
		const props = this.props;

		return (
			<ul className="a2-letter-attachments-list" >
			{
					props.attachments.map((b, i) => <LetterAttachment key={b.guid}
						guid={b.guid}
						filePath={b.filePath}
						label={b.label}
						type={b.type}
						state={this.mapAttachmentState(b)}
						onClick={this.attachmentOnClick}
						removeAttachment={props.removeAttachment}
						downloadFile={props.downloadFile}
						isEditable={props.isEditable}
				/>)
			}
			</ul>
        );
	}
}

/**
 * Компонент кнопки
 * @param props
 */
const LetterAttachment = (props: LetterAttachmentProps) => {
	
	
	// получим имя класса
	let className = ["a2-letter-attachment-holder", { active: props.state === LetterAttachmentState.Active }];

	switch (props.type) {
		case AttachmentType.Pdf:
			className.push("pdf");
			break;
		case AttachmentType.Image:
			className.push("image");
			break;
		case AttachmentType.Plain:
			className.push("plain");
			break;
	}
	let label = props.label;

	switch (props.state) {
		case LetterAttachmentState.Loading:
			label += " - загрузка";
			break;
		case LetterAttachmentState.Failed:
			label += " - ошибка";
			break;
	}

	return (
		<li className={classNames(className)}>
			<a id={props.filePath} className="a2-letter-attachment"
				onClick={e => { e.preventDefault(); props.onClick && props.onClick(props.guid); }} >
				{label}
			</a>
			{props.isEditable ?  
				<span onClick={e => props.removeAttachment && props.removeAttachment(props.guid)}>&#10006;</span>
				:
				<span onClick={e => props.downloadFile && props.downloadFile(props.guid)} className="download-btn">скачать</span>}
			
		</li>
	);
}

export namespace LetterAttachments.Utils {
	/**
	 * Получает тип по формату файла
	 * @param fileName
	 */
	export function getAttachmentType(fileName: string): AttachmentType {
		if (fileName.indexOf(".") === -1)
			return AttachmentType.Other;
		let ext = fileName.substring(fileName.lastIndexOf(".") + 1);
		switch (ext) {
			case "jpeg":
			case "jpg":
			case "png":
			case "tiff":
			case "bmp":
			case "gif":
				return AttachmentType.Image;
			case "pdf":
				return AttachmentType.Pdf;
			case "doc":
			case "docx":
				return AttachmentType.MsDoc;
			case "txt":
			case "rtf":
				return AttachmentType.Plain;
		}
		return AttachmentType.Other;
	}
}

/**
 * Модель описывающая свойства для компонента LetterAttachments
 */
export interface LetterAttachmentsProps {
	/**
	 * Guid активного документа
	 */
	activeAttachmentGuid?: string,

	/**
	 * Обработчик события на документе
	 */
	attachmentOnClick?(guid: string): void,

	/**
	 * Удаляет документ
	 */
	removeAttachment?(guid: string): void,

	/**
	 * Скачивает файл
	 */
	downloadFile?(guid: string): void,

	/**
	 * Прикрепленные документы
	 */
	attachments: LetterAttachmentProps[],

	/**
	 * Режим редактирования
	 */
	isEditable?: boolean
}

/**
 * Модель описывающая состояние для компонента LetterAttachments
 */
export interface LetterAttachmentsState { }

/**
 * Модель описывающая свойства для компонента LetterAttachment
 */
export interface LetterAttachmentProps extends LetterAttachmentData {
	/**
	 * Состояние документа
	 */
	state?: LetterAttachmentState,

	/**
	 * Обработчик события на документе
	 */
	onClick?(guid: string): void,

	/**
	 * Удаляет документ
	 */
	removeAttachment?(guid: string): void,

	/**
	 * Скачивает файл
	 */
	downloadFile?(guid: string): void,

	/**
	 * Режим редактирования
	 */
	isEditable?: boolean

}

/**
 * Состояние документа
 */
export enum LetterAttachmentState {
	Normal,
	Active,
	Loading,
	Failed
}





