﻿import * as React from "react"
import * as ReactDOM from "react-dom"

import { LettersListProps, LettersList } from "../LettersList";
import { LetterData, LetterState, Direction } from "../Models";


class Application extends React.Component<ApplicationProps, ApplicationState> {
    constructor(props: {}) {
		super(props);

		this.loadMoreLetters = this.loadMoreLetters.bind(this);
		this.letterOnClick = this.letterOnClick.bind(this);
	}

	/**
	 * Загружает письма для указанной страницы 
	 * @param page страница которую нужно подгрузить
	 */
	private async loadMoreLetters(page: number): Promise<void> {
		return new Promise<void>((resolve, reject) => { });
	}


	/**
	 * Обрабатывает событие click-а по письму в списке писем. Делает письмо активным.
	 * @param guid
	 */
	private letterOnClick(guid: string): void {
		
	}

	/**
	 * Получает данные для инициализации списка писем
	 */
	private getLettersListProps(): LettersListProps {
		let letters = [] as LetterData[];
		for (let i = 0; i < 30; i++) {
			letters.push({
				guid: (new Date().getTime() + i).toString(),
				subject: "Заголовок \ тема письма" + i,
				date: new Date(),
				state: LetterState.IzvPol,
				direction: Direction.Incoming,
				chainId: "184ba588cdb94b2b93733e08ad90ca29",
				readNotification: true,
				sender: {
					userId: 1,
					orgName: "ООО Общество по переподготовке специалистов водного транспорта",
					shortName: "Константинопольский К.И.",
					fullName: "Константинопольский Константин Иванович",
					post: "финансовый директор",
					isConfirmed: true,
					certSerial: "2a224bc000030006005d"
				},
				receiver: {
					userId: 2,
					orgName: "ООО Глобус",
					shortName: "Иванов В. Л.",
					fullName: "Иванов Владислав Львович",
					post: "бухгалтер",
					isConfirmed: true,
					certSerial: "2a224bc000030006005d"
				}
			});
		}


		return {
			activeLetterGuid: undefined,
			letterOnClick: this.letterOnClick,
			loadMoreLetters: this.loadMoreLetters,
			letters: letters,
			hasMoreLetters: false,
			pageLoaded: 1
		};
	}

	/**
	 * @internal
	 */
	public async componentDidMount(): Promise<void> {
		
	}
	
	/**
	 * @internal
	 */
	public render() {
		let lettersListData = this.getLettersListProps();
		

        return (
            <div>
				<LettersList {...lettersListData} />
            </div>
		);
    }
}

interface ApplicationProps { }


interface ApplicationState { }


ReactDOM.render(<Application />, document.getElementById("content"));