﻿import * as React from "react"
import * as ReactDOM from "react-dom"
import * as classNames from "classnames";
import { popUp } from "./Components/Modal";
import { UserData, OrganisationData } from "./Models";
import { addressBookService } from "./Services/AddressBookService";
import { Toast } from "./Components/Toast";
import { Utils } from "./Components/Utils";


/**
 * Показывает окно с адресной книгой. 
 * @param userId обязательный, необходим чтобы получить список организаций с последними использованными контрагентами.
 */
export const AddressBookPopUp = async (userId: number): Promise<UserData> => {
	return new Promise<UserData>((resolve, reject) => {
		let addressBookProps: AddressBookProps = {
			userId: userId,
			close: () => {
				addressBookPopUp.close();
				reject();
			},
			center: () => addressBookPopUp.center(),
			subscriberSelected: (subscriber: UserData) => {
				addressBookPopUp.close();
				resolve(subscriber);
			}
		};
		let addressBookPopUp = popUp(<AddressBook {...addressBookProps} />);
		
	});
}

/**
 * Адресная книга
 */
export class AddressBook extends React.Component<AddressBookProps, AddressBookState> {
	constructor(props: AddressBookProps) {
		super(props);

		this.organisationOnClick = this.organisationOnClick.bind(this);
		this.searchFieldOnFocus = this.searchFieldOnFocus.bind(this);
		this.blurSearchField = this.blurSearchField.bind(this);

		this.state = {
			organisations: [],
			activeOrganisationId: 0,
			isSearchModeEnabled: false,
			isSearchPromptVisible: false,
			isSearchInProgress: false,
			searchFieldValue: "",
			findedUsers: []
		};
	}

	/**
	 * Таймер, запускающий функцию для поиска пользователей.
	 * Таймер запускается черз 0.8 секунд каждый раз, 
	 * когда пользователь печатает символ в поле поиска, при этом старый таймер убивается.
	 */
	private _searchFieldRequestTimer: NodeJS.Timer;

	/**
	 * Идентификатор запроса поиска пользователей. Необходим для того чтобы проверить запрос на актуальность
	 */
	private _searchFieldRequestId = 0;

	/**
	 * Поле поиска
	 */
	private _searchField: HTMLInputElement | undefined;

	/**
	 * Устанавливает организацию активной/неактивной.
	 * @param id
	 */
	private organisationOnClick(id: number): void {
		if (id === this.state.activeOrganisationId) {
			this.setState({
				activeOrganisationId: 0
			});
		}
		else {
			this.setState({
				activeOrganisationId: id
			});
		}
		
	}

	/**
	 * Выбирает абонента 
	 * @param subscriber
	 */
	private selectSubscriber(subscriber: UserData): void {
        if (!subscriber.isConfirmed) 
			return;
		this.props.subscriberSelected(subscriber);
	}

	/**
	 * Уводит фокус с поля поиска
	 */
	private blurSearchField() {
		if (this._searchField === undefined)
			return;

		this._searchField.blur();
		this.setState({
			activeOrganisationId: 0,
			isSearchModeEnabled: false,
			isSearchPromptVisible: this.state.searchFieldValue.length < 3
		});
	}

	/**
	 * Включает режим поиска 
	 */
	private searchFieldOnFocus(): void {
		this.setState({
			isSearchModeEnabled: true,
			isSearchPromptVisible: this.state.searchFieldValue.length < 3
		});
	}
	
	/**
	 * Запускает поиск пользователей, если введено три символа
	 */
	private searchFieldOnInput(input: string): void {
		clearTimeout(this._searchFieldRequestTimer);
		let isSearchInProgress = false;
		if (input.length >= 3) {	
			isSearchInProgress = true;
			this._searchFieldRequestTimer = setTimeout(() => this.findUsers(input), 800);
		}
		this.setState({
			isSearchPromptVisible: input.length < 3,
			searchFieldValue: input,
			findedUsers: input.length < 3 ? [] : this.state.findedUsers,
			isSearchInProgress: isSearchInProgress
		});
	}

	/**
	 * Находит пользователей
	 * @param query
	 */
	private async findUsers(query: string): Promise<void> {
		let searchFieldRequestId = ++this._searchFieldRequestId;
		if (this._searchFieldRequestId !== searchFieldRequestId)
			return;

		try {
			let findedUsers = await addressBookService.findUsers(query);

			this.setState({
				isSearchInProgress: false,
				findedUsers: findedUsers
			});
		}
		catch {
			Toast.showMessage("Ошибка", "Не удалось найти пользователя.");
		}
	}

	/**
	 * Устанавливает организации полученные от сервера
	 */
	private async setOrganisations(): Promise<void> {
		try {
			let organisations = await addressBookService.getOrganisations(this.props.userId);

			this.setState({
				organisations: organisations
			});
		}
		catch {
			Toast.showMessage("Ошибка", "Не удалось получить список организаций.");
		}		
	}

	/**
	 * @internal
	 */
	public componentDidMount() {
		this.setOrganisations();
	}

	/**
	 * При инициализации показывается "Список организаций с последними использованными контрагентами".
	 * При наведении фокуса на поле поиска адресная книга переключается в режим поиска.
	 * @internal
	 */
	public render() {
		const props = this.props;
		const state = this.state;

		return (
			<div className="a2-address_book">
				<div className="a2-address_book-header">
					Адресная книга
					<span onClick={e => props.close()}>&#10006;</span>
				</div>
				<div className="a2-address_book-search_field-holder">
					<input ref={x => this._searchField = x || undefined} className="a2-address_book-search_field" value={state.searchFieldValue} 
						onFocus={e => this.searchFieldOnFocus()}
						onInput={e => this.searchFieldOnInput(e.currentTarget.value)}
                    />
                    <span className="a2-address_book-search_reset_btn" onClick={e => this.blurSearchField()}>&#10006;</span>
				</div>
				{!state.isSearchModeEnabled ?
					<div className="a2-adress_book-organisations">
						{state.organisations.map((x, i) =>
							<div key={x.id} className={classNames("a2-adress_book-organisation", { active: x.id === state.activeOrganisationId })}>
								<div className="a2-adress_book-organisation-name" onClick={e => this.organisationOnClick(x.id)}>{Utils.normalizeOrgName(x.name, ...(x.members.map(y => y.fullName)))}</div>
								{x.id === state.activeOrganisationId &&
                                    <table className="a2-adress_book-organisation-members" cellPadding={0} cellSpacing={0}><tbody>
										{
											x.members.map((y, j) =>
                                            <tr key={j} className={classNames("a2-adress_book-organisation-member", { certified: y.isConfirmed })} onClick={e => this.selectSubscriber(y)}>
												<td className="a2-member-name">{y.fullName || y.shortName || "-"}</td>
												<td className="a2-member-post">{y.post || "-"}</td>
											</tr>
											)
										}
                                </tbody></table>
								}

							</div>
						)}
					</div>
					:
					state.isSearchPromptVisible ?
						<div className="a2-adress_book-search_prompt">Введите имя контрагента для поиска!</div>
                        :
                        <table className="a2-adress_book-search_result" cellPadding={0} cellSpacing={0}><tbody>
							<tr className="a2-adress_book-search_result-header">
                                <td>Организация</td>
                                <td>ФИО</td>
							</tr>
							{
                                state.isSearchInProgress ?
                                    <tr className="a2-adress_book-search_result-loading"><td colSpan={2}>Загрузка...</td></tr>
									:
									state.findedUsers.length === 0 ?
                                        <tr className="a2-adress_book-search_result-empty"><td colSpan={2}>Контрагенты не найдены</td></tr>
										:
										state.findedUsers.map((x, i) =>
											<tr key={x.orgId + "-" + x.userId} className={classNames("a2-adress_book-search_result-finded_user", { certified: x.isConfirmed })}
												onClick={e => this.selectSubscriber(x)}
											>
												<td className="a2-user-organisation">{Utils.normalizeOrgName(x.orgName, x.fullName)}</td>
												<td className="a2-user-name">{x.fullName}</td>
											</tr>
										)
							}
                        </tbody></table>
				}
				

			</div>
        );
	}
}


/**
 * Модель описывающая свойства компонента AddressBook
 */
export interface AddressBookProps {

	/**
	 * Закрывает окно
	 */
	close(): void,

	/**
	 * Выравнивает окно по центру
	 */
	center(): void,

	/**
	 * Текущий пользователь для которого запрашивается организации 
	 */
	userId: number,

	/**
	 * Вызываемое событие при выборе абонента
	 */
	subscriberSelected(subscriber: UserData): void
}

/**
 * Модель описывающая состояние компонента AddressBook
 */
export interface AddressBookState {
	/**
	 * Список организаций с последними использованными контрагентами
	 */
	organisations: OrganisationData[],

	/**
	 * Список организаций с последними использованными контрагентами
	 */
	activeOrganisationId: number,

	/**
	 * Активен ли режим поиска контрагентов
	 */
	isSearchModeEnabled: boolean,

	/**
	 * Видно ли сообщение с подсказкой. 
	 */
	isSearchPromptVisible: boolean,

	/**
	 * Выполняется ли запрос на сервер для поиска контрагентов
	 */
	isSearchInProgress: boolean,

	/**
	 * Текущее значение поля поиска
	 */
	searchFieldValue: string,

	/**
	 * Найденные контрагенты
	 */
	findedUsers: UserData[]
}



